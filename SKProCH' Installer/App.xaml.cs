﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Ionic.Zip;
using MaterialDesignThemes.Wpf;
using Microsoft.Win32;
using Rasyidf.Localization;
using Sentry;
using Shared.Model;
using Shared.Palette;
using SKProCHInstaller.Model;
using SKProCHInstaller.View;
using SKProCHInstaller.ViewModel;
using Utilities = SKProCHInstaller.Model.Utilities;

namespace SKProCHInstaller {
    /// <summary>
    ///     Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application {
        public static readonly RegistryKey Reg = Registry.CurrentUser.OpenSubKey("SOFTWARE", true)?.CreateSubKey(@"SKProCH's Launcher");
        [Localizable(false)] private static string LanguagesFilesPath = $"{Path.GetTempPath()}\\SKProCH's Installer Language Files\\";

        public static IDisposable Sentry = SentrySdk.Init(options => {
            options.Dsn = new Dsn("https://84deafe985274fe28bbfe08b4d01dace@sentry.io/1862889");
            options.Release = Assembly.GetExecutingAssembly().GetName().Version + "i";
            options.AttachStacktrace = true;
        });

        public static string ChoosenDirectory { get; set; }

        protected override async void OnStartup(StartupEventArgs e) {
            FrameworkElement.StyleProperty.OverrideMetadata(typeof(Window),
                new FrameworkPropertyMetadata {DefaultValue = Current.FindResource(typeof(Window))});
            Exceptions.SetGlobalHandlers(Dispatcher.CurrentDispatcher);
            foreach (var file in Directory.GetFiles(Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]), @"*.bak"))
                try {
                    File.Delete(file);
                }
                catch (Exception) {
                    // ignored
                }


            ChoosenDirectory = Path.Combine(Utilities.GetBiggestDriveName(), "Games\\SKProCH's Minecraft Launcher\\");
            #if DEBUG
            App.LanguagesFilesPath = Path.Combine(Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\..\\")), "Languages");
            #endif

            LoadCulture(LanguagesFilesPath);


            base.OnStartup(e);
            RegPaletteHelper.LoadFromReg();

            new MainWindow {DataContext = new MainWindowVM()}.Show();


            #if !DEBUG
            if (await Updates.IsInstallerUpdateAvailable(Assembly.GetExecutingAssembly().GetName().Version))
                Dispatcher?.BeginInvoke(new Action(async () => await DialogHost.Show(new UpdateExists(), @"Main")));
            #endif
        }

        [Localizable(false)]
        public static string BuildCultureUrl(string culture) {
            return $"https://gitlab.com/skprochlab/MCLauncher/raw/master/SKProCH'%20Installer/Languages/{culture}.xml";
        }

        public static void DownloadAllCultures(string path) {
            Directory.CreateDirectory(path);
            #if !DEBUG
            using (var wc = new WebClient()) {
                File.Delete(Path.Combine(path, "All languages.zip"));
                wc.DownloadFile(new Uri("https://gitlab.com/skprochlab/MCLauncher/-/archive/master/MCLauncher-master.zip?path=Languages"),
                    Path.Combine(path, "All languages.zip"));
            }

            using (var zf = new ZipFile(Path.Combine(path, "All languages.zip"))) {
                zf.ToList()
                  .ForEach(zipEntry => {
                       try {
                           zipEntry.FileName = Path.GetFileName(zipEntry.FileName);
                           zipEntry.Extract(path, ExtractExistingFileAction.OverwriteSilently);
                       }
                       catch (Exception) {
                           // ignored
                       }
                   });
            }

            File.Delete(Path.Combine(path, "All languages.zip"));
            #endif
        }

        public static void LoadCulture(string path) {
            Directory.CreateDirectory(path);
            var IdFromReg = Reg.GetValue("Language") ?? Thread.CurrentThread.CurrentUICulture.IetfLanguageTag;
            var NeedAsyncRefresh = true;
            if (!(EnumerateAllCultures(path).Any(s => s.Contains(IdFromReg.ToString())) || EnumerateAllCultures(path).Any(s => s.Contains(@"en-US")))) {
                DownloadAllCultures(path);
                NeedAsyncRefresh = false;
            }

            LocalizationService.ScanLanguagesInFolder(path);
            LocalizationService.Current.Initialize(path,
                EnumerateAllCultures(path).Any(s => s.Contains((string) IdFromReg))
                    ? (string) IdFromReg
                    : @"en-US");
            if (NeedAsyncRefresh)
                Task.Run(() => {
                    DownloadAllCultures(path);
                    LocalizationService.ScanLanguagesInFolder(path);
                    LocalizationService.Current.Initialize(path,
                        EnumerateAllCultures(path).Any(s => s.Contains((string) IdFromReg))
                            ? (string) IdFromReg
                            : @"en-US");
                });
        }

        public static string[] EnumerateAllCultures(string path) {
            return Directory.GetFiles(path);
        }
    }
}