﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Controls;
using Shared;
using SKProCHInstaller.Model;

namespace SKProCHInstaller.View {
    /// <summary>
    ///     Логика взаимодействия для UpdateExists.xaml
    /// </summary>
    public partial class UpdateExists : UserControl, INotifyPropertyChanged {
        private bool _isProcessing;
        private int _progress;

        public UpdateExists() {
            InitializeComponent();
        }

        public AnotherCommandImplementation AutoUpdate =>
            new AnotherCommandImplementation(async o => {
                IsProcessing = true;
                using (var wc = new WebClient()) {
                    wc.DownloadProgressChanged += (sender, args) => Progress = args.ProgressPercentage;
                    var latest = (await Updates.GetManifest())[0].Assets.Links.First(link => link.Name.Contains(@"SKProCH's Installer"));
                    File.Move(Environment.GetCommandLineArgs()[0], Path.ChangeExtension(Environment.GetCommandLineArgs()[0], ".bak"));
                    var newPath = Environment.GetCommandLineArgs()[0];
                    await wc.DownloadFileTaskAsync(latest.Url, newPath);
                    await Task.Delay(500);
                    var prc = Process.Start(newPath);
                    Environment.Exit(0);
                }
            });

        public AnotherCommandImplementation ManualUpdate => new AnotherCommandImplementation(o => {
            var prc = Process.Start("https://gitlab.com/skprochlab/MCLauncher/-/releases");
            Environment.Exit(0);
        });

        public bool IsProcessing {
            get => _isProcessing;
            set {
                _isProcessing = value;
                RaisePropertyChanged(@"IsProcessing");
            }
        }

        public int Progress {
            get => _progress;
            set {
                _progress = value;
                RaisePropertyChanged(@"Progress");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void RaisePropertyChanged(string propName) {
            var e = PropertyChanged;
            e?.Invoke(this, new PropertyChangedEventArgs(propName)); // некоторые из нас здесь используют Dispatcher, для безопасного взаимодействия с UI thread
        }
    }
}