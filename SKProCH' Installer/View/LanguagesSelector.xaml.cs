﻿using System.Collections.Generic;
using System.Globalization;
using System.Windows.Controls;
using Rasyidf.Localization;

namespace SKProCHInstaller.View {
    /// <summary>
    ///     Логика взаимодействия для LanguagesSelector.xaml
    /// </summary>
    public partial class LanguagesSelector : UserControl {
        public LanguagesSelector() {
            InitializeComponent();
        }

        private void LanguagesComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (LanguagesComboBox.SelectedItem == null) return;
            var pair = LanguagesComboBox.SelectedItem is KeyValuePair<CultureInfo, LocalizationDictionary> item ? item : default;
            LocalizationService.Current.ChangeLanguage(pair.Value);
            App.Reg.SetValue("Language", pair.Value.CultureId);
        }
    }
}