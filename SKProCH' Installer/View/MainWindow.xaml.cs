﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SKProCHInstaller.View {
    /// <summary>
    ///     Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        private void ScrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e) {
            var scv = (ScrollViewer) sender;
            if (scv.HorizontalScrollBarVisibility != ScrollBarVisibility.Disabled) {
                scv.ScrollToVerticalOffset(scv.VerticalOffset - e.Delta);
                e.Handled = true;
            }
        }
    }
}