﻿using System.Windows.Controls;

namespace SKProCHInstaller.View {
    /// <summary>
    ///     Логика взаимодействия для InstallMethods.xaml
    /// </summary>
    public partial class Install : UserControl {
        public Install() {
            InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e) {
            (sender as TextBox)?.ScrollToEnd();
        }
    }
}