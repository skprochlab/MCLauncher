﻿using System.ComponentModel;
using Shared;

namespace SKProCHInstaller.ViewModel {
    [Magic]
    // ReSharper disable once InconsistentNaming
    internal class InstallVM : PropertyChangedBase {
        public delegate void ProgressContainer(double percent, string stage);

        public delegate void ProgressLog(string message, double percent);

        public InstallVM() {
            ProgressUpdate += (percent, stage) => {
                InstallProgress = percent;
                InstallStage = stage;
            };
            LogUpdate += (message, percent) => {
                InstallLog += $"{message}\n";
                InstallProgress += percent;
            };
        }

        public double InstallProgress { get; set; }
        public string InstallStage { get; set; }
        [Localizable(false)] public string InstallLog { get; set; }

        //static - костыль, чтобы убрать необходимости в ссылках на класс
        public static event ProgressContainer ProgressUpdate;
        public static event ProgressLog LogUpdate;

        public static void OnProgressUpdate(double percent, string stage) {
            ProgressUpdate?.Invoke(percent, stage);
            LogUpdate?.Invoke(stage, 0);
        }

        public static void OnLogUpdate(string logEntry, double percent) {
            LogUpdate?.Invoke(logEntry, percent);
        }
    }
}