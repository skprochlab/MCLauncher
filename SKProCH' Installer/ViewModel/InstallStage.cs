using System.Windows.Controls;
using Rasyidf.Localization;
using Shared;

namespace SKProCHInstaller.ViewModel {
    [Magic]
    public sealed class InstallStage : PropertyChangedBase {
        public LocalizedString StageName { get; set; }
        public bool IsEnabled { get; set; }
        public object Content { get; set; }
        public ScrollBarVisibility IsScrollBarVisible { get; set; }
    }
}