﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using MaterialDesignThemes.Wpf;
using Rasyidf.Localization;
using Shared;
using Shared.Model;
using Shared.Palette;
using SKProCHInstaller.Model;
using SKProCHInstaller.View;
using Utilities = SKProCHInstaller.Model.Utilities;

namespace SKProCHInstaller.ViewModel {
    // ReSharper disable once InconsistentNaming
    [Magic]
    public sealed class MainWindowVM : PropertyChangedBase {
        public MainWindowVM() {
            #if !DEBUG
            IsInstalledAlready = App.Reg.GetValue("Version") != null;
            #endif

            IsDetermined = true;
            #if !DEBUG
            if (IsInstalledAlready)
                App.ChoosenDirectory = (string) App.Reg.GetValue("Path");
            #endif


            WindowHeight = 400;
            WindowWidth = 500;
            ContinueCommand = new AnotherCommandImplementation(o => {
                if (SelectedStageIndex + 1 >= Stages.Count) {
                    IsFinalScene = true;
                    return;
                }

                try {
                    ((Stages[SelectedStageIndex].Content as UserControl)?.DataContext as IEndingUsing)?.OnEndingUsing();
                    SelectedStageIndex++;
                    if (SelectedStage == InstallStage)
                        StartInstall();

                    Stages[SelectedStageIndex].IsEnabled = true;
                }
                catch (Exception e) {
                    MyMessageQueue.Enqueue(e.Message);
                }
            });
            Stages = new List<InstallStage> {
                LanguageStage,
                ColorToolStage,
                ChooseDirectoryStage,
                AdditionalOptionsStage,
                InstallStage
            };
            SelectedStage = Stages[0];

            var i = 0;

            foreach (var variable in LocalizationService.RegisteredPacks) {
                if (variable.Key.IetfLanguageTag == LocalizationService.Current.Culture.IetfLanguageTag) {
                    SelectedLanguageIndex = i;
                    break;
                }

                i++;
            }
        }

        public int WindowWidth { get; set; }
        public int WindowHeight { get; set; }

        public IList<InstallStage> Stages { get; set; }
        public int SelectedStageIndex { get; set; }
        public InstallStage SelectedStage { get; set; }

        public Dictionary<CultureInfo, LocalizationDictionary> LanguagesList => LocalizationService.RegisteredPacks;
        public int SelectedLanguageIndex { get; set; }

        public bool IsLogoShown { get; set; } = true;
        public bool IsFinalScene { get; set; }
        public bool IsDeleting { get; set; }
        public bool IsInstalledAlready { get; set; }
        public bool IsDetermined { get; set; }

        private bool IsInstallingNow {
            set => CanContinue = !value;
        }

        public bool CanContinue { get; set; } = true;

        public SnackbarMessageQueue MyMessageQueue { get; } = new SnackbarMessageQueue {IgnoreDuplicate = false};

        #region Child properties

        public bool CreateLnk { get; set; } = true;

        #endregion

        public AnotherCommandImplementation ContinueCommand { get; }

        public AnotherCommandImplementation HideLogoCommand => new AnotherCommandImplementation(o => { IsLogoShown = false; });

        public AnotherCommandImplementation ExitCommand => new AnotherCommandImplementation(o => { ExitUtilities.InitiateExit(); });

        public AnotherCommandImplementation ExitAndOpenLauncherCommand =>
            new AnotherCommandImplementation(o => {
                foreach (var file in Directory.GetFiles(App.ChoosenDirectory, "*.exe")) {
                    if (!file.Contains(@"SKProCHs Launcher")) continue;
                    var p = new ProcessStartInfo(file) {WorkingDirectory = Path.GetDirectoryName(file)};
                    Process.Start(p);
                    break;
                }

                ExitUtilities.InitiateExit();
            });

        public AnotherCommandImplementation UpdateCommand =>
            new AnotherCommandImplementation(o => {
                foreach (var variable in Stages)
                    variable.IsEnabled = false;

                SelectedStage = InstallStage;
                IsLogoShown = false;
                StartInstall();
            });

        public AnotherCommandImplementation DeleteCommand =>
            new AnotherCommandImplementation(o => {
                var result = MessageBox.Show(
                    LocalizationService.GetString(@"Uninstall", @"SureMessage", @"Are you sure you want to remove SKProCH's Launcher?"),
                    LocalizationService.GetString(@"Uninstall", @"SureCaption", @"Deletion Confirmation"), MessageBoxButton.YesNo);
                if (result == MessageBoxResult.No)
                    return;

                SelectedStage = InstallStage;
                IsLogoShown = false;

                foreach (var variable in Stages)
                    variable.IsEnabled = false;

                DeleteLauncher();
            });

        public AnotherCommandImplementation Reinstall => new AnotherCommandImplementation(o => {
            Process.Start(Environment.GetCommandLineArgs()[0]);
            Environment.Exit(0);
        });

        public void StartInstall() {
            Task.Run(async () => {
                IsInstallingNow = true;
                try {
                    Directory.CreateDirectory(App.ChoosenDirectory);
                    Stages.ToList().ForEach(it => it.IsEnabled = false);
                    await InstallMethods.StartInstall(CreateLnk);
                }
                catch (Exception e) {
                    MyMessageQueue.Enqueue(e.Message);
                }

                IsInstallingNow = false;
            });
        }

        public void DeleteLauncher() {
            IsInstallingNow = true;

            Task.Run(async () => {
                await InstallMethods.KillLauncherProcess();
                InstallVM.OnProgressUpdate(0, LocalizationService.GetString(@"Uninstall", @"Delete", @"Deleting files"));
                Utilities.RecursiveFoldersDelete(App.ChoosenDirectory);
                InstallVM.OnProgressUpdate(80, LocalizationService.GetString(@"Uninstall", @"CleanReg", @"Cleaning the registry"));

                try {
                    await InstallMethods.ClearReg();
                }
                catch (Exception) {
                    // ignored
                }

                InstallVM.OnProgressUpdate(90, LocalizationService.GetString(@"Uninstall", @"CleanReg", @"Cleaning the registry"));

                try {
                    await InstallMethods.RemoveUrlProcessing();
                }
                catch (Exception) {
                    // ignored
                }

                InstallVM.OnProgressUpdate(100, LocalizationService.GetString(@"Install", @"End", @"Ending"));
                IsDeleting = true;
                IsInstallingNow = false;
            });
        }

        #region InstallStages

        public static readonly InstallStage LanguageStage = new InstallStage {
            IsEnabled = true,
            StageName = new LocalizedString(@"Stage", @"LanguageName", @"Language"),
            Content = new LanguagesSelector(),
            IsScrollBarVisible = ScrollBarVisibility.Auto
        };

        public static readonly InstallStage ColorToolStage = new InstallStage {
            IsEnabled = false,
            StageName = new LocalizedString(@"Stage", @"ThemeName", @"Theme"),
            Content = new ColorTool {DataContext = new ColorToolViewModel()},
            IsScrollBarVisible = ScrollBarVisibility.Disabled
        };

        public static readonly InstallStage ChooseDirectoryStage = new InstallStage {
            IsEnabled = false,
            StageName = new LocalizedString(@"Stage", @"DirectoryName", @"Directory"),
            IsScrollBarVisible = ScrollBarVisibility.Auto,
            Content = new ChooseDirectory {DataContext = new ChooseDirectoryVM()}
        };

        public static readonly InstallStage AdditionalOptionsStage = new InstallStage {
            IsEnabled = false,
            StageName = new LocalizedString(@"Stage", @"AdditionalName", @"Additional"),
            IsScrollBarVisible = ScrollBarVisibility.Auto,
            Content = new AdditionalOptions()
        };

        public static readonly InstallStage InstallStage = new InstallStage {
            IsEnabled = false,
            StageName = new LocalizedString(@"Stage", @"InstallName", @"Installing"),
            IsScrollBarVisible = ScrollBarVisibility.Disabled,
            Content = new Install {DataContext = new InstallVM()}
        };

        #endregion
    }
}