﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using Rasyidf.Localization;
using SKProCHInstaller.ViewModel;

namespace SKProCHInstaller.Model {
    internal static class Utilities {
        public static void GrantAccess(string fullPath) {
            var dInfo = new DirectoryInfo(fullPath);
            var dSecurity = dInfo.GetAccessControl();

            dSecurity.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), FileSystemRights.FullControl,
                InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.NoPropagateInherit, AccessControlType.Allow));

            dInfo.SetAccessControl(dSecurity);
        }

        public static IEnumerable<string> RecursiveFilesScan(string dir) {
            Directory.CreateDirectory(dir);
            var dirs = new List<string>();
            dirs.AddRange(Directory.GetDirectories(dir));
            var result = new List<string>();
            result.AddRange(Directory.GetFiles(dir));
            dirs.ForEach(variable => result.AddRange(RecursiveFilesScan(variable)));
            return result;
        }

        public static void RecursiveFoldersDelete([Localizable(false)] string dir, int percents = 80) {
            var files = RecursiveFilesScan(dir).ToList();

            files.ForEach(variable => {
                try {
                    File.Delete(variable);

                    InstallVM.OnLogUpdate(string.Format(LocalizationService.GetString(@"Install", @"FileDeleted", @"File {0} was deleted"),
                            variable.Replace(Directory.GetCurrentDirectory(), ""))
                        , percents / (double) files.Count);
                }
                catch (Exception e) {
                    InstallVM.OnLogUpdate(string.Format(LocalizationService.GetString(@"Uninstall", @"DeleteError", @"Error while deleting {0}: {1}\n{2}"),
                        variable.Replace(Directory.GetCurrentDirectory(), ""), e.Message, e.StackTrace), percents / (double) files.Count);
                }
            });

            RecursiveFoldersDeleteMethod(dir);
        }

        private static void RecursiveFoldersDeleteMethod(string dir) {
            Directory.GetDirectories(dir).ToList().ForEach(RecursiveFoldersDeleteMethod);
            Directory.Delete(dir, true);
        }

        public static string GetBiggestDriveName() {
            var allDrives = DriveInfo.GetDrives();
            DriveInfo biggestDrive = null;

            allDrives
               .ToList()
               .ForEach(driveInfo => {
                    if (!driveInfo.IsReady)
                        return;
                    if (biggestDrive == null || biggestDrive.TotalFreeSpace < driveInfo.TotalFreeSpace)
                        biggestDrive = driveInfo;
                });

            return biggestDrive?.Name;
        }
    }
}