﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Ionic.Zip;
using Microsoft.Win32;
using Rasyidf.Localization;
using SKProCHInstaller.ViewModel;
using vbAccelerator.Components.Shell;

namespace SKProCHInstaller.Model {
    internal static class InstallMethods {
        public static async Task StartInstall(bool createLnk) {
            try {
                await PrivateStartInstall(createLnk);
            }
            catch (Exception e) {
                InstallVM.OnProgressUpdate(0, "EXCEPTION");
                InstallVM.OnProgressUpdate(0, e.ToString());
                InstallVM.OnProgressUpdate(0, "REPORT THIS ON GITLAB AND RERUN INSTALLER");
            }
        }

        private static async Task PrivateStartInstall(bool createLnk) {
            Utilities.GrantAccess(App.ChoosenDirectory);
            await KillLauncherProcess();
            InstallVM.OnProgressUpdate(0, LocalizationService.GetString(@"Install", @"CleanFolder", @"Cleaning the installation folder"));
            await ClearInstall();
            InstallVM.OnProgressUpdate(10, LocalizationService.GetString(@"Install", @"DownloadingPackage", @"Downloading the package"));
            await DownloadArchive();
            InstallVM.OnProgressUpdate(40, LocalizationService.GetString(@"Install", @"UnzipPackage", @"Unzipping the package"));
            await Unpack();
            InstallVM.OnProgressUpdate(80, LocalizationService.GetString(@"Install", @"WriteToReg", @"Writing data to the registry"));
            await WriteToReg();

            foreach (var file in Directory.GetFiles(App.ChoosenDirectory, "*.exe")) {
                if (!file.Contains(@"SKProCHs Launcher")) continue;
                if (createLnk) {
                    var shellLink = new ShellLink {Target = file, WorkingDirectory = App.ChoosenDirectory};
                    shellLink.Save(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "SKProCH's Launcher.lnk"));
                }

                await SetUrlProcessing(file);
                break;
            }

            InstallVM.OnProgressUpdate(100, LocalizationService.GetString(@"Install", @"End", @"Ending"));
        }
        #pragma warning disable CS1998 // В данном асинхронном методе отсутствуют операторы await, поэтому метод будет выполняться синхронно. Воспользуйтесь оператором await для ожидания неблокирующих вызовов API или оператором await Task.Run(...) для выполнения связанных с ЦП заданий в фоновом потоке.
        public static async Task KillLauncherProcess() {
            #pragma warning restore CS1998 // В данном асинхронном методе отсутствуют операторы await, поэтому метод будет выполняться синхронно. Воспользуйтесь оператором await для ожидания неблокирующих вызовов API или оператором await Task.Run(...) для выполнения связанных с ЦП заданий в фоновом потоке.
            foreach (var process in Process.GetProcesses())
                try {
                    if (process.MainModule != null && process.MainModule.FileName.Contains(App.ChoosenDirectory)) process.Kill();
                }
                catch (Exception) {
                    // ignored
                }
        }

        public static async Task DownloadArchive(int percents = 30) {
            var latest = (await Updates.GetManifest())[0]
                        .Assets.Links.First(link => link.Name.Contains(@"SKProCH's Launcher") || link.Name.Contains(@"SKProCH' Launcher"));

            Directory.CreateDirectory(App.ChoosenDirectory);

            using var wc = new WebClient();
            var allEvents =
                Observable.FromEventPattern<DownloadProgressChangedEventHandler, DownloadProgressChangedEventArgs>(
                               h => wc.DownloadProgressChanged += h,
                               h => wc.DownloadProgressChanged -= h)
                          .Select(p => p.EventArgs);
            var debouncedEvents = allEvents.Sample(TimeSpan.FromMilliseconds(250))
                                           .DistinctUntilChanged();
            using (debouncedEvents.Subscribe(args =>
                InstallVM.OnLogUpdate(string.Format(LocalizationService.GetString(@"Install", @"Downloaded", @"Downloaded {0}%"), args.ProgressPercentage),
                    args.ProgressPercentage))) {
                await wc.DownloadFileTaskAsync(latest.Url, Path.Combine(App.ChoosenDirectory, "temp.zip"));
            }

            InstallVM.OnLogUpdate(string.Format(LocalizationService.GetString(@"Install", @"Downloaded", @"Downloaded {0}%"), 100), 100);
        }

        #pragma warning disable CS1998 // В данном асинхронном методе отсутствуют операторы await, поэтому метод будет выполняться синхронно. Воспользуйтесь оператором await для ожидания неблокирующих вызовов API или оператором await Task.Run(...) для выполнения связанных с ЦП заданий в фоновом потоке.
        public static async Task Unpack(int percents = 40) {
            #pragma warning restore CS1998 // В данном асинхронном методе отсутствуют операторы await, поэтому метод будет выполняться синхронно. Воспользуйтесь оператором await для ожидания неблокирующих вызовов API или оператором await Task.Run(...) для выполнения связанных с ЦП заданий в фоновом потоке.
            var zf = ZipFile.Read(Path.Combine(App.ChoosenDirectory, "temp.zip"));

            foreach (var zipEntry in zf.Entries)
                try {
                    zipEntry.Extract(App.ChoosenDirectory);
                    InstallVM.OnLogUpdate(string.Format(LocalizationService.GetString(@"Install", @"FileUnzipped", @"File {0} unzipped"), zipEntry.FileName),
                        percents / (double) zf.Entries.Count);
                }
                catch (Exception e) {
                    InstallVM.OnLogUpdate(
                        string.Format(LocalizationService.GetString(@"Install", @"UnzippingError", @"Error while unzipping file {0}: {1}\n{2}"),
                            zipEntry.FileName, e.Message, e.StackTrace),
                        percents / (double) zf.Entries.Count);
                }

            zf.Dispose();
            File.Delete(Path.Combine(App.ChoosenDirectory, "temp.zip"));
        }

        #pragma warning disable CS1998 // В данном асинхронном методе отсутствуют операторы await, поэтому метод будет выполняться синхронно. Воспользуйтесь оператором await для ожидания неблокирующих вызовов API или оператором await Task.Run(...) для выполнения связанных с ЦП заданий в фоновом потоке.
        public static async Task ClearInstall(int percents = 10) {
            #pragma warning restore CS1998 // В данном асинхронном методе отсутствуют операторы await, поэтому метод будет выполняться синхронно. Воспользуйтесь оператором await для ожидания неблокирующих вызовов API или оператором await Task.Run(...) для выполнения связанных с ЦП заданий в фоновом потоке.

            Directory.GetFiles(App.ChoosenDirectory).ToList()
                     .ForEach(file => {
                          try {
                              File.Delete(file);
                          }
                          catch (Exception) {
                              // ignored
                          }

                          InstallVM.OnLogUpdate(string.Format(LocalizationService.GetString(@"Install", @"FileDeleted", @"File {0} was deleted"), file),
                              percents / (double) Directory.GetFiles(App.ChoosenDirectory).Length);
                      });
        }

        [Localizable(false)]
        #pragma warning disable CS1998 // В данном асинхронном методе отсутствуют операторы await, поэтому метод будет выполняться синхронно. Воспользуйтесь оператором await для ожидания неблокирующих вызовов API или оператором await Task.Run(...) для выполнения связанных с ЦП заданий в фоновом потоке.
        public static async Task SetUrlProcessing(string path, int percents = 10) {
            #pragma warning restore CS1998 // В данном асинхронном методе отсутствуют операторы await, поэтому метод будет выполняться синхронно. Воспользуйтесь оператором await для ожидания неблокирующих вызовов API или оператором await Task.Run(...) для выполнения связанных с ЦП заданий в фоновом потоке.
            var urlHandler = Registry.ClassesRoot;
            urlHandler = urlHandler.CreateSubKey("skpmclauncher");
            urlHandler.SetValue("", "SKProCH's Launcher");
            urlHandler.SetValue("URL Protocol", "");
            var iconUrlHandler = urlHandler.CreateSubKey("DefaultIcon");
            iconUrlHandler.SetValue("", path);
            urlHandler = urlHandler.CreateSubKey("shell");
            urlHandler = urlHandler.CreateSubKey("open");
            urlHandler = urlHandler.CreateSubKey("command");
            urlHandler.SetValue("", '"' + path + '"' + " " + '"' + "%1" + '"');
        }

        #pragma warning disable CS1998 // В данном асинхронном методе отсутствуют операторы await, поэтому метод будет выполняться синхронно. Воспользуйтесь оператором await для ожидания неблокирующих вызовов API или оператором await Task.Run(...) для выполнения связанных с ЦП заданий в фоновом потоке.
        public static async Task RemoveUrlProcessing(int percents = 10) {
            Registry.ClassesRoot.DeleteSubKeyTree("skpmclauncher");
        }
        #pragma warning restore CS1998 // В данном асинхронном методе отсутствуют операторы await, поэтому метод будет выполняться синхронно. Воспользуйтесь оператором await для ожидания неблокирующих вызовов API или оператором await Task.Run(...) для выполнения связанных с ЦП заданий в фоновом потоке.

        public static async Task WriteToReg(int percents = 10) {
            var latest = (await Updates.GetManifest())[0];
            App.Reg.SetValue("Version", latest.TagName);
            App.Reg.SetValue("Path", App.ChoosenDirectory);
        }

        #pragma warning disable CS1998 // В данном асинхронном методе отсутствуют операторы await, поэтому метод будет выполняться синхронно. Воспользуйтесь оператором await для ожидания неблокирующих вызовов API или оператором await Task.Run(...) для выполнения связанных с ЦП заданий в фоновом потоке.
        public static async Task ClearReg(int percents = 10) {
            #pragma warning restore CS1998 // В данном асинхронном методе отсутствуют операторы await, поэтому метод будет выполняться синхронно. Воспользуйтесь оператором await для ожидания неблокирующих вызовов API или оператором await Task.Run(...) для выполнения связанных с ЦП заданий в фоновом потоке.
            App.Reg.DeleteValue("Version");
            App.Reg.DeleteValue("Path");
        }
    }
}