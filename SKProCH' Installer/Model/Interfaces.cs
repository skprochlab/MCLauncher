﻿namespace SKProCHInstaller.Model {
    //The interface for those classes that must perform some operations before end of using
    public interface IEndingUsing {
        void OnEndingUsing();
    }
}