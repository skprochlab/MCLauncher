﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Launcher.ViewModel;

namespace Launcher.View {
    /// <summary>
    ///     Логика взаимодействия для IconManager.xaml
    /// </summary>
    public partial class IconManager : Window {
        public IconManager() {
            InitializeComponent();
        }

        public IconManagerVM VM => (IconManagerVM) DataContext;

        private void UIElement_OnPreviewMouseWheel(object sender, MouseWheelEventArgs e) {
            var scv = (ScrollViewer) sender;
            scv.ScrollToVerticalOffset(scv.VerticalOffset - e.Delta);
            e.Handled = true;
        }
    }
}