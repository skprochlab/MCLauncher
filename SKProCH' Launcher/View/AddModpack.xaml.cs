﻿using System.Windows;
using Launcher.ViewModel;

namespace Launcher.View {
    /// <summary>
    ///     Логика взаимодействия для AddModpack.xaml
    /// </summary>
    public partial class AddModpack : Window {
        public AddModpack() {
            InitializeComponent();
        }

        public AddModpackVM VM => (AddModpackVM) DataContext;
    }
}