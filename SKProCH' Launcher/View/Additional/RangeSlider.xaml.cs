﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Controls {
    /// <summary>
    ///     Логика взаимодействия для RangeSlider.xaml
    /// </summary>
    public partial class RangeSlider {
        public delegate void OnChange();

        public static readonly DependencyProperty MaximumProperty = DependencyProperty.Register("Maximum",
            typeof(double), typeof(RangeSlider),
            new FrameworkPropertyMetadata(1d, FrameworkPropertyMetadataOptions.AffectsMeasure,
                (o, args) => (o as RangeSlider)
                  ?.ValidateValues()));

        public static readonly DependencyProperty MinimumProperty = DependencyProperty.Register("Minimum",
            typeof(double), typeof(RangeSlider),
            new FrameworkPropertyMetadata(0d, FrameworkPropertyMetadataOptions.AffectsMeasure,
                (o, args) => (o as RangeSlider)
                  ?.ValidateValues()));

        public static readonly DependencyProperty StartProperty = DependencyProperty.Register("Start", typeof(double),
            typeof(RangeSlider),
            new FrameworkPropertyMetadata(0d,
                FrameworkPropertyMetadataOptions.AffectsMeasure |
                FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public static readonly DependencyProperty EndProperty = DependencyProperty.Register("End", typeof(double),
            typeof(RangeSlider),
            new FrameworkPropertyMetadata(0d,
                FrameworkPropertyMetadataOptions.AffectsMeasure |
                FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public static readonly DependencyProperty TickPeriodicityProperty = DependencyProperty.Register(
            "TickPeriodicity", typeof(double), typeof(RangeSlider),
            new FrameworkPropertyMetadata(0d,
                FrameworkPropertyMetadataOptions.AffectsMeasure |
                FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        private MouseButton? pressedMouseButton;

        public RangeSlider() {
            InitializeComponent();
            Loaded += (sender, args) => {
                RecalcRange(Start, End);
                MinSlider.ValueChanged += MinSlider_ValueChanged;
                MaxSlider.ValueChanged += MaxSlider_ValueChanged;
            };
        }

        public double Maximum {
            get => (double) GetValue(MaximumProperty);
            set => SetValue(MaximumProperty, value);
        }

        public double Minimum {
            get => (double) GetValue(MinimumProperty);
            set => SetValue(MinimumProperty, value);
        }

        public double Start {
            get => (double) GetValue(StartProperty);
            set => SetValue(StartProperty, value);
        }

        public double End {
            get => (double) GetValue(EndProperty);
            set => SetValue(EndProperty, value);
        }

        public double TickPeriodicity {
            get => (double) GetValue(TickPeriodicityProperty);
            set => SetValue(TickPeriodicityProperty, value);
        }

        private void ValidateValues() {
            Start = Math.Min(Math.Max(Start, Minimum), Maximum);
            End = Math.Max(Minimum, Math.Min(End, Maximum));

            RecalcRange(Start, End);
        }

        private void RecalcRange(double start, double end) {
            if (start != end) {
                var width = RangeGrid.ActualWidth;
                var range = Maximum - Minimum;
                SelectionRange.Margin =
                    new Thickness(start / range * width, 0, width - end / range * width, 0);
            }
            else {
                SelectionRange.Margin = new Thickness(9999999, 0, 0, 999999);
            }
        }

        private void MinSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e) {
            if (e.NewValue > End)
                End = e.NewValue;
            RecalcRange(e.NewValue, End);
        }

        private void MaxSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e) {
            if (e.NewValue < Start)
                Start = e.NewValue;
            RecalcRange(Start, e.NewValue);
        }

        private void Border_StartMouseDown(object sender, MouseButtonEventArgs e) {
            pressedMouseButton = e.ChangedButton;
            Move(e.GetPosition(sender as IInputElement), (sender as Border).ActualWidth);
        }


        private void Border_PreviewMouseMove(object sender, MouseEventArgs e) {
            if (pressedMouseButton != null)
                Move(e.GetPosition(sender as IInputElement), (sender as Border).ActualWidth);
        }

        private void Move(Point pos, double width) {
            var newPos = Math.Round(pos.X / width * (Maximum - Minimum) / TickPeriodicity) * TickPeriodicity;

            switch (pressedMouseButton) {
                case MouseButton.Left:
                    Start = newPos;
                    break;
                case MouseButton.Right:
                    End = newPos;
                    break;
                case MouseButton.Middle:
                    break;
                case MouseButton.XButton1:
                    break;
                case MouseButton.XButton2:
                    break;
                case null:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void Border_MouseUp(object sender, MouseButtonEventArgs e) {
            pressedMouseButton = null;
        }

        private void Border_StopMouseTracking(object sender, MouseEventArgs e) {
            pressedMouseButton = null;
        }
    }
}