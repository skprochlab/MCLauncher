using System.Windows;
using System.Windows.Controls;

namespace Launcher.View.Additional {
    public class SettingsPanel : ContentControl {
        public static readonly DependencyProperty TitleProperty = DependencyProperty.Register(
            "Title", typeof(string), typeof(SettingsPanel), new PropertyMetadata(default(string)));

        public string Title {
            get { return (string) GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public static readonly DependencyProperty DescriptionProperty = DependencyProperty.Register(
            "Description", typeof(string), typeof(SettingsPanel), new PropertyMetadata(default(string)));

        public string Description {
            get { return (string) GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }
    }
}