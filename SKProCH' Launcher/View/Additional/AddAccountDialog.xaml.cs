﻿using System;
using System.Windows.Controls;
using Launcher.Model;

namespace Launcher.View.Additional {
    /// <summary>
    ///     Логика взаимодействия для AddAccountDialog.xaml
    /// </summary>
    public partial class AddAccountDialog : UserControl, IHavePassword {
        public AddAccountDialog() {
            InitializeComponent();
        }

        public string GetPassword() {
            return passBox.Password;
        }

        public void ResetPassword() {
            Dispatcher.BeginInvoke(new Action(() => passBox.Clear()));
        }
    }
}