﻿using System.Windows.Controls;
using Launcher.ViewModel;

namespace Launcher.View.Tabs {
    /// <summary>
    ///     Логика взаимодействия для AddLocalModpack.xaml
    /// </summary>
    public partial class AddLocalModpack : UserControl, IContentVMBase {
        public AddLocalModpack() {
            InitializeComponent();
        }

        public object VM {
            get => DataContext;
            set => DataContext = value;
        }
    }
}