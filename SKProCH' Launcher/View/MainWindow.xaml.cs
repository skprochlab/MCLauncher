﻿using System.Windows;
using System.Windows.Input;
using Launcher.ViewModel;

namespace Launcher {
    /// <summary>
    ///     Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        public MainWindowVM VM => (MainWindowVM) DataContext;

        #region RecieveMessage

        public void BringToFront() {
            //if (mw == null) mw = this;
            if (Tag == null) {
                if (WindowState == WindowState.Minimized)
                    WindowState = WindowState.Normal;
                var temp = Topmost;
                Topmost = true;
                Topmost = temp;
                Activate();
            }
        }

        #endregion

        private void ModpacksGrid_OnPreviewMouseWheel(object sender, MouseWheelEventArgs e) {
            //ScrollViewer scv = VisualTreeHelper.GetChild((VisualTreeHelper.GetChild((ListBox) sender, 0) as Decorator), 0) as ScrollViewer;
            //scv.ScrollToVerticalOffset(scv.VerticalOffset - e.Delta);
            //e.Handled = true;

            // Create a new event and raise it on the desired UserControl.
            var eventArg = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta) {RoutedEvent = MouseWheelEvent, Source = sender};
            var controlToScroll = ScrollContainer;
            controlToScroll.RaiseEvent(eventArg);

            e.Handled = true;
        }
    }
}