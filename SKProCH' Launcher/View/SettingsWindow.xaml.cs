﻿using System.Windows;

namespace Launcher.View {
    /// <summary>
    ///     Логика взаимодействия для SettingsWindow.xaml
    /// </summary>
    public partial class PropertyTabsContainer : Window {
        public PropertyTabsContainer() {
            InitializeComponent();
            Loaded += (sender, args) => {
                DH.DialogOpened += (o, eventArgs) => {
                    Topmost = true;
                    Activate();
                };
                DH.DialogClosing += (o, eventArgs) => { Topmost = false; };
            };
        }
    }
}