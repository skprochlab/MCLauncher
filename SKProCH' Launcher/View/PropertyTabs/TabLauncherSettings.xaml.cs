using System.Collections.Generic;
using System.Globalization;
using System.Windows.Controls;
using Rasyidf.Localization;

namespace Launcher.View.PropertyTabs {
    public partial class TabLauncherSettings : UserControl {
        public TabLauncherSettings() {
            InitializeComponent();
        }

        private void LanguagesComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (LanguagesComboBox.SelectedItem == null) return;
            var pair = LanguagesComboBox.SelectedItem is KeyValuePair<CultureInfo, LocalizationDictionary> item ? item : default;
            LocalizationService.Current.ChangeLanguage(pair.Value);
            App.Reg.SetValue("Language", pair.Value.CultureId);
        }
    }
}