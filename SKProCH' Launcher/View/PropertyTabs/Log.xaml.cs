﻿using System.Windows.Controls;
using ICSharpCode.AvalonEdit.Search;

namespace Launcher.View.PropertyTabs.Tabs {
    /// <summary>
    ///     Логика взаимодействия для Log.xaml
    /// </summary>
    public partial class TabLog : UserControl {
        public TabLog() {
            InitializeComponent();
            SearchPanel.Install(LogContainer);
        }
    }
}