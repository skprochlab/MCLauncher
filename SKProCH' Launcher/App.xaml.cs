using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using System.Xml;
using Hardcodet.Wpf.TaskbarNotification;
using ICSharpCode.AvalonEdit.Highlighting;
using ICSharpCode.AvalonEdit.Highlighting.Xshd;
using Ionic.Zip;
using Launcher.Model;
using Launcher.Model.Additional;
using Launcher.Model.Config;
using Launcher.View.Additional;
using Launcher.ViewModel;
using Launcher.ViewModel.Additional;
using MaterialDesignThemes.Wpf;
using Microsoft.Win32;
using Rasyidf.Localization;
using Sentry;
using Shared.Model;
using Shared.Palette;

namespace Launcher {
    public partial class App : Application {
        private static string LanguagesFilesPath = Path.GetFullPath(Path.Combine(@"lang"));
        public static readonly RegistryKey Reg = Registry.CurrentUser.OpenSubKey("SOFTWARE", true)?.CreateSubKey("SKProCH's Launcher");
        public static TaskbarIcon NotifyIcon;

        public static IDisposable Sentry = SentrySdk.Init(options => {
            options.Dsn = new Dsn("https://84deafe985274fe28bbfe08b4d01dace@sentry.io/1862889");
            options.Release = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            options.AttachStacktrace = true;
        });

        private static MainWindow AppMainWindow;
        private static MainWindowVM AppMainWindowVM;

        protected override void OnStartup(StartupEventArgs e) {
            FrameworkElement.StyleProperty.OverrideMetadata(typeof(Window),
                new FrameworkPropertyMetadata {DefaultValue = Current.FindResource(typeof(Window))});
            Exceptions.SetGlobalHandlers(Dispatcher.CurrentDispatcher);
            Exceptions.Restarting += (sender, args) => ExitUtilities.PrepareExit();
            SetupExitActions();
            LoadLogHighlight();


            if (IsOpenAlready()) {
                AppMessageExchange.OpenChannel().SendCommand("TryOpenMV");
                Environment.Exit(0);
            }

            #if DEBUG
            App.LanguagesFilesPath = Path.Combine(Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\..\\")), "Languages");
            #endif
            LoadCulture(LanguagesFilesPath);

            Blindly.Run(GlobalDB.UpdateAccounts);
            Blindly.Run(AppMessageExchange.SetServer);
            Blindly.Run(() => GameUtilities.DeserializeForge());
            Blindly.Run(UpdateUtilities.ClearOldFiles);
            Blindly.Run(() => Icons.CustomIcons.ToString());
            Blindly.Run(() => Icons.DefaultIcons.ToString());
            Blindly.Run(RegPaletteHelper.LoadFromReg);
            Config.Init();

            base.OnStartup(e);

            AppMainWindowVM = new MainWindowVM();
            AppMainWindow = new MainWindow {DataContext = AppMainWindowVM};
            AppMainWindow.Show();

            Task.Run(async () => {
                #if !DEBUG
                if (await UpdateUtilities.IsLauncherUpdateAvailable()) {
                    await Dispatcher.BeginInvoke(new Action(() =>
                        DialogHost
                           .Show(new UpdateAnnouncements {
                                    DataContext =
                                        new UpdateAnnouncementsVM(Assembly.GetExecutingAssembly().GetName().Version)
                                },
                                @"GlobalDialogHost")));
                }
                #endif
            });

            NotifyIcon = (TaskbarIcon) Current.FindResource(@"LauncherTray");
        }

        private void LoadLogHighlight() {
            using var stream = GetType().Assembly.GetManifestResourceStream("Launcher.Resources.Log.xshd");
            using var reader = new XmlTextReader(stream);
            HighlightingManager.Instance.RegisterHighlighting("Log", new string[0],
                HighlightingLoader.Load(reader,
                    HighlightingManager.Instance));
        }

        private static void SetupExitActions() {
            ExitUtilities.Restart += (sender, s) =>
                Process.Start(Directory.GetFiles(s, "*.exe")
                                       .FirstOrDefault(s =>
                                            Path.GetFileNameWithoutExtension(s.ToLower()).Contains(@"launcher")));

            ExitUtilities.Save += (sender, args) => { GlobalDB.Database.Dispose(); };

            ExitUtilities.Dispose += (sender, args) => {
                NotifyIcon?.Dispose();
                mutexObj?.Dispose();
                AppMessageExchange.CloseServer();
            };
        }

        public static void TryOpenVM() {
            Current.Dispatcher.Invoke(() => {
                if (WindowManager.GetWindow((AppMainWindow.DataContext as MainWindowVM).ViewID) == null) {
                    AppMainWindow = new MainWindow {DataContext = AppMainWindowVM};
                    AppMainWindow.Show();
                }

                AppMainWindow.Activate();
            });
        }

        #region One Instance

        public static Mutex mutexObj = new Mutex(false, Marshal.GetTypeLibGuidForAssembly(Assembly.GetExecutingAssembly()).ToString());

        private static bool IsOpenAlready() {
            try {
                return !mutexObj.WaitOne(TimeSpan.FromSeconds(1), false);
            }
            catch (AbandonedMutexException) {
                return false;
            }
        }

        #endregion

        #region Localization

        [Localizable(false)]
        public static string BuildCultureUrl(string culture) {
            return $"https://gitlab.com/skprochlab/MCLauncher/raw/master/SKProCH'%20Installer/Languages/{culture}.xml";
        }

        public static void DownloadAllCultures(string path) {
            Directory.CreateDirectory(path);
            #if !DEBUG
            using (var wc = new WebClient()) {
                File.Delete(Path.Combine(path, "All languages.zip"));
                wc.DownloadFile(new Uri("https://gitlab.com/skprochlab/MCLauncher/-/archive/master/MCLauncher-master.zip?path=Languages"),
                    Path.Combine(path, "All languages.zip"));
            }

            using (var zf = new ZipFile(Path.Combine(path, "All languages.zip"))) {
                zf.ToList()
                  .ForEach(zipEntry => {
                       try {
                           zipEntry.FileName = Path.GetFileName(zipEntry.FileName);
                           zipEntry.Extract(path, ExtractExistingFileAction.OverwriteSilently);
                       }
                       catch (Exception) {
                           // ignored
                       }
                   });
            }

            File.Delete(Path.Combine(path, "All languages.zip"));
            #endif
        }

        public static void LoadCulture(string path) {
            Directory.CreateDirectory(path);
            var IdFromReg = Reg.GetValue("Language") ?? Thread.CurrentThread.CurrentUICulture.IetfLanguageTag;
            var NeedAsyncRefresh = true;
            if (!(EnumerateAllCultures(path).Any(s => s.Contains(IdFromReg.ToString())) || EnumerateAllCultures(path).Any(s => s.Contains(@"en-US")))) {
                DownloadAllCultures(path);
                NeedAsyncRefresh = false;
            }

            LocalizationService.ScanLanguagesInFolder(path);
            LocalizationService.Current.Initialize(path,
                EnumerateAllCultures(path).Any(s => s.Contains((string) IdFromReg))
                    ? (string) IdFromReg
                    : @"en-US");
            if (NeedAsyncRefresh)
                Task.Run(() => {
                    DownloadAllCultures(path);
                    LocalizationService.ScanLanguagesInFolder(path);
                    LocalizationService.Current.Initialize(path,
                        EnumerateAllCultures(path).Any(s => s.Contains((string) IdFromReg))
                            ? (string) IdFromReg
                            : @"en-US");
                });
        }

        public static string[] EnumerateAllCultures(string path) {
            return Directory.GetFiles(path);
        }

        #endregion
    }
}