﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Launcher.Model;
using Launcher.Model.Config;
using Launcher.View.PropertyTabs;
using Launcher.View.PropertyTabs.Tabs;
using Launcher.ViewModel.PropertyTabs;
using MaterialDesignThemes.Wpf;
using Rasyidf.Localization;
using Shared;
using Shared.Palette;

namespace Launcher.ViewModel {
    [Magic]
    // ReSharper disable once InconsistentNaming
    internal sealed class PropertyTabsContainerVM : PropertyChangedBase, IRequireViewIdentification {
        public readonly int Id;

        public PropertyTabsContainerVM(int id = -1) {
            Id = id;
            if (id == -1) {
                MenuItemsSource = new ObservableCollection<ListAsMenu> {
                    new ListAsMenu {
                        Name = "ColorTool",
                        Description = new LocalizedString("Tabs", "ColorToolDescription", "Styles and colors"),
                        Icon = new PackIcon {Kind = PackIconKind.ColorLens},
                        Content = new ColorTool {DataContext = new ColorToolViewModel()},
                        Title = new LocalizedString("Tabs", "ColorToolTitle", "Themes")
                    },
                    new ListAsMenu {
                        Name = "Settings",
                        Description = new LocalizedString("Tabs", "SettingsDescription", "Customize behavior"),
                        Icon = new PackIcon {Kind = PackIconKind.GearOutline},
                        Content = new TabLauncherSettings {DataContext = new TabLauncherSettingsVM()},
                        Title = new LocalizedString("Tabs", "SettingsTitle", "Settings")
                    },
                    new ListAsMenu {
                        Name = "Java",
                        Description = new LocalizedString("Tabs", "JavaDescription", "Allocate RAM here"),
                        Icon = new PackIcon {Kind = PackIconKind.LanguageJava},
                        Content = new TabJavaManager {DataContext = new TabJavaManagerVM()},
                        Title = new LocalizedString("Tabs", "JavaTitle", "Java")
                    },
                    new ListAsMenu {
                        Name = "Accounts",
                        Description = new LocalizedString("Tabs", "AccountsDescription", "Account management"),
                        Icon = new PackIcon {Kind = PackIconKind.Account},
                        Content = new TabAccountManager {DataContext = new TabAccountManagerVM()},
                        Title = new LocalizedString("Tabs", "AccountsTitle", "Accounts")
                    }
                };
            }
            else {
                CurrentModpack = Config.GetModpackByUid(id);
                MenuItemsSource = new ObservableCollection<ListAsMenu> {
                    new ListAsMenu {
                        Name = "ModpackInfo",
                        Description = new LocalizedString("Tabs", "ModpackInfoDescription", "Allocate RAM here"),
                        Icon = new PackIcon {Kind = PackIconKind.Package},
                        Content = new TabModpackInfo {DataContext = new TabModpackInfoVM(Config.GetModpackByUid(Id))},
                        Title = new LocalizedString("Tabs", "ModpackInfoTitle", "Modpack")
                    },
                    new ListAsMenu {
                        Name = "Java",
                        Description = new LocalizedString("Tabs", "JavaDescription", "Allocate RAM here"),
                        Icon = new PackIcon {Kind = PackIconKind.LanguageJava},
                        Content = new TabJavaManager {DataContext = new TabJavaManagerVM(id)},
                        Title = new LocalizedString("Tabs", "JavaTitle", "Java")
                    },
                    new ListAsMenu {
                        Name = "Accounts",
                        Description = new LocalizedString("Tabs", "AccountsDescription", "Account management"),
                        Icon = new PackIcon {Kind = PackIconKind.Account},
                        Content = new TabAccountManager {DataContext = new TabAccountManagerVM(id)},
                        Title = new LocalizedString("Tabs", "AccountsTitle", "Accounts")
                    }
                };

                if (CurrentModpack.IsLogRequested)
                    MenuItemsSource.Insert(0,
                        new ListAsMenu {
                            Name = "Log",
                            Description = new LocalizedString("Tabs", "LogDescription", "Game log"),
                            Icon = new PackIcon {Kind = PackIconKind.Console},
                            Content = new TabLog {DataContext = CurrentModpack.ThisModpackTabLog},
                            Title = new LocalizedString("Tabs", "LogTitle", "Console")
                        });
                else
                    CurrentModpack.LogRequest += OnLogRequest;
            }
        }

        public ObservableCollection<ListAsMenu> MenuItemsSource { get; set; }
        public bool IsGlobal => Id == -1;
        public string DialogHostIdentifier => @"SettingsDialogHost" + (Id == -1 ? "" : Id.ToString());
        public int SelectedMenuIndex { get; set; }
        public Modpack CurrentModpack { get; set; }


        public AnotherCommandImplementation TryReset =>
            new AnotherCommandImplementation(o => {
                if (((UserControl) o)?.DataContext is IResetable canReset) canReset.ResetCommand.Execute(null);
            }, o => ((UserControl) o)?.DataContext as IResetable != null);

        public SnackbarMessageQueue MessageQueue { get; set; } = new SnackbarMessageQueue(TimeSpan.FromSeconds(30));

        public AnotherCommandImplementation CloseCommand => new AnotherCommandImplementation(o => { WindowManager.CloseWindow(ViewID); });

        public AnotherCommandImplementation PlayOffline => new AnotherCommandImplementation(o => CurrentModpack.StartMinecraft(true));

        public AnotherCommandImplementation Play => new AnotherCommandImplementation(o => { CurrentModpack.StartMinecraft(); });

        public AnotherCommandImplementation KillProcess => new AnotherCommandImplementation(o => { CurrentModpack.KillMinecraft(); });

        public ColorZone ColorZoneMode =>
            IsGlobal
                ? new ColorZone {Mode = MaterialDesignThemes.Wpf.ColorZoneMode.Accent}
                : new ColorZone {Mode = MaterialDesignThemes.Wpf.ColorZoneMode.PrimaryMid};


        public Guid ViewID { get; } = Guid.NewGuid();

        private void OnLogRequest(object sender, EventArgs e) {
            Application.Current.Dispatcher.Invoke(() => {
                MenuItemsSource.Insert(0, new ListAsMenu {
                    Name = "Log",
                    Description = new LocalizedString("Tabs", "LogDescription", "Game log"),
                    Icon = new PackIcon {Kind = PackIconKind.Console},
                    Content = new TabLog {DataContext = CurrentModpack.ThisModpackTabLog},
                    Title = new LocalizedString("Tabs", "LogTitle", "Console")
                });
            });
            CurrentModpack.LogRequest -= OnLogRequest;
        }

        public void TravelTo(string name) {
            SelectedMenuIndex = MenuItemsSource.IndexOf(MenuItemsSource.First(x => x.Name == name));
        }

        public void TravelTo(string name, string message) {
            TravelTo(name);
            MessageQueue.Enqueue(message, LocalizationService.GetString(@"Other", @"Accept", @"Accept"), () => { });
        }
    }
}