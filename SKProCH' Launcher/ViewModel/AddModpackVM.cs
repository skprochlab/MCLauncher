using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Launcher.Model.Config;
using Launcher.View;
using Launcher.View.Tabs;
using Launcher.ViewModel.Tabs;
using MaterialDesignThemes.Wpf;
using Rasyidf.Localization;
using Shared;
using SKProCHInstaller.Model;

namespace Launcher.ViewModel {
    [Magic]
    // ReSharper disable once InconsistentNaming
    public sealed class AddModpackVM : PropertyChangedBase {
        public enum AddModpackType {
            Vanilla,
            InstallLocal,
            InstallRemote,
            InstallFromCurse
        }

        public AddModpackVM() {
            MenuItemsSource = new List<ListAsMenu> {
                new ListAsMenu {
                    Name = "Local",
                    Title = LocalizationService.GetString(@"ModpackType", @"Local", @"Local modpacks"),
                    Content = new AddLocalModpack {DataContext = new AddLocalModpackVM()}
                },
                new ListAsMenu {
                    Name = "URL",
                    Title = LocalizationService.GetString(@"ModpackType", @"URL", @"Import from URL")
                },
                new ListAsMenu {
                    Name = "Curse",
                    Title = LocalizationService.GetString(@"ModpackType", @"Curse", @"Import from Curse")
                }
            };
            ImageSource = Path.Combine(Directory.GetCurrentDirectory(), @"icons\DefaultIcons\", @"Default.skplaucnhericon");
        }

        public string ModpackName { get; set; }
        public string ImageSource { get; set; }
        public string SelectedGroup { get; set; }
        public ListAsMenu ChoosedMenu { get; set; }
        public List<string> ListOfModpacksGroups => Config.LocalGroups.Select(x => x.GroupName).ToList();
        public List<ListAsMenu> MenuItemsSource { get; set; }

        public ICommand ChooseIconCommand =>
            new AnotherCommandImplementation(o => {
                var im = new IconManager {DataContext = new IconManagerVM()};
                var result = im.ShowDialog();

                if (result == true)
                    ImageSource = im.VM.ChoosenIcon.IconPath;
            });

        public AddModpackResult Result { get; private set; }

        public ICommand ContinueCommand =>
            new AnotherCommandImplementation(o => {
                AddModpackType result;
                switch (ChoosedMenu.Content) {
                    case AddLocalModpack _:
                        result = AddModpackType.Vanilla;
                        break;


                    default:
                        return;
                }

                Result = new AddModpackResult {
                    Name = ModpackName, Icon = ImageSource, GroupName = SelectedGroup, Type = result, Content =
                        ((IMenuItemVM) ((IContentVMBase) ChoosedMenu.Content).VM).Result
                };
                ((Window) o).DialogResult = true;
            }, o => {
                try {
                    return ((IMenuItemVM) ((IContentVMBase) ChoosedMenu.Content).VM).CanContinue &&
                           !string.IsNullOrWhiteSpace(ModpackName);
                }
                catch (NullReferenceException) {
                    return false;
                }
            });

        public sealed class AddModpackResult {
            public string Name { get; set; }
            public string Icon { get; set; }
            public string GroupName { get; set; }
            public object Content { get; set; }
            public AddModpackType Type { get; set; }
        }
    }

    [Magic]
    public sealed class ListAsMenu : PropertyChangedBase {
        public string Name { get; set; }

        public LocalizedString Title { get; set; }

        public LocalizedString Description { get; set; }

        public PackIcon Icon { get; set; }

        public object Content { get; set; }

        public bool IsEnabled => Content != null;
    }

    // ReSharper disable once InconsistentNaming
    public interface IContentVMBase {
        // ReSharper disable once InconsistentNaming
        object VM { get; set; }
    }

    // ReSharper disable once InconsistentNaming
    public interface IMenuItemVM {
        bool CanContinue { get; set; }
        object Result { get; set; }
    }
}