﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Windows.Controls;
using Launcher.Model;
using Launcher.Model.Config;
using Launcher.View;
using Rasyidf.Localization;
using Shared;
using Shared.Model;

namespace Launcher.ViewModel {
    [Magic]
    // ReSharper disable once InconsistentNaming
    public sealed class MainWindowVM : InheritedFromConfig, IRequireViewIdentification {
        public MainWindowVM() {
            SingleSelectionGroup.AddPropertyNameToRaise(@"Modpacks", @"SelectedModpack", RaisePropertyChanged);
        }

        public ObservableCollection<ModpacksGroup> LocalModpacks => Config.LocalGroups;

        public Modpack SelectedModpack => (Modpack) SingleSelectionGroup.GetSelectedContentByGroup("Modpacks");

        public AnotherCommandImplementation AddModpackCommand { get; } =
            new AnotherCommandImplementation(o => {
                var am = new AddModpack {DataContext = new AddModpackVM()};
                am.ShowDialog();
                if (am.DialogResult == null || !(bool) am.DialogResult) return;
                switch (am.VM.Result.Type) {
                    case AddModpackVM.AddModpackType.Vanilla:
                        Config.LocalModpacks.Add(new Modpack {
                            ModpackName = am.VM.Result.Name,
                            ModpackIcon = am.VM.Result.Icon,
                            GroupName = am.VM.Result.GroupName
                        });
                        Config.LocalModpacks[Config.LocalModpacks.Count - 1]
                              .InstallMinecraft((string) am.VM.Result.Content);
                        break;
                    case AddModpackVM.AddModpackType.InstallLocal:
                        break;
                    case AddModpackVM.AddModpackType.InstallRemote:
                        break;
                    case AddModpackVM.AddModpackType.InstallFromCurse:
                        break;
                    default:
                        return;
                }

                Config.LocalModpacks[Config.LocalModpacks.Count - 1].Save();
            });


        public AnotherCommandImplementation ChangeModpackIconCommand =>
            new AnotherCommandImplementation(o => {
                var im = new IconManager {DataContext = new IconManagerVM()};
                var result = im.ShowDialog();
                if (result != true) return;
                ((Modpack) o).ModpackIcon = im.VM.ChoosenIcon.IconPath;
                ((Modpack) o).Save();
            });

        public AnotherCommandImplementation Play => new AnotherCommandImplementation(o => { SelectedModpack.StartMinecraft(); });

        public AnotherCommandImplementation PlayOffline => new AnotherCommandImplementation(o => { SelectedModpack.StartMinecraft(true); });

        public AnotherCommandImplementation ShowGlobalSettings => new AnotherCommandImplementation(o => { WindowManager.PropertyTabsContainer.TryOpen(); });

        public AnotherCommandImplementation OpenFolder => new AnotherCommandImplementation(o => {
            Process.Start("explorer.exe", Path.Combine(Directory.GetCurrentDirectory(), o.ToString()));
        });

        public AnotherCommandImplementation ShowLog =>
            new AnotherCommandImplementation(o => WindowManager.PropertyTabsContainer.TravelTo(SelectedModpack.Id, @"Log"));

        public AnotherCommandImplementation KillProcess => new AnotherCommandImplementation(o => { SelectedModpack.KillMinecraft(); });

        public Guid ViewID { get; } = Guid.NewGuid();
    }

    public sealed class NotEmptyValidationRule : ValidationRule {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo) {
            return string.IsNullOrWhiteSpace((value ?? "").ToString())
                ? new ValidationResult(false, LocalizationService.GetString(@"ValidationRule", @"EmptyField", @"This field must not be empty"))
                : ValidationResult.ValidResult;
        }
    }
}