using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Rasyidf.Localization;
using Shared;

namespace Launcher.ViewModel.Tabs {
    [Magic]
    internal class AddLocalModpackVM : PropertyChangedBase, IMenuItemVM {
        public AddLocalModpackVM() {
            foreach (var versionType in GameUtilities.GetMinecraftManifest().Versions.GroupBy(version => version.Type).Select(versions => versions.First())) {
                var pair = new CustomBoolValuePair {Value = versionType.Type == "release", Key = versionType.Type};
                pair.PropertyChanged += (sender, args) => UpdateVersions();
                MinecraftReleaseTypes.Add(pair);
            }

            UpdateVersions();
        }

        public List<MinecraftVersionItem> VersionsList { get; set; }
        public object SelectedItem { get; set; }

        public ObservableCollection<CustomBoolValuePair> MinecraftReleaseTypes { get; set; } = new ObservableCollection<CustomBoolValuePair>();

        public bool CanContinue {
            get => SelectedItem != null;
            set => throw new NotSupportedException();
        }

        public object Result {
            get => ((MinecraftVersionItem) SelectedItem).Version;
            set => throw new NotSupportedException();
        }

        public void UpdateVersions() {
            VersionsList = GameUtilities.GetMinecraftManifest()
                                        .Versions.Where(e => MinecraftReleaseTypes.First(pair => pair.Key == e.Type).Value)
                                        .Select(i => new MinecraftVersionItem {
                                             Version = i.Type == "release" ? i.Id : $@"  {i.Id}", ReleaseDate = i.ReleaseTime.ToString().Remove(10),
                                             Type = i.Type
                                         })
                                        .ToList();
        }

        [Magic]
        public class CustomBoolValuePair : PropertyChangedBase {
            public bool Value { get; set; }
            public string Key { get; set; }
            public string LocalizedKey => LocalizationService.GetString(@"MinecraftReleaseType", Key?.Replace("_", ""), Key);
        }

        [Magic]
        public sealed class MinecraftVersionItem : PropertyChangedBase {
            private string _version;
            public bool IsForgeAvailable { get; set; }

            public string Version {
                get => _version;
                set {
                    _version = value;
                    Task.Run(() => IsForgeAvailable = GameUtilities.IsForgeExists(Version));
                }
            }

            public string ReleaseDate { get; set; }
            public string Type { get; set; }
        }
    }
}