﻿using System;
using System.Windows.Controls;
using Launcher.Model;
using MaterialDesignThemes.Wpf;
using Rasyidf.Localization;
using Shared;
using SKProCHInstaller.Model;

namespace Launcher.ViewModel.Additional {
    [Magic]
    // ReSharper disable once InconsistentNaming
    internal sealed class AddAccountDialogVM : PropertyChangedBase {
        private string _errorText;

        public AddAccountDialogVM() { }

        public AddAccountDialogVM(int id, string message = null) {
            var editableAccount = AccountStorage.Accounts.FindById(id);
            ErrorText = message;

            if (editableAccount.IsLicensed) {
                IsAddingLegacy = true;
                LoginText = editableAccount.Login;
                EditableAccountBase = editableAccount;
            }
            else {
                IsAddingLegacy = false;
                LoginText = editableAccount.Nickname;
                Uuid = editableAccount.Uuid;
            }
        }

        public bool IsAddingLegacy { get; set; } = true;

        public bool IsError { get; set; }

        public string ErrorText {
            get => _errorText;

            set {
                _errorText = value;
                IsError = !string.IsNullOrWhiteSpace(value);
            }
        }

        public string LoginText { get; set; }
        public string Uuid { get; set; }

        public AccountBase EditableAccountBase { get; set; }
        public bool IsEdit => EditableAccountBase != null;

        public AnotherCommandImplementation TryContinue =>
            new AnotherCommandImplementation(async o => {
                WPFUtils.ForceValidateBindings<TextBox>(o, TextBox.TextProperty);

                if (IsAddingLegacy ? HasCredentialsError : HasNickError || HasUUIDError)
                    return;
                ErrorText = "";
                if (IsAddingLegacy) {
                    if (!(o is IHavePassword password))
                        return;

                    try {
                        var auth = await AccountStorage.AddLicensed(LoginText, password.GetPassword(), EditableAccountBase?.Id);

                        if (!auth.IsSuccess) {
                            password.ResetPassword();
                            ErrorText = auth.Error.ErrorMessage;
                            return;
                        }
                    }
                    catch (Exception e) {
                        password.ResetPassword();
                        ErrorText = LocalizationService.GetString(@"Account", @"InnerAddError",
                            @"Inner program error (or Minecraft not purchased):") + Environment.NewLine + e;
                        return;
                    }
                }
                else {
                    AccountStorage.AddUnlicensed(LoginText, Uuid);
                }

                DialogHost.CloseDialogCommand.Execute(true, null);
            });

        public bool HasCredentialsError { get; set; }

        public bool HasNickError { get; set; }

        public bool HasUUIDError { get; set; }
    }
}