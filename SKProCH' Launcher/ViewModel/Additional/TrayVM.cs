﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Launcher.Model.Config;
using Shared;
using Shared.Model;

namespace Launcher.ViewModel.Additional {
    [Magic]
    // ReSharper disable once InconsistentNaming
    internal sealed class TrayVM : PropertyChangedBase {
        public TrayVM() {
            Config.FavouritesChanged += (sender, args) => UpdateModpackList();
            GlobalDB.HistoryChanged += (sender, args) => UpdateModpackList();
            UpdateModpackList();
        }

        public ObservableCollection<object> ModpackList { get; set; } = new ObservableCollection<object>();

        public AnotherCommandImplementation ExitCommand => new AnotherCommandImplementation(o => { ExitUtilities.InitiateExit(); });

        // ReSharper disable once InconsistentNaming
        public AnotherCommandImplementation TryOpenMWCommand => new AnotherCommandImplementation(o => App.TryOpenVM());

        public AnotherCommandImplementation RestartCommand => new AnotherCommandImplementation(o => ExitUtilities.SmartRestart());

        private void UpdateModpackList() {
            Task.Run(() => {
                var selected = new List<Modpack>();

                foreach (var favourite in Config.FindFavourites()) selected.Add(favourite);

                foreach (var entity in GlobalDB.ModpacksHistory.FindAll().ToList())
                    if (selected.All(modpack => modpack?.Id.ToString() != entity))
                        selected.Add(Config.GetModpackByUid(Convert.ToInt32(entity.Value)));

                selected = selected.Where(modpack => modpack != null).ToList();

                selected.Reverse();

                Application.Current.Dispatcher.BeginInvoke(new Action(() => {
                    var list = new ObservableCollection<object>();
                    foreach (var modpack in selected)
                        list.Add(new MenuItem {
                            Header = modpack.ModpackName,
                            Icon = new Image {Source = new BitmapImage(new Uri(modpack.ModpackIcon))},
                            Command = new AnotherCommandImplementation(o => modpack.StartMinecraft())
                        });

                    if (selected.Count != 0)
                        list.Add(new Separator());
                    ModpackList = list;
                }));
            });
        }
    }
}