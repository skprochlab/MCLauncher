﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Ionic.Zip;
using Launcher.Model;
using Shared;
using Shared.Model;

namespace Launcher.ViewModel.Additional {
    [Magic]
    internal class UpdateAnnouncementsVM : PropertyChangedBase {
        private Version _actualVersion;

        [Localizable(false)]
        public UpdateAnnouncementsVM(Version currentVersion) {
            CurrentVersion = currentVersion;
            List<Release> manifest = null;
            Task tsk = Task.Run(async () => manifest = await UpdateUtilities.GetManifest());
            tsk.Wait();

            Task.Run(() => {
                ActualVersion = Version.Parse(Regex.IsMatch(manifest[0].TagName, @"\d+.\d+.\d+")
                    ? Regex.Match(manifest[0].TagName, @"\d+.\d+.\d+").Value
                    : Regex.Match(manifest[0].TagName, @"\d+.\d+").Value);
                Versions = manifest
                          .Where(release => Version.Parse(Regex.IsMatch(release.TagName, @"\d+.\d+.\d+")
                                                        ? Regex.Match(release.TagName, @"\d+.\d+.\d+").Value
                                                        : Regex.Match(release.TagName, @"\d+.\d+").Value)
                                                   .CompareTo(currentVersion) > 0)
                          .OrderByDescending(r => r.TagName)
                          .Select(release => new PairsForListBox {
                               Title = release.TagName,
                               Description = release.Description.Remove(0, release.Description.IndexOf("\r\n*", StringComparison.Ordinal) + 2)
                           })
                          .ToList();
            });
        }

        public Version CurrentVersion { get; set; }

        public Version ActualVersion {
            get => _actualVersion;
            set {
                _actualVersion = value;
                ActualVersionString = value.ToString();
            }
        }

        public string ActualVersionString { get; set; }

        public List<PairsForListBox> Versions { get; set; }
        public bool IsProcessing { get; set; } = false;

        public AnotherCommandImplementation UpdateViaInstaller =>
            new AnotherCommandImplementation(o => {
                Task.Run(async () => {
                    var installer = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, @"*.exe")
                                             .FirstOrDefault(s => Path.GetFileNameWithoutExtension(s) == @"SKProCH's Installer");
                    if (installer != null) {
                        Process.Start(installer);
                        ExitUtilities.InitiateExit();
                    }

                    var manifest = await UpdateUtilities.GetManifest();
                    using (var wc = new WebClient()) {
                        wc.DownloadProgressChanged += (sender, args) => Progress = args.ProgressPercentage;
                        await wc.DownloadFileTaskAsync(manifest[0].Assets.Links.First(link => link.Name.Contains(@"Installer")).Url,
                            Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"SKProCH's Installer.exe"));
                        Process.Start(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"SKProCH's Installer.exe"));
                        ExitUtilities.InitiateExit();
                    }
                });
            });

        public AnotherCommandImplementation UpdateByself =>
            new AnotherCommandImplementation(async o => {
                var dir = AppDomain.CurrentDomain.BaseDirectory;
                Directory.CreateDirectory("old");
                foreach (var file in Directory.GetFiles(dir)) {
                    if (file.Contains(@"SKProCH's Installer.exe")) continue;
                    try {
                        File.Delete(file);
                    }
                    catch (Exception) {
                        try {
                            File.Move(file, Path.Combine("old", Path.GetFileNameWithoutExtension(file) + ".bak"));
                        }
                        catch (Exception) {
                            // ignored
                        }
                    }
                }

                var latest = (await UpdateUtilities.GetManifest())[0]
                            .Assets.Links.First(link => link.Name.Contains(@"SKProCH's Launcher") || link.Name.Contains(@"SKProCH' Launcher"));
                using (var wc = new WebClient()) {
                    wc.DownloadProgressChanged += (sender, args) => Progress = args.ProgressPercentage;
                    await wc.DownloadFileTaskAsync(latest.Url, Path.Combine(dir, "temp.zip"));
                }

                using (var zf = ZipFile.Read(Path.Combine(dir, "temp.zip"))) {
                    foreach (var zipEntry in zf.Entries)
                        try {
                            zipEntry.Extract(dir);
                        }
                        catch (Exception) {
                            // ignored
                        }
                }

                File.Delete(Path.Combine(dir, "temp.zip"));

                ExitUtilities.SmartRestart();
            });

        public int Progress { get; set; }
    }

    [Magic]
    public sealed class PairsForListBox : PropertyChangedBase {
        public string Title { get; set; }
        public string Description { get; set; }
    }
}