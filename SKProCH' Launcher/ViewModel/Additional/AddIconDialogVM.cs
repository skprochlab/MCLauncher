﻿using System.Windows.Input;
using Launcher.Model;
using MaterialDesignThemes.Wpf;
using Shared;

namespace Launcher.ViewModel.Additional {
    [Magic]
    // ReSharper disable once InconsistentNaming
    internal sealed class AddIconDialogVM : PropertyChangedBase {
        public string IconName { get; set; }

        public string IconPath { get; set; }

        public ICommand FindFile => new AnotherCommandImplementation(o => { IconPath = Utilities.FindImagesFile(); });

        public ICommand ProcessIcon =>
            new AnotherCommandImplementation(o => { DialogHost.CloseDialogCommand.Execute($@"{IconName} {IconPath}", null); },
                o => !string.IsNullOrWhiteSpace(IconPath) && !string.IsNullOrWhiteSpace(IconName));
    }
}