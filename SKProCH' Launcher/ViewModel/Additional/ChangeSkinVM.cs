using System;
using System.Windows.Controls;
using Launcher.Model;
using MaterialDesignThemes.Wpf;
using MojangSharp.Endpoints;
using Shared;
using SKProCHInstaller.Model;

namespace Launcher.ViewModel.Additional {
    [Magic]
    public class ChangeSkinVM : PropertyChangedBase {
        public AccountBase Account;

        public ChangeSkinVM(AccountBase accountBase) {
            Account = accountBase;
        }

        public string SkinURL { get; set; }

        public AnotherCommandImplementation Start =>
            new AnotherCommandImplementation(async o => {
                WPFUtils.ForceValidateBindings<TextBox>(o, TextBox.TextProperty);
                if (HasError) return;

                IsError = false;
                try {
                    var skin = await new UploadSkin(Account.AccessToken, Account.Uuid,
                            new Uri(SkinURL), IsSlim)
                       .PerformRequestAsync();
                    if (skin.IsSuccess) {
                        DialogHost.CloseDialogCommand.Execute(null, null);
                    }
                    else {
                        IsError = true;
                        ErrorText = skin.Error.ToString();
                    }
                }
                catch (UriFormatException u) {
                    IsError = true;
                    ErrorText = u.Message;
                }
                catch (Exception e) {
                    IsError = true;
                    ErrorText = e.ToString();
                }
            });

        public bool IsSlim { get; set; }

        public bool IsError { get; set; }

        public string ErrorText { get; set; }

        public bool HasError { get; set; }
    }
}