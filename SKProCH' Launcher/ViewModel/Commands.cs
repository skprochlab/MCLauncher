﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Launcher.ViewModel {
    #region RelayCommand

    public sealed class RelayCommand : ICommand {
        private readonly Predicate<object> _canExecute;
        private readonly Action<object> _execute;

        public RelayCommand(Predicate<object> canExecute, Action<object> execute) {
            _canExecute = canExecute;
            _execute = execute;
        }

        public event EventHandler CanExecuteChanged {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }

        public bool CanExecute(object parameter) {
            return _canExecute(parameter);
        }

        public void Execute(object parameter) {
            _execute(parameter);
        }
    }

    #endregion

    #region AsyncCommand

    public sealed class AsyncCommand : IAsyncCommand {
        private readonly Func<bool> _canExecute;
        private readonly IErrorHandler _errorHandler;
        private readonly Func<Task> _execute;

        private bool _isExecuting;

        public AsyncCommand(
            Func<Task> execute,
            Func<bool> canExecute = null,
            IErrorHandler errorHandler = null) {
            _execute = execute;
            _canExecute = canExecute;
            _errorHandler = errorHandler;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute() {
            return !_isExecuting && (_canExecute?.Invoke() ?? true);
        }

        public async Task ExecuteAsync() {
            if (CanExecute())
                try {
                    _isExecuting = true;
                    await _execute();
                }
                finally {
                    _isExecuting = false;
                }

            RaiseCanExecuteChanged();
        }

        public void RaiseCanExecuteChanged() {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        #region Explicit implementations

        bool ICommand.CanExecute(object parameter) {
            return CanExecute();
        }

        void ICommand.Execute(object parameter) {
            ExecuteAsync().FireAndForgetSafeAsync(_errorHandler);
        }

        #endregion
    }

    public sealed class AsyncCommand<T> : IAsyncCommand<T> {
        private readonly Func<T, bool> _canExecute;
        private readonly IErrorHandler _errorHandler;
        private readonly Func<T, Task> _execute;

        private bool _isExecuting;

        public AsyncCommand(Func<T, Task> execute, Func<T, bool> canExecute = null, IErrorHandler errorHandler = null) {
            _execute = execute;
            _canExecute = canExecute;
            _errorHandler = errorHandler;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(T parameter) {
            return !_isExecuting && (_canExecute?.Invoke(parameter) ?? true);
        }

        public async Task ExecuteAsync(T parameter) {
            if (CanExecute(parameter))
                try {
                    _isExecuting = true;
                    await _execute(parameter);
                }
                finally {
                    _isExecuting = false;
                }

            RaiseCanExecuteChanged();
        }

        public void RaiseCanExecuteChanged() {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        #region Explicit implementations

        bool ICommand.CanExecute(object parameter) {
            return CanExecute((T) parameter);
        }

        void ICommand.Execute(object parameter) {
            ExecuteAsync((T) parameter).FireAndForgetSafeAsync(_errorHandler);
        }

        #endregion
    }

    public interface IAsyncCommand : ICommand {
        Task ExecuteAsync();
        bool CanExecute();
    }

    public interface IAsyncCommand<T> : ICommand {
        Task ExecuteAsync(T parameter);
        bool CanExecute(T parameter);
    }

    public interface IErrorHandler {
        void HandleError(Exception ex);
    }

    public static class TaskUtilities {
        #pragma warning disable RECS0165 // Asynchronous methods should return a Task instead of void
        public static async void FireAndForgetSafeAsync(this Task task, IErrorHandler handler = null)
            #pragma warning restore RECS0165 // Asynchronous methods should return a Task instead of void
        {
            try {
                await task;
            }
            catch (Exception ex) {
                handler?.HandleError(ex);
            }
        }
    }

    #endregion
}