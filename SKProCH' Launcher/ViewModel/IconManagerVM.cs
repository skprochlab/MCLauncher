﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using Launcher.Model;
using Launcher.View.Additional;
using Launcher.ViewModel.Additional;
using MaterialDesignThemes.Wpf;
using Shared;
using Shared.Model;

namespace Launcher.ViewModel {
    [Magic]
    // ReSharper disable once InconsistentNaming
    public sealed class IconManagerVM : PropertyChangedBase {
        public IconData ChoosenIcon;

        public IconManagerVM() {
            DefaultIconsList = Icons.DefaultIcons;
            CustomIconsList = Icons.CustomIcons;
            Icons.CustomIconsChanged += (sender, set) => CustomIconsList = set;
        }

        public HashSet<IconData> DefaultIconsList { get; set; }
        public HashSet<IconData> CustomIconsList { get; set; }
        public bool CanContinue { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();

        public ICommand ChooseIcon =>
            new AnotherCommandImplementation(o => {
                ChoosenIcon = (IconData) SingleSelectionGroup.GetSelectedContentByGroup(UUID);
                if (SingleSelectionGroup.GetSelectedListBoxName(UUID).Contains("Default"))
                    Icons.PrepareDefaultIcon(ChoosenIcon);
                ((Window) o).DialogResult = true;
            });


        public ICommand AddIconCommand =>
            new AnotherCommandImplementation(async o => {
                var view = new AddIconDialog {
                    DataContext = new AddIconDialogVM()
                };

                var result = await DialogHost.Show(view, @"RootDialog");
                if (!(result is string s)) return;
                var iconName = s.Split(' ')[0];
                var iconPath = s.Split(' ')[1];
                Icons.PrepareCustomIcon(iconName, iconPath);
            });

        public ICommand Close => new AnotherCommandImplementation(o => ((Window) o).DialogResult = false);
    }
}