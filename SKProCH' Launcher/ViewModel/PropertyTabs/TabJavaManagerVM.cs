﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Launcher.Model;
using Launcher.Model.Config;
using Launcher.Model.Java;
using MaterialDesignThemes.Wpf;
using Microsoft.Win32;
using Rasyidf.Localization;
using Shared;
using Shared.Model;
using Utilities = Launcher.Model.Utilities;

namespace Launcher.ViewModel.PropertyTabs {
    [SuppressMessage("ReSharper", "PossibleInvalidOperationException")]
    [Magic]
    // ReSharper disable once InconsistentNaming
    internal sealed class TabJavaManagerVM : PropertyChangedBase, IResetable {
        private readonly double _totalMem = Utilities.TotalRam.Value;

        private readonly int uid;

        private double _maxMem;
        private double _minMem;
        private CanOverrideClass<JavaInfo> _selectedItem;
        private bool isJavaRefreshing;

        public TabJavaManagerVM(int uid = -1) {
            this.uid = uid;

            if (!IsGlobal) {
                JavaUtils.JavasChanged += (sender, args) => RefreshJava(args);
                JavaUtils.MemChanged += (sender, args) => RefreshMem();
            }

            RefreshJava();
            RefreshMem();

            ResetCommand = new AnotherCommandImplementation(o => {
                SelectedItem = null;
                IsMemOverride = false;
                RaisePropertyChanged("MaxMem");
                RaisePropertyChanged("MinMem");
                JavaUtils.SaveJavaSettings(uid, ConstructJava());
            });
        }

        public bool IsGlobal => uid == -1;
        public bool IsMemOverride { get; set; }
        public bool IsJavaOverride { get; set; }

        public CanOverrideClass<JavaInfo> SelectedItem {
            get => _selectedItem;
            set {
                _selectedItem = value;
                if (isJavaRefreshing) return;
                RaisePropertyChanged(@"TotalMem");
                if (IsGlobal) {
                    GlobalDB.GlobalSettings.Upsert(@"GlobalJavaPath", new Entity(value?.Content?.JavaDescription));
                    JavaUtils.OnJavasChanged();
                }
                else {
                    IsJavaOverride = value != null;
                    JavaUtils.SaveJavaSettings(uid, ConstructJava());
                }
            }
        }

        private JavaInfo SelectedJava => SelectedItem?.Content;

        public List<CanOverrideClass<JavaInfo>> ListOfJavas { get; set; }

        public double MaxMem {
            get => (int) (IsMemOverride || IsGlobal ? _maxMem : (double) JavaUtils.GetGlobalJavaSetting().MaxMem);
            set {
                _maxMem = value;
                if (IsGlobal) {
                    GlobalDB.GlobalSettings.Upsert(@"GlobalMaxMem", new Entity(value));
                    JavaUtils.OnMemChanged();
                }
                else {
                    IsMemOverride = true;
                    JavaUtils.SaveJavaSettings(uid, ConstructJava());
                }
            }
        }

        public double MinMem {
            get => (int) (IsMemOverride || IsGlobal ? _minMem : (double) JavaUtils.GetGlobalJavaSetting().MinMem);
            set {
                _minMem = value;
                if (IsGlobal) {
                    GlobalDB.GlobalSettings.Upsert(@"GlobalMinMem", new Entity(value));
                    JavaUtils.OnMemChanged();
                }
                else {
                    IsMemOverride = true;
                    JavaUtils.SaveJavaSettings(uid, ConstructJava());
                }
            }
        }

        public double TotalMem => SelectedJava == null || SelectedJava.Arch != @"32" ? _totalMem : 1024;

        public AnotherCommandImplementation BrowseJava =>
            new AnotherCommandImplementation(o => {
                var dlg = new OpenFileDialog {DefaultExt = @".exe", Filter = @"Java|javaw.exe"};
                var result = dlg.ShowDialog();
                if (result != true) return;
                var path = Shared.Model.Utilities.GetDirectory(dlg.FileName);

                if (ListOfJavas.Any(Jclass => Shared.Model.Utilities.GetDirectory(Jclass.Content.JavaDescription) == path)) {
                    MessageQueue.Enqueue(LocalizationService.GetString(@"Java", @"AlreadyAdded", @"his Java runtime has already been added."),
                        LocalizationService.GetString(@"Other", @"Accept", @"Accept"), () => { });
                    return;
                }

                if (!JavaProvider.TryGetJavaFromFile(dlg.FileName, out var javaInfo)) {
                    MessageQueue.Enqueue(LocalizationService.GetString(@"Java", @"NotAJava", @"You are trying to add a program that isn't Java"),
                        LocalizationService.GetString(@"Other", @"Accept", @"Accept"), () => { });
                    return;
                }

                JavaUtils.AddCachedEntry(path);
                RefreshJava();
                JavaUtils.OnJavasChanged();
            });

        public SnackbarMessageQueue MessageQueue { get; set; } = new SnackbarMessageQueue(TimeSpan.FromSeconds(5)) {IgnoreDuplicate = true};

        public AnotherCommandImplementation OpenGlobalSettings =>
            new AnotherCommandImplementation(o => WindowManager.PropertyTabsContainer.TravelTo(-1, "Java"));

        public AnotherCommandImplementation ResetCommand { get; }

        private void RefreshJava(IEnumerable<JavaInfo> javaInfos = null) {
            isJavaRefreshing = true;

            var javas = javaInfos ?? JavaUtils.GetJavas();
            var spec = JavaUtils.GetSpecifiedJavaSettings(uid);
            var def = JavaUtils.GetSpecifiedJavaSettings(-1);
            ListOfJavas = javas.Select(jclass => new CanOverrideClass<JavaInfo> {
                                    Content = jclass,
                                    IsDefault = !IsGlobal && jclass.JavaDescription == def.JavaPath
                                })
                               .ToList();

            _selectedItem = ListOfJavas.FirstOrDefault(x => x.Content.JavaDescription == spec.JavaPath);
            RaisePropertyChanged("ListOfJavas");
            RaisePropertyChanged("SelectedItem");
            RaisePropertyChanged("TotalMem");

            isJavaRefreshing = false;
        }

        private void RefreshMem() {
            var spec = JavaUtils.GetSpecifiedJavaSettings(uid);
            var def = JavaUtils.GetSpecifiedJavaSettings(-1);
            _maxMem = (double) (spec.MaxMem ?? def.MaxMem);
            _minMem = (double) (spec.MinMem ?? def.MinMem);
            if (!IsGlobal)
                IsMemOverride = spec.MaxMem != null || spec.MinMem != null;
            RaisePropertyChanged("MaxMem");
            RaisePropertyChanged("MinMem");
        }

        private JavaControl ConstructJava() {
            return new JavaControl {
                JavaPath = SelectedJava?.JavaDescription,
                MaxMem = IsMemOverride || IsGlobal ? (int?) MaxMem : null,
                MinMem = IsMemOverride || IsGlobal ? (int?) MinMem : null
            };
        }
    }
}