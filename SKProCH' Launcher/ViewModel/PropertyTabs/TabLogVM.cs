﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using ICSharpCode.AvalonEdit.Document;
using Launcher.Model;
using Rasyidf.Localization;
using Shared;
using Shared.Model;
using Utilities = Launcher.Model.Utilities;

namespace Launcher.ViewModel.PropertyTabs.Tabs {
    [Magic]
    // ReSharper disable once InconsistentNaming
    public sealed class TabLogVM : PropertyChangedBase {
        private readonly Subject<string> _writeRequests = new Subject<string>();
        private string _pathToLogFile;

        private Process _procToAttach;

        public TabLogVM() {
            ClearLog();
            WriteRequests.BufferWhenAvailable(TimeSpan.FromMilliseconds(500))
                         .ObserveOnDispatcher()
                         .Subscribe(list => Document.Insert(Document.TextLength, string.Join(Environment.NewLine, list) + Environment.NewLine));
        }

        public IObservable<string> WriteRequests => _writeRequests.AsObservable();
        public bool CanConnectToProcess { get; set; }

        public TextDocument Document { get; set; }

        public bool IsAutoScrolling { get; set; } = true;

        public bool WrapLines {
            get => LogHorizontalScrollBarVisibility == ScrollBarVisibility.Disabled;
            set => LogHorizontalScrollBarVisibility = value ? ScrollBarVisibility.Disabled : ScrollBarVisibility.Auto;
        }

        public ScrollBarVisibility LogHorizontalScrollBarVisibility { get; set; } = ScrollBarVisibility.Auto;

        public AnotherCommandImplementation Copy =>
            new AnotherCommandImplementation(o => Clipboard.SetText(Document.Text));

        public AnotherCommandImplementation Clear => new AnotherCommandImplementation(o => ClearLog());

        [Localizable(false)]
        public AnotherCommandImplementation Upload =>
            new AnotherCommandImplementation(async o => {
                WriteToLog(LocalizationService.GetString(@"Pasteee", @"StartUpload", @"Starting upload to paste.ee"), true);
                var result = await Utilities.UploadToPasteee(Document.Text);
                if (result.Success == true) {
                    WriteToLog(LocalizationService.GetString(@"Pasteee", @"Successful", @"Log uploaded to: ") + result.Link, true);
                    WriteToLog(LocalizationService.GetString(@"Pasteee", @"ToClipboard", @"Link pasted to clipboard."), true);
                    Clipboard.SetText(result.Link.ToString());
                }
                else {
                    WriteToLog(LocalizationService.GetString(@"Pasteee", @"Fail", @"Errors occurred during download: ") +
                               string.Join("\n", result.Errors.Select(error => error.ToString())), true);
                }
            });

        public AnotherCommandImplementation ConnectToLog =>
            new AnotherCommandImplementation(o => {
                CanConnectToProcess = false;
                ReadLogFile();
            });

        public string WriteToLog(string text, bool isLauncherEntry = false) {
            if (isLauncherEntry) text = $"[{DateTime.Now.Hour}:{DateTime.Now.Minute}:{DateTime.Now.Second}] [SKProCH's Launcher]: {text}";

            _writeRequests.OnNext(text);
            //TODO Remove return value
            return text;
        }

        public void ClearLog() {
            Application.Current.Dispatcher?.Invoke(() => {
                    Document = new TextDocument();
                    WriteToLog(
                        $@"Using SKProCH's Launcher v{Assembly.GetEntryAssembly()?.GetName().Version}{Environment.NewLine}{Environment.NewLine}",
                        true);
                }
            );
        }


        public void AttachToFile(Process proc, string pathToFile) {
            _procToAttach = proc;
            _pathToLogFile = pathToFile;
            CanConnectToProcess = true;
        }

        private void ReadLogFile() {
            Task.Run(() => {
                long latestSize = 0;
                long lastPos = 0;
                try {
                    while (!_procToAttach.HasExited) {
                        var f = new FileInfo(_pathToLogFile);
                        if (f.Length.Equals(latestSize)) continue;
                        latestSize = f.Length;
                        var fs = new FileStream(_pathToLogFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                        using (var reader = new StreamReader(fs)) {
                            reader.BaseStream.Position = lastPos;
                            foreach (var s in reader.ReadToEnd().SplitByLines()) WriteToLog(s);

                            lastPos = reader.BaseStream.Position;
                        }

                        Task.Delay(1000);
                    }
                }
                catch (Exception) {
                    // ignored
                }
            });
        }
    }
}