using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Controls;
using Launcher.Model.Config;
using Launcher.View;
using Rasyidf.Localization;
using Shared;
using SKProCHInstaller.Model;

namespace Launcher.ViewModel.PropertyTabs {
    public class TabModpackInfoVM {
        private string _modpackName;

        public TabModpackInfoVM(Modpack modpack) {
            Modpack = modpack;
            _modpackName = Modpack.ModpackName;
        }

        public Modpack Modpack { get; set; }

        public string ModpackName {
            get => _modpackName;
            set {
                if (Path.Combine("instances", Modpack.ModpackName) == Path.Combine("instances", value)) return;
                _modpackName = value;
                if (!NameHasError)
                    Task.Run(async () => {
                        try {
                            await Modpack.Semaphore.WaitAsync();
                            Directory.Move(Path.Combine("instances", Modpack.ModpackName), Path.Combine("instances", value));
                            Modpack.ModpackName = value;
                            Modpack.Save();
                        }
                        finally {
                            Modpack.Semaphore.Release();
                        }
                    });
            }
        }

        public AnotherCommandImplementation ChooseIconCommand =>
            new AnotherCommandImplementation(o => {
                var im = new IconManager {DataContext = new IconManagerVM()};
                var result = im.ShowDialog();
                if (result != true) return;
                Modpack.ModpackIcon = im.VM.ChoosenIcon.IconPath;
            });

        public bool NameHasError { get; set; }
        public string OldName => string.Format(LocalizationService.GetString(@"Modpack", @"Old", @"Old: {0}"), Modpack.ModpackName);
        public string OldGroup => string.Format(LocalizationService.GetString(@"Modpack", @"Old", @"Old: {0}"), Modpack.GroupName);
        public List<string> ListOfModpacksGroups => Config.LocalGroups.Select(x => x.GroupName).ToList();
    }

    public class ModpackNameChangeRule : ValidationRule {
        public Wrapper Wrapper { get; set; }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo) {
            if ((string) Wrapper?.Format == (string) value)
                return ValidationResult.ValidResult;

            if (value == null)
                return new ValidationResult(false, LocalizationService.GetString(@"ValidationRule", @"EmptyField", @"This field must not be empty"));

            var path = Path.Combine("instances", value.ToString());

            if (Directory.Exists(path))
                return new ValidationResult(false, LocalizationService.GetString(@"ValidationRule", @"AlreadyExists", @"This value already exists."));

            try {
                Directory.CreateDirectory(path);
                Directory.Delete(path);
            }
            catch (Exception) {
                return new ValidationResult(false, LocalizationService.GetString(@"ValidationRule", @"WrongChars", @"Value contains invalid characters"));
            }

            return ValidationResult.ValidResult;
        }
    }
}