﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Launcher.Model;
using Launcher.Model.Config;
using Launcher.View.Additional;
using Launcher.ViewModel.Additional;
using MaterialDesignThemes.Wpf;
using Shared;
using Shared.Model;

namespace Launcher.ViewModel.PropertyTabs {
    [Magic]
    internal sealed class TabAccountManagerVM : PropertyChangedBase, IResetable {
        private CanOverrideClass<AccountBase> _selectedItem;

        public TabAccountManagerVM(int uid = -1) {
            Uid = uid;
            IsGlobal = uid == -1;
            if (!IsGlobal)
                AccountStorage.Changed += (sender, args) => RefreshStorage();
            RefreshStorage();

            ResetCommand = new AnotherCommandImplementation(o => { SelectedItem = null; });
        }

        public int Uid { get; set; }
        public string DialogHostUid => $"AccountManager{Uid}";

        public bool IsGlobal { get; }

        public ObservableCollection<CanOverrideClass<AccountBase>> Storage { get; set; }

        public CanOverrideClass<AccountBase> SelectedItem {
            get => _selectedItem;
            set {
                _selectedItem = value;
                if (IsGlobal) {
                    AccountStorage.DefaultAccount = value?.Content?.Id;
                    AccountStorage.OnChanged();
                }
                else {
                    Config.GetModpackByUid(Uid).AccountId = value?.Content?.Id;
                }
            }
        }

        public AccountBase SelectedAccount => SelectedItem?.Content;

        public AnotherCommandImplementation AddAccount =>
            new AnotherCommandImplementation(async o => {
                await DialogHost.Show(new AddAccountDialog {DataContext = new AddAccountDialogVM()}, @"AccountManager-1");
                RefreshStorage();
            });

        public AnotherCommandImplementation DeleteAccount =>
            new AnotherCommandImplementation(o => {
                AccountStorage.Accounts.Delete(SelectedAccount.Id);
                RefreshStorage();
            });

        public AnotherCommandImplementation OpenGlobalSeettings =>
            new AnotherCommandImplementation(o => { WindowManager.PropertyTabsContainer.TravelTo(-1, "Accounts"); });


        public AnotherCommandImplementation ChangeSkin =>
            new AnotherCommandImplementation(o => {
                if (SelectedAccount?.IsLicensed != true) return;
                DialogHost.Show(new ChangeSkin {DataContext = new ChangeSkinVM(SelectedAccount)}, DialogHostUid);
            });

        public AnotherCommandImplementation ResetCommand { get; }

        private void RefreshStorage() {
            Task.Run(() => {
                var defaultAcc = AccountStorage.DefaultAccount;
                Storage = new ObservableCollection<CanOverrideClass<AccountBase>>(AccountStorage.Accounts.FindAll()
                                                                                                .Select((mybase, i) =>
                                                                                                     new CanOverrideClass<AccountBase>(mybase, true,
                                                                                                         !IsGlobal && mybase.Id == defaultAcc)));
                var id = IsGlobal ? AccountStorage.DefaultAccount : Config.GetModpackByUid(Uid)?.AccountId;
                _selectedItem = Storage.FirstOrDefault(o => o.Content.Id == id);
                Application.Current.Dispatcher.BeginInvoke(new Action(() => { RaisePropertyChanged("SelectedItem"); }));
            });
        }
    }
}