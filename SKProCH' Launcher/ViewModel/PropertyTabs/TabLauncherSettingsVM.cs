using System;
using System.Collections.Generic;
using System.Globalization;
using Launcher.Model;
using Launcher.Model.Config;
using Rasyidf.Localization;
using Shared;

namespace Launcher.ViewModel.PropertyTabs {
    [Magic]
    // ReSharper disable once InconsistentNaming
    public class TabLauncherSettingsVM : PropertyChangedBase {
        private bool _showConsoleAtStartup = Convert.ToBoolean(GlobalDB.GlobalSettings.FindById("ShowConsoleAtStartup") ?? "True");
        private bool _closeConsole = Convert.ToBoolean(GlobalDB.GlobalSettings.FindById("CloseConsole") ?? "False");
        public bool ShowGlobal { get; set; } = true;

        public bool ShowConsole { get; set; }

        public TabLauncherSettingsVM() {
            var i = 0;
            foreach (var variable in LocalizationService.RegisteredPacks) {
                if (variable.Key.IetfLanguageTag == LocalizationService.Current.Culture.IetfLanguageTag) {
                    SelectedLanguageIndex = i;
                    break;
                }

                i++;
            }
        }

        public IDictionary<CultureInfo, LocalizationDictionary> LanguagesList => LocalizationService.RegisteredPacks;

        public int SelectedLanguageIndex { get; set; }

        public bool ShowConsoleAtStartup {
            get => _showConsoleAtStartup;
            set {
                _showConsoleAtStartup = value;
                GlobalDB.GlobalSettings.Upsert("ShowConsoleAtStartup", new Entity(value));
            }
        }

        public bool CloseConsole {
            get => _closeConsole;
            set {
                _closeConsole = value;
                GlobalDB.GlobalSettings.Upsert("CloseConsole", new Entity(value));
            }
        }
    }
}