using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows;
using Newtonsoft.Json;
using Rasyidf.Localization;
using Shared.Model;

namespace Launcher.Model {
    public static class Icons {
        private static readonly Lazy<HashSet<IconData>> _defaultIcons = new Lazy<HashSet<IconData>>(LoadDefaultIcons);

        private static readonly Lazy<HashSet<IconData>> _customIcons = new Lazy<HashSet<IconData>>(LoadCustomItems);
        public static HashSet<IconData> DefaultIcons => _defaultIcons.Value;
        public static HashSet<IconData> CustomIcons => _customIcons.Value;
        public static event EventHandler<HashSet<IconData>> CustomIconsChanged;

        private static HashSet<IconData> LoadDefaultIcons() {
            Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), @"icons\DefaultIcons\"));

            try {
                using var wc = new WebClient();
                var icons = new HashSet<IconData>(JsonConvert.DeserializeObject<string[]>(wc.DownloadString(@"https://storage.illumined.ru/icon/"))
                                                             .Where(s => !string.IsNullOrWhiteSpace(s.Replace(@"-", "").Replace(@".", "")))
                                                             .Select(x => new IconData {
                                                                  IconName = Path.GetFileNameWithoutExtension(x).Replace(@"-", @" ").Replace(@".", ""),
                                                                  IconPath = @"https://storage.illumined.ru/icon/" + x
                                                              }));
                PrepareDefaultIcon(icons.First(data => data.IconName == @"Default"));
                return icons;
            }
            catch (Exception) {
                return Directory.GetFiles(Path.Combine(Directory.GetCurrentDirectory(), @"icons\DefaultIcons\"))
                                .Select(item => new IconData {IconName = Path.GetFileNameWithoutExtension(item), IconPath = item})
                                .ToHashSet();
            }
        }


        private static HashSet<IconData> LoadCustomItems() {
            Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), @"icons\CustomIcons\"));

            return Directory.GetFiles(Path.Combine(Directory.GetCurrentDirectory(), @"icons\CustomIcons\"))
                            .Select(item => new IconData {IconPath = item, IconName = Path.GetFileNameWithoutExtension(item)})
                            .ToHashSet();
        }

        [Localizable(false)]
        public static string PrepareDefaultIcon(IconData data, string filename = null) {
            // ReSharper disable once InvertIf
            if (!File.Exists(Path.Combine(Directory.GetCurrentDirectory(), @"icons\DefaultIcons\", $"{filename ?? data.IconName}.skplaucnhericon"))) {
                using var wc = new WebClient();
                wc.DownloadFile(new Uri(data.IconPath),
                    Path.Combine(Directory.GetCurrentDirectory(), @"icons\DefaultIcons\", $"{filename ?? data.IconName}.skplaucnhericon"));
            }

            return Path.Combine(@"icons\DefaultIcons\", $"{filename ?? data.IconName}.skplaucnhericon");
        }

        public static void PrepareCustomIcon(string iconName, string iconPath) {
            try {
                using var wc = new WebClient();
                wc.DownloadFile(new Uri(iconPath),
                    Path.Combine(Directory.GetCurrentDirectory(), @"icons\CustomIcons\", iconName + @".skplaucnhericon"));
                OnCustomIconsChanged(LoadCustomItems());
            }
            catch (Exception) {
                MessageBox.Show(LocalizationService.GetString(@"IconManager", @"FileError", @"Something is wrong with the file. check again"));
            }
        }

        private static void OnCustomIconsChanged(HashSet<IconData> e) {
            CustomIconsChanged?.Invoke(null, e);
        }
    }

    public struct IconData {
        public string IconPath { get; set; }
        public string IconName { get; set; }
    }
}