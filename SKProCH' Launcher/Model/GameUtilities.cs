﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows;
using System.Xml.Serialization;
using Ionic.Zip;
using Launcher.ForgeXML;
using Launcher.Model.Additional;
using MinecraftContent;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Rasyidf.Localization;
using SpecificForgeVersionManifest;
using SpecificMinecraftVersionManifest;
using static Launcher.Model.Utilities;
using Library = SpecificForgeVersionManifest.Library;
using Object = MinecraftAssetsManifest.Object;

namespace Launcher {
    public static class GameUtilities {
        public enum DownloadResult {
            Success,
            UpToDate,
            WrongSystem,
            DownloadFail,
            UnknownFail
        }

        public enum DownloadType {
            MinecraftAsset,
            MinecraftLib,
            MinecraftNative,
            MinecraftJar,
            ForgeLib,
            ForgeNative
        }

        public static SemaphoreSlim SemaphoreSlim = new SemaphoreSlim(1);
        private static Forge _cachedForge;
        private static MinecraftVersionsIndex _minecraftVersionsIndexCache;

        public static Forge DeserializeForge(bool cleanCache = false) {
            if (_cachedForge != null && !cleanCache) return _cachedForge;
            SemaphoreSlim.Wait();
            if (!(_cachedForge != null && !cleanCache))
                using (var wc = new WebClient()) {
                    ServicePointManager.SecurityProtocol =
                        SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                    var serializer = new XmlSerializer(typeof(Metadata), new XmlRootAttribute("metadata"));
                    var forgeXml = (Metadata) serializer.Deserialize(
                        new MemoryStream(wc.DownloadData("https://files.minecraftforge.net/maven/net/minecraftforge/forge/maven-metadata.xml")));
                    var temp = new Forge {
                        Artifact = forgeXml.ArtifactId, Numbers = new Dictionary<string, Forge.Number>(), ByMcVersions = new Dictionary<string, List<string>>()
                    };

                    foreach (var variable in forgeXml.Versioning.Versions.Version) {
                        var versions = variable.Split('-');
                        temp.Numbers.Add(versions[1], new Forge.Number {McVersion = versions[0], FullForgeID = variable});
                        if (!temp.ByMcVersions.ContainsKey(versions[0])) temp.ByMcVersions.Add(versions[0], new List<string>());
                        temp.ByMcVersions[versions[0]].Add(versions[1]);
                    }

                    _cachedForge = temp;
                }

            SemaphoreSlim.Release();
            return _cachedForge;
        }

        public static bool IsForgeExists(string version) {
            return DeserializeForge().ByMcVersions.ContainsKey(version);
        }

        // ReSharper disable once InconsistentNaming
        public static List<string> GetForgeVersionsIDByMcVersion(string version) {
            if (Convert.ToInt32(version.Split('.')[1]) < 5 || Convert.ToInt32(version.Split('.')[1]) > 12) return new List<string>();
            try {
                var result = DeserializeForge().ByMcVersions[version];
                if (Convert.ToInt32(result[0].Split('.')[2]) < Convert.ToInt32(result[result.Count - 1].Split('.')[2])) result.Reverse();
                return result;
            }
            catch (Exception) {
                return new List<string>();
            }
        }

        public static Forge.Number GetForgeVersion(string version, Forge forgeManifest = null) {
            if (forgeManifest == null) forgeManifest = DeserializeForge();
            return forgeManifest.Numbers.ContainsKey(version) ? forgeManifest.Numbers[version] : null;
        }

        public static ForgeVersion GetForgeManifest(string version, string extractPath) {
            return GetForgeManifest(GetForgeVersion(version), extractPath);
        }

        public static ForgeVersion GetForgeManifest(Forge.Number version, string extractPath) {
            var pathtotempfile = Path.ChangeExtension(Path.GetTempFileName(), "jar");
            const string baseUrl = @"http://files.minecraftforge.net/maven/net/minecraftforge/forge/";

            try {
                DownloadFile($"{baseUrl}{version.FullForgeID}/{DeserializeForge().Artifact}-{version.FullForgeID}-installer.jar", pathtotempfile);

                if (extractPath != @"false") {
                    var proc = new ProcessStartInfo {
                        UseShellExecute = true, WorkingDirectory = Path.GetTempPath(), FileName = @"java", Arguments = $"-jar {pathtotempfile} -extract",
                        WindowStyle = ProcessWindowStyle.Hidden
                    };

                    Process.Start(proc).WaitForExit();

                    foreach (var line in File.ReadAllLines(pathtotempfile + @".log"))
                        if (line.Contains(@"File extracted successfully to")) {
                            File.Move(Path.Combine(Path.GetTempPath(), line.Replace(@"File extracted successfully to ", "")), extractPath);
                            break;
                        }

                    File.Delete(pathtotempfile + @".log");
                }

                var versionJsonText = "";

                using (var zip = ZipFile.Read(pathtotempfile)) {
                    foreach (var e in zip)
                        if (e.FileName == @"install_profile.json") {
                            e.Extract(Path.GetTempPath(), ExtractExistingFileAction.OverwriteSilently);
                            versionJsonText = File.ReadAllText(Path.Combine(Path.GetDirectoryName(pathtotempfile), @"install_profile.json"));
                        }
                }

                if (versionJsonText == "") {
                    MessageBox.Show(LocalizationService.GetString(@"Other", @"ManifestRequired", @"Unable to detect manifest"));
                    return null;
                }

                var result = ForgeVersion.FromJson(versionJsonText);
                File.Delete(Path.Combine(Path.GetDirectoryName(pathtotempfile), @"install_profile.json"));
                File.Delete(pathtotempfile);
                return result;
            }
            catch (Exception e) {
                MessageBox.Show(e.Message);
            }

            return null;
        }

        public static Result InstallForgeLib(Library versionInfoLibrary) {
            try {
                if (versionInfoLibrary.Clientreq == null || versionInfoLibrary.Clientreq == true)
                    DownloadFile(GetUrl(versionInfoLibrary.Name, versionInfoLibrary.Url), Path.Combine(@"libraries/", GetPath(versionInfoLibrary.Name)));
                else
                    return new Result {
                        TotalResult = DownloadResult.WrongSystem, Path = Path.Combine(@"libraries/", GetPath(versionInfoLibrary.Name)),
                        Name = versionInfoLibrary.Name,
                        Url = GetUrl(versionInfoLibrary.Name, versionInfoLibrary.Url)
                    };
            }
            catch (Exception e) {
                try {
                    var tempFile = Path.GetTempFileName();
                    DownloadFile(GetUrl(versionInfoLibrary.Name, versionInfoLibrary.Url) + @".pack.xz", tempFile);
                    //Распаковываем файл
                    var proc = new ProcessStartInfo {
                        UseShellExecute = true, FileName = @"java",
                        Arguments =
                            $"-jar \"{Path.Combine(Directory.GetCurrentDirectory(), "unpack200.jar")}\" \"{tempFile}\" \"{Path.Combine(Directory.GetCurrentDirectory(), @"libraries/", GetPath(versionInfoLibrary.Name))}\"",
                        WindowStyle = ProcessWindowStyle.Hidden
                    };
                    Process.Start(proc).WaitForExit();
                    File.Delete(tempFile);
                }
                catch (Exception) {
                    return new Result {
                        TotalResult = DownloadResult.DownloadFail,
                        Path = Path.Combine(Directory.GetCurrentDirectory(), @"libraries/", GetPath(versionInfoLibrary.Name)),
                        Name = versionInfoLibrary.Name, Url = GetUrl(versionInfoLibrary.Name, versionInfoLibrary.Url), Error = e.Message, ST = e.StackTrace,
                        InfoObject = versionInfoLibrary
                    };
                }
            }

            //Если загрузили успешно, продолжаем
            var variable = Path.Combine(Directory.GetCurrentDirectory(), @"libraries/", GetPath(versionInfoLibrary.Name));
            var fs = new FileStream(variable, FileMode.Open);
            using var bs = new BufferedStream(fs);
            using var sha1 = new SHA1Managed();

            try {
                var fileHash = sha1.ComputeHash(bs);
                var formatted = new StringBuilder(2 * fileHash.Length);

                foreach (var b in fileHash)
                    formatted.AppendFormat(@"{0:X2}", b);

                fs.Close();
                fs.Dispose();
                var hash = formatted.ToString().ToLower();

                Directory.CreateDirectory(Path.GetDirectoryName(Path.Combine(Path.GetDirectoryName(variable).Replace(@"libraries/", @"libraries/"),
                    Path.GetFileNameWithoutExtension(variable),
                    hash + @".jar")));

                File.Move(variable,
                    Path.Combine(Path.GetDirectoryName(variable).Replace(@"libraries/", @"libraries/"), Path.GetFileNameWithoutExtension(variable),
                        hash + @".jar"));

                return new Result {
                    TotalResult = DownloadResult.Success, Path = Path.Combine(@"libraries/", GetPath(versionInfoLibrary.Name)), Name = versionInfoLibrary.Name,
                    Url = GetUrl(versionInfoLibrary.Name, versionInfoLibrary.Url)
                };
            }
            catch (Exception e) {
                return new Result {
                    TotalResult = DownloadResult.UnknownFail,
                    Path = Path.Combine(Directory.GetCurrentDirectory(), @"libraries/", GetPath(versionInfoLibrary.Name)),
                    Name = versionInfoLibrary.Name, Url = GetUrl(versionInfoLibrary.Name, versionInfoLibrary.Url), Error = e.Message, ST = e.StackTrace,
                    InfoObject = versionInfoLibrary
                };
            }
        }

        public static string GetUrl(string name, Uri url) {
            return GetUrl(name, Convert.ToString(url));
        }

        private static string GetUrl(string name, string url) {
            if (string.IsNullOrEmpty(url)) url = @"https://libraries.minecraft.net/";
            var separatedName = name.Split(':');
            return url + GetPath(name);
        }

        private static string GetPath(string name) {
            var separatedName = name.Split(':');
            return separatedName[0].Replace(@".", @"/") + @"/" + separatedName[1] + @"/" + separatedName[2] + @"/" + separatedName[1] + @"-" +
                   separatedName[2] + @".jar";
        }

        private static string GetJarName(string name) {
            return name.Split(':')[2] + @"-" + name.Split(':')[3];
        }

        public static string GetMinecraftVersionsManifest(string url = "https://launchermeta.mojang.com/mc/game/version_manifest.json") {
            using var wc = new WebClient();
            return wc.DownloadString(url);
        }

        public static MinecraftVersionsIndex GetMinecraftManifest(bool cleanCache = false, string rawjson = null) {
            if (_minecraftVersionsIndexCache != null && !cleanCache) return _minecraftVersionsIndexCache;
            if (rawjson == null) rawjson = GetMinecraftVersionsManifest();
            _minecraftVersionsIndexCache = MinecraftVersionsIndex.FromJson(rawjson);
            return _minecraftVersionsIndexCache;
        }

        [Localizable(false)]
        public static SpecificMinecraftVersion GetMinecraftVersionManifest(string version, bool hardUpdate = false, MinecraftVersionsIndex mcManifest = null) {
            if (!hardUpdate && File.Exists(Path.Combine("versions", version, $"{version}.json")))
                return SpecificMinecraftVersion.FromJson(File.ReadAllText(Path.Combine("versions", version, $"{version}.json")));

            if (mcManifest == null) mcManifest = GetMinecraftManifest();

            foreach (var variable in mcManifest.Versions.Where(variable => variable.Id == version)) {
                DownloadFile(variable.Url, Path.Combine("versions", version, $"{version}.json"));
                return SpecificMinecraftVersion.FromJson(File.ReadAllText(Path.Combine("versions", version, $"{version}.json")));
            }

            return null;
        }

        public static Result DownloadMinecraftAssets(KeyValuePair<string, Object> asset, bool hardupdate = false) {
            bool isNeedDownload;

            if (hardupdate) isNeedDownload = true;
            else if (File.Exists(Path.Combine(Directory.GetCurrentDirectory(), @"assets/objects/", asset.Value.Hash.Remove(2) + @"/" + asset.Value.Hash)))
                isNeedDownload =
                    !CompareSha1(Path.Combine(Directory.GetCurrentDirectory(), @"assets/objects/", asset.Value.Hash.Remove(2) + @"/" + asset.Value.Hash),
                        asset.Value.Hash);
            else isNeedDownload = true;

            var url = @"http://resources.download.minecraft.net/" + asset.Value.Hash.Remove(2) + @"/" + asset.Value.Hash;
            var path = Path.Combine(Directory.GetCurrentDirectory(), @"assets\objects\", asset.Value.Hash.Remove(2) + @"\" + asset.Value.Hash);
            if (!isNeedDownload) return new Result {TotalResult = DownloadResult.UpToDate, Path = path, Url = url, Name = asset.Key};

            try {
                try {
                    DownloadFile(url, path);
                }
                catch (WebException e) when (e.InnerException.GetType() == typeof(IOException)) { }

                return new Result {
                    TotalResult = DownloadResult.Success, Path = path, Url = url, Name = asset.Key,
                    DownloadType = DownloadType.MinecraftAsset
                };
            }
            catch (Exception exception) {
                return new Result {
                    TotalResult = DownloadResult.DownloadFail, Path = path, Url = url, Name = asset.Key,
                    Error = exception.Message, ST = exception.StackTrace, DownloadType = DownloadType.MinecraftAsset, InfoObject = asset
                };
            }
        }

        public static Result DownloadMinecraftLibrary(SpecificMinecraftVersionManifest.Library lib, bool hardupdate = false) {
            var path = Path.Combine(Directory.GetCurrentDirectory(), @"libraries/", lib.Downloads.Artifact.Path.Replace(@".jar", ""),
                lib.Downloads.Artifact.Sha1 + @".jar");
            bool isNeedDownload;

            if (hardupdate) isNeedDownload = true;
            else if (File.Exists(path)) isNeedDownload = !CompareSha1(path, lib.Downloads.Artifact.Sha1);
            else isNeedDownload = true;

            if (!isNeedDownload)
                return new Result {
                    TotalResult = DownloadResult.UpToDate, Path = path, Url = Convert.ToString(lib.Downloads.Artifact.Url), Name = lib.Name,
                    DownloadType = DownloadType.MinecraftLib
                };
            try {
                try {
                    DownloadFile(lib.Downloads.Artifact.Url, path);
                }
                catch (WebException e) when (e.InnerException.GetType() == typeof(IOException)) { }

                return new Result {
                    TotalResult = DownloadResult.Success, Path = path, Url = lib.Downloads.Artifact.Url.ToString(), Name = lib.Name,
                    DownloadType = DownloadType.MinecraftLib
                };
            }
            catch (Exception exception) {
                return new Result {
                    TotalResult = DownloadResult.DownloadFail, Path = path, Url = Convert.ToString(lib.Downloads.Artifact.Url), Name = lib.Name,
                    Error = exception.Message, ST = exception.StackTrace, DownloadType = DownloadType.MinecraftLib, InfoObject = lib
                };
            }
        }

        public static Result DownloadMinecraftNative(SpecificMinecraftVersionManifest.Library lib, bool hardUpdate = false) {
            //Launcher is only on Windows - we don't check other platforms
            var classifier = lib.GetClassifier();
            if (classifier == null)
                return new Result {TotalResult = DownloadResult.WrongSystem, Name = lib.Name, InfoObject = lib, DownloadType = DownloadType.MinecraftNative};
            var path = Path.Combine(Directory.GetCurrentDirectory(), @"libraries/", classifier.Path.Replace(@".jar", ""), classifier.Sha1 + @".jar");
            bool isNeedDownload;

            if (hardUpdate) isNeedDownload = true;
            else if (File.Exists(path)) isNeedDownload = !CompareSha1(path, classifier.Sha1);
            else isNeedDownload = true;

            if (!isNeedDownload)
                return new Result {
                    TotalResult = DownloadResult.UpToDate, Path = path, Url = Convert.ToString(classifier.Url), Name = lib.Name,
                    InfoObject = lib, DownloadType = DownloadType.MinecraftNative
                };

            try {
                try {
                    DownloadFile(classifier.Url, path);
                }
                catch (WebException e) when (e.InnerException.GetType() == typeof(IOException)) { }

                return new Result {
                    TotalResult = DownloadResult.Success, Path = path, Url = Convert.ToString(classifier.Url), Name = lib.Name,
                    InfoObject = lib, DownloadType = DownloadType.MinecraftNative
                };
            }
            catch (Exception exception) {
                return new Result {
                    TotalResult = DownloadResult.DownloadFail, Path = path, Url = Convert.ToString(classifier.Url), Name = lib.Name,
                    Error = exception.Message, ST = exception.StackTrace, InfoObject = lib, DownloadType = DownloadType.MinecraftNative
                };
            }
        }

        [Localizable(false)]
        public static Result DownloadMinecraftJar(ServerClass lib, bool hardUpdate = false) {
            bool isNeedDownload;

            if (hardUpdate) isNeedDownload = true;
            else if (File.Exists(lib.Path)) isNeedDownload = !CompareSha1(lib.Path, lib.Sha1);
            else isNeedDownload = true;

            if (!isNeedDownload)
                return new Result {
                    TotalResult = DownloadResult.UpToDate, Path = lib.Path, Url = Convert.ToString(lib.Url), Name = "minecraft.jar",
                    InfoObject = lib, DownloadType = DownloadType.MinecraftJar
                };
            try {
                DownloadFile(lib.Url, lib.Path);

                return new Result {
                    TotalResult = DownloadResult.Success, Path = lib.Path, Url = Convert.ToString(lib.Url), Name = "minecraft.jar",
                    InfoObject = lib, DownloadType = DownloadType.MinecraftJar
                };
            }
            catch (Exception exception) {
                return new Result {
                    TotalResult = DownloadResult.DownloadFail, Path = lib.Path, Url = Convert.ToString(lib.Url), Name = "minecraft.jar",
                    Error = exception.Message, ST = exception.StackTrace, InfoObject = lib, DownloadType = DownloadType.MinecraftJar
                };
            }
        }

        public sealed class Result {
            public DownloadType DownloadType;
            public string Error = @"None";
            public object InfoObject;
            public string Name;
            public string Path;

            // ReSharper disable once InconsistentNaming
            public string ST = @"None";
            public DownloadResult TotalResult;
            public string Url;

            public bool IsPositiveResult() {
                return TotalResult switch {
                    DownloadResult.Success      => true,
                    DownloadResult.UpToDate     => true,
                    DownloadResult.WrongSystem  => true,
                    DownloadResult.DownloadFail => false,
                    DownloadResult.UnknownFail  => false,
                    _                           => throw new ArgumentOutOfRangeException()
                };
            }
        }
    }

    public sealed class Forge {
        public string Artifact { get; set; }
        public Dictionary<string, List<string>> ByMcVersions { get; set; }
        public Dictionary<string, Number> Numbers { get; set; }

        public sealed class Number {
            public string McVersion { get; set; }

            // ReSharper disable once InconsistentNaming
            public string FullForgeID { get; set; }
        }
    }

    #region ForgeFromXML

    namespace ForgeXML {
        [XmlRoot(ElementName = "versions")]
        public class Versions {
            [XmlElement(ElementName = "version")] public List<string> Version { get; set; }
        }

        [XmlRoot(ElementName = "versioning")]
        public class Versioning {
            [XmlElement(ElementName = "release")] public string Release { get; set; }
            [XmlElement(ElementName = "versions")] public Versions Versions { get; set; }

            [XmlElement(ElementName = "lastUpdated")]
            public string LastUpdated { get; set; }
        }

        [XmlRoot(ElementName = "metadata")]
        public class Metadata {
            [XmlElement(ElementName = "groupId")] public string GroupId { get; set; }

            [XmlElement(ElementName = "artifactId")]
            public string ArtifactId { get; set; }

            [XmlElement(ElementName = "versioning")]
            public Versioning Versioning { get; set; }
        }
    }

    #endregion
}

namespace SpecificForgeVersionManifest {
    public partial class ForgeVersion {
        [JsonProperty("install", NullValueHandling = NullValueHandling.Ignore)]
        public Install Install { get; set; }

        [JsonProperty("versionInfo", NullValueHandling = NullValueHandling.Ignore)]
        public VersionInfo VersionInfo { get; set; }

        [JsonProperty("optionals", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> Optionals { get; set; }
    }

    public sealed class Install {
        [JsonProperty("profileName", NullValueHandling = NullValueHandling.Ignore)]
        public string ProfileName { get; set; }

        [JsonProperty("target", NullValueHandling = NullValueHandling.Ignore)]
        public string Target { get; set; }

        [JsonProperty("path", NullValueHandling = NullValueHandling.Ignore)]
        public string Path { get; set; }

        [JsonProperty("version", NullValueHandling = NullValueHandling.Ignore)]
        public string Version { get; set; }

        [JsonProperty("filePath", NullValueHandling = NullValueHandling.Ignore)]
        public string FilePath { get; set; }

        [JsonProperty("welcome", NullValueHandling = NullValueHandling.Ignore)]
        public string Welcome { get; set; }

        [JsonProperty("minecraft", NullValueHandling = NullValueHandling.Ignore)]
        public string Minecraft { get; set; }

        [JsonProperty("mirrorList", NullValueHandling = NullValueHandling.Ignore)]
        public Uri MirrorList { get; set; }

        [JsonProperty("logo", NullValueHandling = NullValueHandling.Ignore)]
        public string Logo { get; set; }

        [JsonProperty("modList", NullValueHandling = NullValueHandling.Ignore)]
        public string ModList { get; set; }
    }

    public sealed class VersionInfo {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("time", NullValueHandling = NullValueHandling.Ignore)]
        public string Time { get; set; }

        [JsonProperty("releaseTime", NullValueHandling = NullValueHandling.Ignore)]
        public string ReleaseTime { get; set; }

        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        [JsonProperty("minecraftArguments", NullValueHandling = NullValueHandling.Ignore)]
        public string MinecraftArguments { get; set; }

        [JsonProperty("mainClass", NullValueHandling = NullValueHandling.Ignore)]
        public string MainClass { get; set; }

        [JsonProperty("inheritsFrom", NullValueHandling = NullValueHandling.Ignore)]
        public string InheritsFrom { get; set; }

        [JsonProperty("jar", NullValueHandling = NullValueHandling.Ignore)]
        public string Jar { get; set; }

        [JsonProperty("logging", NullValueHandling = NullValueHandling.Ignore)]
        public Logging Logging { get; set; }

        [JsonProperty("libraries", NullValueHandling = NullValueHandling.Ignore)]
        public List<Library> Libraries { get; set; }
    }

    public sealed class Library {
        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("url", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Url { get; set; }

        [JsonProperty("serverreq", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Serverreq { get; set; }

        [JsonProperty("checksums", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> Checksums { get; set; }

        [JsonProperty("clientreq", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Clientreq { get; set; }
    }

    public sealed class Logging { }

    public partial class ForgeVersion {
        public static ForgeVersion FromJson(string json) {
            return JsonConvert.DeserializeObject<ForgeVersion>(json, Converter.Settings);
        }
    }

    public static class Serialize {
        public static string ToJson(this ForgeVersion self) {
            return JsonConvert.SerializeObject(self, Converter.Settings);
        }
    }

    internal static class Converter {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore, DateParseHandling = DateParseHandling.None,
            Converters = {new IsoDateTimeConverter {DateTimeStyles = DateTimeStyles.AssumeUniversal}}
        };
    }
}

namespace MinecraftContent {
    public class MinecraftVersionsIndex {
        [JsonProperty("latest", NullValueHandling = NullValueHandling.Ignore)]
        public Latest Latest { get; set; }

        [JsonProperty("versions", NullValueHandling = NullValueHandling.Ignore)]
        public List<Version> Versions { get; set; }

        public static MinecraftVersionsIndex FromJson(string json) {
            return JsonConvert.DeserializeObject<MinecraftVersionsIndex>(json);
        }
    }

    public class Latest {
        [JsonProperty("release", NullValueHandling = NullValueHandling.Ignore)]
        public string Release { get; set; }

        [JsonProperty("snapshot", NullValueHandling = NullValueHandling.Ignore)]
        public string Snapshot { get; set; }
    }

    public class Version {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        [JsonProperty("url", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Url { get; set; }

        [JsonProperty("time", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? Time { get; set; }

        [JsonProperty("releaseTime", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? ReleaseTime { get; set; }
    }
}

namespace SpecificMinecraftVersionManifest {
    public partial class SpecificMinecraftVersion {
        [JsonProperty("assetIndex", NullValueHandling = NullValueHandling.Ignore)]
        public AssetIndex AssetIndex { get; set; }

        [JsonProperty("assets", NullValueHandling = NullValueHandling.Ignore)]
        public string Assets { get; set; }

        [JsonProperty("downloads", NullValueHandling = NullValueHandling.Ignore)]
        public SpecificMinecraftVersionDownloads Downloads { get; set; }

        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("libraries", NullValueHandling = NullValueHandling.Ignore)]
        public List<Library> Libraries { get; set; }

        [JsonProperty("logging", NullValueHandling = NullValueHandling.Ignore)]
        public Logging Logging { get; set; }

        [JsonProperty("mainClass", NullValueHandling = NullValueHandling.Ignore)]
        public string MainClass { get; set; }

        [JsonProperty("minecraftArguments", NullValueHandling = NullValueHandling.Ignore)]
        public string MinecraftArguments { get; set; }

        [JsonProperty("minimumLauncherVersion", NullValueHandling = NullValueHandling.Ignore)]
        public long? MinimumLauncherVersion { get; set; }

        [JsonProperty("releaseTime", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? ReleaseTime { get; set; }

        [JsonProperty("time", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? Time { get; set; }

        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }
    }

    public sealed class AssetIndex {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("sha1", NullValueHandling = NullValueHandling.Ignore)]
        public string Sha1 { get; set; }

        [JsonProperty("size", NullValueHandling = NullValueHandling.Ignore)]
        public long? Size { get; set; }

        [JsonProperty("totalSize", NullValueHandling = NullValueHandling.Ignore)]
        public long? TotalSize { get; set; }

        [JsonProperty("url", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Url { get; set; }
    }

    public sealed class SpecificMinecraftVersionDownloads {
        [JsonProperty("client", NullValueHandling = NullValueHandling.Ignore)]
        public ServerClass Client { get; set; }

        [JsonProperty("server", NullValueHandling = NullValueHandling.Ignore)]
        public ServerClass Server { get; set; }

        [JsonProperty("windows_server", NullValueHandling = NullValueHandling.Ignore)]
        public ServerClass WindowsServer { get; set; }
    }

    public sealed class ServerClass {
        [JsonProperty("sha1", NullValueHandling = NullValueHandling.Ignore)]
        public string Sha1 { get; set; }

        [JsonProperty("size", NullValueHandling = NullValueHandling.Ignore)]
        public long? Size { get; set; }

        [JsonProperty("url", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Url { get; set; }

        [JsonProperty("path", NullValueHandling = NullValueHandling.Ignore)]
        public string Path { get; set; }
    }

    public sealed class Library {
        [JsonProperty("downloads", NullValueHandling = NullValueHandling.Ignore)]
        public LibraryDownloads Downloads { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("extract", NullValueHandling = NullValueHandling.Ignore)]
        public Extract Extract { get; set; }

        [JsonProperty("natives", NullValueHandling = NullValueHandling.Ignore)]
        public Natives Natives { get; set; }

        [JsonProperty("rules", NullValueHandling = NullValueHandling.Ignore)]
        public List<Rule> Rules { get; set; }
    }

    public sealed class LibraryDownloads {
        [JsonProperty("artifact", NullValueHandling = NullValueHandling.Ignore)]
        public ServerClass Artifact { get; set; }

        [JsonProperty("classifiers", NullValueHandling = NullValueHandling.Ignore)]
        public Classifiers Classifiers { get; set; }
    }

    public sealed class Classifiers {
        [JsonProperty("natives-linux", NullValueHandling = NullValueHandling.Ignore)]
        public ServerClass NativesLinux { get; set; }

        [JsonProperty("natives-osx", NullValueHandling = NullValueHandling.Ignore)]
        public ServerClass NativesOsx { get; set; }

        [JsonProperty("natives-windows", NullValueHandling = NullValueHandling.Ignore)]
        public ServerClass NativesWindows { get; set; }

        [JsonProperty("natives-windows-32", NullValueHandling = NullValueHandling.Ignore)]
        public ServerClass NativesWindows32 { get; set; }

        [JsonProperty("natives-windows-64", NullValueHandling = NullValueHandling.Ignore)]
        public ServerClass NativesWindows64 { get; set; }
    }

    public sealed class Extract {
        [JsonProperty("exclude", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> Exclude { get; set; }
    }

    public sealed class Natives {
        [JsonProperty("linux", NullValueHandling = NullValueHandling.Ignore)]
        public string Linux { get; set; }

        [JsonProperty("osx", NullValueHandling = NullValueHandling.Ignore)]
        public string Osx { get; set; }

        [JsonProperty("windows", NullValueHandling = NullValueHandling.Ignore)]
        public string Windows { get; set; }
    }

    public sealed class Rule {
        [JsonProperty("action", NullValueHandling = NullValueHandling.Ignore)]
        public string Action { get; set; }

        [JsonProperty("os", NullValueHandling = NullValueHandling.Ignore)]
        public Os Os { get; set; }
    }

    public sealed class Os {
        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }
    }

    public sealed class Logging {
        [JsonProperty("client", NullValueHandling = NullValueHandling.Ignore)]
        public LoggingClient Client { get; set; }
    }

    public sealed class LoggingClient {
        [JsonProperty("argument", NullValueHandling = NullValueHandling.Ignore)]
        public string Argument { get; set; }

        [JsonProperty("file", NullValueHandling = NullValueHandling.Ignore)]
        public AssetIndex File { get; set; }

        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }
    }

    public partial class SpecificMinecraftVersion {
        public static SpecificMinecraftVersion FromJson(string json) {
            return JsonConvert.DeserializeObject<SpecificMinecraftVersion>(json, Converter.Settings);
        }
    }

    public static class Serialize {
        public static string ToJson(this SpecificMinecraftVersion self) {
            return JsonConvert.SerializeObject(self, Converter.Settings);
        }
    }

    internal static class Converter {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore, DateParseHandling = DateParseHandling.None,
            Converters = {new IsoDateTimeConverter {DateTimeStyles = DateTimeStyles.AssumeUniversal}}
        };
    }
}

namespace MinecraftAssetsManifest {
    public class AssetsManifest {
        [JsonProperty("map_to_resources", NullValueHandling = NullValueHandling.Ignore)]
        public bool? MapToResources { get; set; }

        [JsonProperty("objects", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, Object> Objects { get; set; }

        public static AssetsManifest FromJson(string json) {
            return JsonConvert.DeserializeObject<AssetsManifest>(json, Converter.Settings);
        }
    }

    public sealed class Object {
        [JsonProperty("hash", NullValueHandling = NullValueHandling.Ignore)]
        public string Hash { get; set; }

        [JsonProperty("size", NullValueHandling = NullValueHandling.Ignore)]
        public long? Size { get; set; }
    }

    internal static class Converter {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore, DateParseHandling = DateParseHandling.None,
            Converters = {new IsoDateTimeConverter {DateTimeStyles = DateTimeStyles.AssumeUniversal}}
        };
    }
}