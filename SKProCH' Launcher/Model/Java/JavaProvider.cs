using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Microsoft.Win32;
using Shared.Model;

namespace Launcher.Model.Java {
    public static class JavaProvider {
        public static IEnumerable<JavaInfo> GetFromUninstall() {
            var toReturn = new List<JavaInfo>();
            toReturn.AddRange(ProcessUninstallReg(RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                                                             .OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall")));
            toReturn.AddRange(ProcessUninstallReg(RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32)
                                                             .OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall")));
            return toReturn;
        }

        public static IEnumerable<JavaInfo> GetFromPath() {
            var toReturn = new List<JavaInfo>();
            var defaultJava = Shared.Model.Utilities.FindOnPath(@"javaw.exe");

            if (defaultJava != null && File.Exists(defaultJava) && TryGetJavaFromFile(defaultJava, out var jclass1))
                toReturn.Add(jclass1);

            return toReturn;
        }

        public static IEnumerable<JavaInfo> GetFromReg() {
            var toReturn = new List<JavaInfo>();
            toReturn.AddRange(ProcessReg(RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                                                    .OpenSubKey("SOFTWARE")
                                                   ?.OpenSubKey("JavaSoft")
                                                   ?.OpenSubKey("Java Runtime Environment")));
            toReturn.AddRange(ProcessReg(RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32)
                                                    .OpenSubKey("SOFTWARE")
                                                   ?.OpenSubKey("JavaSoft")
                                                   ?.OpenSubKey("Java Runtime Environment")));
            return toReturn;
        }


        private static IEnumerable<JavaInfo> ProcessUninstallReg(RegistryKey key) {
            var toReturn = new List<JavaInfo>();

            if (key == null) return toReturn;
            toReturn.AddRange(from subKeyName in key.GetSubKeyNames()
                              select key.OpenSubKey(subKeyName)
                              into entry
                              let sub = entry.GetValue("DisplayName")
                              where sub != null && sub.ToString().StartsWith(@"Java")
                              let path = Path.Combine((string) entry.GetValue("InstallLocation"), @"bin", @"java.exe")
                              where File.Exists(path)
                              select new JavaInfo {
                                  JavaVersion = sub.ToString(), JavaDescription = Shared.Model.Utilities.GetDirectory(path),
                                  Arch = Shared.Model.Utilities.GetMachineType(path) == Shared.Model.Utilities.MachineType.X64 ? @"64" : @"32"
                              });

            return toReturn;
        }

        private static IEnumerable<JavaInfo> ProcessReg(RegistryKey jreg) {
            var toReturn = new List<JavaInfo>();
            if (jreg == null) return toReturn;
            foreach (var subKeyName in jreg.GetSubKeyNames())
                try {
                    var path = Path.Combine((string) jreg.OpenSubKey(subKeyName).GetValue("JavaHome"), @"bin", @"java.exe");

                    if (File.Exists(path))
                        toReturn.Add(new JavaInfo {
                            JavaVersion = subKeyName, JavaDescription = Shared.Model.Utilities.GetDirectory(path),
                            Arch = Shared.Model.Utilities.GetMachineType(path) == Shared.Model.Utilities.MachineType.X64 ? @"64" : @"32"
                        });
                }
                catch (Exception) {
                    // ignored
                }

            return toReturn;
        }

        public static bool TryGetJavaFromFile(string filename, out JavaInfo jclass) {
            jclass = null;

            try {
                if (!File.Exists(filename)) return false;
                var process = new Process();

                var startInfo = new ProcessStartInfo {
                    WindowStyle = ProcessWindowStyle.Hidden, CreateNoWindow = true,
                    FileName = filename,
                    Arguments = "-version",
                    RedirectStandardOutput = true, RedirectStandardError = true, UseShellExecute = false
                };

                process.StartInfo = startInfo;
                process.Start();
                var text = process.StandardError.ReadToEnd();
                process.WaitForExit();

                jclass = new JavaInfo {
                    JavaVersion = text.SplitByLines()[0].Split('\"')[1], JavaDescription = Shared.Model.Utilities.GetDirectory(filename),
                    Arch = Shared.Model.Utilities.GetMachineType(filename) == Shared.Model.Utilities.MachineType.X64 ? @"64" : @"32"
                };
                return true;
            }
            catch (Exception) {
                return false;
            }
        }
    }
}