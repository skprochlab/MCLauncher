using Shared;

namespace Launcher.Model.Java {
    [Magic]
    public sealed class JavaInfo {
        public string JavaVersion { get; set; }
        public string JavaDescription { get; set; }
        public string Arch { get; set; }
    }
}