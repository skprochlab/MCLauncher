using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Ionic.Zip;
using Rasyidf.Localization;
using Shared.Model;

namespace Launcher.Model.Java {
    public static class JavaDownload {
        private static readonly SemaphoreSlim _semaphoreSlim = new SemaphoreSlim(1, 1);

        private static int progress = -100;
        private static readonly object lockObject = new object();

        public static async Task<string> ResolveJavaPath(Modpack modpack) {
            modpack.ThisModpackTabLog.WriteToLog(LocalizationService.GetString(@"Java", @"ResolvingDownloadJava", @"Resolving java..."));
            await _semaphoreSlim.WaitAsync();
            try {
                if (File.Exists(Path.Combine(@"java", @"bin", @"javaw.exe")))
                    return Path.GetFullPath(Path.Combine(@"java", @"bin", @"javaw.exe"));

                void OnProgressChanged(object sender, double d) {
                    modpack.ThisModpackTabLog.WriteToLog(string.Format(LocalizationService.GetString(@"Java", @"DownloadProgress", @"Java deployment - {0}%"),
                        d.ToString().SafeSubstring(0, 4)));
                }

                ProgressChanged += OnProgressChanged;

                await Start();

                ProgressChanged -= OnProgressChanged;

                return Path.GetFullPath(Path.Combine(@"java", @"bin", @"javaw.exe"));
            }
            finally {
                _semaphoreSlim.Release();
            }
        }

        public static event EventHandler<double> ProgressChanged;

        public static async Task Start() {
            await DownloadJava();
            ExtractJava();
            MoveTempFiles();
            ProgressChanged?.Invoke(null, 100);
        }

        private static async Task DownloadJava() {
            using (var wc = new WebClient()) {
                wc.DownloadProgressChanged += (sender, e) => OnProgressChanged(e.ProgressPercentage * 0.5);
                try {
                    await
                        wc.DownloadFileTaskAsync($@"https://storage.illumined.ru/java/jre-x{(Environment.Is64BitOperatingSystem ? @"64" : @"32")}.zip",
                            @"tempjava.zip");
                }
                catch (Exception) {
                    await
                        wc.DownloadFileTaskAsync(
                            $@"https://api.adoptopenjdk.net/v2/binary/nightly/openjdk8?openjdk_impl=hotspot&os=windows&arch=x{(Environment.Is64BitOperatingSystem ? @"64" : @"32")}&release=latest&type=jre",
                            @"tempjava.zip");
                }
            }
        }

        private static void ExtractJava() {
            using (var zf = ZipFile.Read(@"tempjava.zip")) {
                zf.ExtractProgress += (sender, e) => {
                    if (e.EventType != ZipProgressEventType.Extracting_AfterExtractEntry) return;
                    OnProgressChanged(50 + e.EntriesExtracted / (double) e.EntriesTotal * 100 * 0.5);
                };
                zf.ExtractAll(@"tempjava", ExtractExistingFileAction.OverwriteSilently);
            }
        }

        private static void MoveTempFiles() {
            Shared.Model.Utilities.MoveDirectory(Directory.GetDirectories("tempjava").First(), @"java");
            Directory.Delete("tempjava");
            File.Delete(@"tempjava.zip");
        }

        private static void OnProgressChanged(double e) {
            lock (lockObject) {
                if (!(Math.Abs(progress - e) > 2)) return;
                ProgressChanged?.Invoke(null, e);
                progress = (int) e;
            }
        }
    }
}