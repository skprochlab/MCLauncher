﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Launcher.Model.Config;
using LiteDB;
using Rasyidf.Localization;

namespace Launcher.Model.Java {
    internal static class JavaUtils {
        private static readonly LiteCollection<Entity> CachedJavas = GlobalDB.Database.GetCollection<Entity>(@"CachedJavas");
        public static event EventHandler<IEnumerable<JavaInfo>> JavasChanged;
        public static event EventHandler MemChanged;

        public static IEnumerable<JavaInfo> GetJavas() {
            var toReturn = new List<JavaInfo> {
                new JavaInfo {
                    JavaVersion = LocalizationService.GetString(@"Java", @"Download",
                        @"Download the latest Java runtime"),
                    JavaDescription = @"DownloadedJava",
                    Arch = Environment.Is64BitOperatingSystem ? @"64" : @"32"
                },
                new JavaInfo {JavaDescription = @"javaw", JavaVersion = LocalizationService.GetString(@"Java", @"Default", @"Use default"), Arch = @"??"}
            };
            toReturn.AddRange(JavaProvider.GetFromUninstall());
            toReturn.AddRange(JavaProvider.GetFromPath());
            toReturn.AddRange(JavaProvider.GetFromReg());
            OperateWithCache(ref toReturn);

            toReturn = toReturn.GroupBy(info => info.JavaDescription).Select(infos => infos.First()).ToList();
            return toReturn;
        }

        private static void OperateWithCache(ref List<JavaInfo> entries) {
            //Make a copy to async add to cache after all
            var entriesCopy = entries.Skip(2).ToList();

            foreach (var entity in CachedJavas.FindAll()) {
                var javaPath = File.GetAttributes(entity).HasFlag(FileAttributes.Directory) ? Path.Combine(entity, "javaw.exe") : entity;
                if (File.Exists(javaPath)) {
                    if (entries.Any(jclass => jclass.JavaDescription == javaPath)) continue;
                    if (JavaProvider.TryGetJavaFromFile(javaPath, out var loadedclass))
                        entries.Add(loadedclass);
                    else
                        CachedJavas.Delete(entity.Id.ToString());
                }
                else {
                    CachedJavas.Delete(entity.Id.ToString());
                }
            }

            Task.Run(() => {
                foreach (var javaClass in entriesCopy) AddCachedEntry(javaClass.JavaDescription);
            });
        }

        public static JavaControl GetGlobalJavaSetting() {
            return GetSpecifiedJavaSettings(-1);
        }

        public static JavaControl GetSpecifiedJavaSettings(int uid) {
            if (uid == -1)
                return new JavaControl {
                    JavaPath = GlobalDB.GlobalSettings.FindById(@"GlobalJavaPath") ??
                               (Shared.Model.Utilities.FindOnPath(@"javaw.exe") != null ? @"javaw" : @"DownloadedJava"),
                    MaxMem = int.TryParse(GlobalDB.GlobalSettings.FindById(@"GlobalMaxMem")?.Value, out var max) ? max : Utilities.TotalRam.Value / 2,
                    MinMem = int.TryParse(GlobalDB.GlobalSettings.FindById(@"GlobalMinMem")?.Value, out var min) ? min : Utilities.TotalRam.Value / 2
                };

            return Config.Config.GetModpackByUid(uid).JavaSettings ?? new JavaControl {JavaPath = null, MaxMem = null, MinMem = null};
        }

        public static void SaveJavaSettings(int uid, JavaControl java) {
            if (uid == -1) {
                GlobalDB.GlobalSettings.Upsert(@"GlobalJavaPath", new Entity(java.JavaPath));
                GlobalDB.GlobalSettings.Upsert(@"GlobalMaxMem", new Entity(java.MaxMem));
                GlobalDB.GlobalSettings.Upsert(@"GlobalMinMem", new Entity(java.MinMem));
                OnJavasChanged();
            }
            else {
                Config.Config.GetModpackByUid(uid).JavaSettings = java;
                Config.Config.GetModpackByUid(uid).Save();
            }
        }

        public static void AddCachedEntry(string path) {
            var fullPath = Path.GetFullPath(path);
            var temp = CachedJavas.FindOne(entity => entity.Value == fullPath);
            if (CachedJavas.FindOne(entity => entity.Value == fullPath) == null) CachedJavas.Insert(new Entity(fullPath));
        }

        public static void OnJavasChanged() {
            JavasChanged?.Invoke(null, GetJavas());
        }

        public static void OnMemChanged() {
            MemChanged?.Invoke(null, EventArgs.Empty);
        }
    }

    public sealed class JavaControl {
        public string JavaPath { get; set; }
        public int? MaxMem { get; set; }
        public int? MinMem { get; set; }
    }
}