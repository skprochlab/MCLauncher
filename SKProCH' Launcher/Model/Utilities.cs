﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Win32;
using Newtonsoft.Json;
using Shared;
using Shared.Model;

namespace Launcher.Model {
    internal static class Utilities {
        private static string _filter;

        public static Lazy<int> TotalRam = new Lazy<int>(() => {
            var wql = new ObjectQuery(@"SELECT * FROM Win32_OperatingSystem");
            var searcher = new ManagementObjectSearcher(wql);
            var results = searcher.Get();

            foreach (ManagementObject result in results)
                return (int) Math.Round(Convert.ToDouble(result[@"TotalVisibleMemorySize"]) / (1024 * 128)) * 128;

            return 0;
        }, LazyThreadSafetyMode.ExecutionAndPublication);

        public static void DownloadFile(Uri url, string path) {
            DownloadFile(Convert.ToString(url), path);
        }

        public static void DownloadFile([Localizable(false)] string url, string path) {
            Exception e = null;
            Directory.CreateDirectory(Path.GetDirectoryName(path));
            var wc = new WebClient();
            try {
                wc.DownloadFile(url, path);
            }
            catch (Exception exception) {
                e = exception;
            }
            finally {
                wc.Dispose();
            }

            if (e != null) throw e;
        }

        public static List<string> RecursiveFilesScan(string dir) {
            Directory.CreateDirectory(dir);
            var dirs = new List<string>();
            dirs.AddRange(Directory.GetDirectories(dir));
            var result = new List<string>();
            result.AddRange(Directory.GetFiles(dir));

            foreach (var variable in dirs)
                result.AddRange(RecursiveFilesScan(variable));

            return result;
        }

        public static void RecursiveFoldersDelete([Localizable(false)] string dir) {
            foreach (var variable in RecursiveFilesScan(dir)) File.Delete(variable);
            RecursiveFoldersDeleteMethod(dir);
        }

        private static void RecursiveFoldersDeleteMethod(string dir) {
            foreach (var directory in Directory.GetDirectories(dir)) RecursiveFoldersDeleteMethod(directory);

            Directory.Delete(dir, true);
        }

        [Localizable(false)]
        public static string GetFilters() {
            if (_filter != null) return _filter;
            _filter = @"Image Files|";
            foreach (var supportedExtension in new BitmapImageCheck().AllSupportedExtensions) _filter += "*" + supportedExtension + ";";

            return _filter;
        }

        [Localizable(false)]
        public static string FindImagesFile() {
            var dlg = new OpenFileDialog {DefaultExt = ".png", Filter = GetFilters()};
            var result = dlg.ShowDialog();
            return result == true ? dlg.FileName : "";
        }

        public static bool CompareSha1(string path, string sha1) {
            return !string.Equals(GetSha1(path), sha1.ToLower(), StringComparison.CurrentCultureIgnoreCase);
        }

        [Localizable(false)]
        public static string GetSha1(string path) {
            try {
                using var fs = new FileStream(path, FileMode.Open);
                using var bs = new BufferedStream(fs);
                using var sha1 = new SHA1Managed();
                var fileHash = sha1.ComputeHash(bs);
                var formatted = new StringBuilder(2 * fileHash.Length);
                foreach (var b in fileHash) formatted.AppendFormat("{0:X2}", b);
                return formatted.ToString().ToLower();
            }
            catch (Exception) {
                return "";
            }
        }


        public static string SafeRead(string file) {
            try {
                return File.ReadAllText(file);
            }
            catch (Exception) {
                return "";
            }
        }

        [Localizable(false)]
        public static async Task<PasteeeResponse> UploadToPasteee(string message) {
            //message = message.Replace(System.Environment.NewLine, @"\n").Replace("\"", "\\\"").Replace("\r\n", "\n");
            using var httpClient = new HttpClient();
            using var request = new HttpRequestMessage(new HttpMethod(@"POST"), "https://api.paste.ee/v1/pastes");
            request.Headers.TryAddWithoutValidation(@"X-Auth-Token", @"uMXjxv5ElfUGmdp71VqC6v10Rbqo1d6ZLG6NbBq62");

            request.Content =
                new StringContent(
                    "{\"description\":\"SKProCHs Launcher Log Upload\",\"sections\":[{\"name\":\"Current log\",\"syntax\":\"autodetect\",\"contents\":\"" +
                    message.Escape() + "\"}]}",
                    Encoding.UTF8,
                    "application/json");

            var response = await httpClient.SendAsync(request);
            return JsonConvert.DeserializeObject<PasteeeResponse>(await response.Content.ReadAsStringAsync());
        }

        [Localizable(false)]
        public sealed class PasteeeResponse {
            public string Id { get; set; }
            public Uri Link { get; set; }
            public bool? Success { get; set; }
            public List<PasteeeError> Errors { get; set; }

            public sealed class PasteeeError {
                public string Field { get; set; }
                public string Message { get; set; }
                public long? Code { get; set; }

                public override string ToString() {
                    return $"Error code: {Code}, Field: {Field}, Message : {Message}";
                }
            }
        }


        /// <summary>
        ///     Provides methods for checking whether a file can likely be opened as a BitmapImage, based upon its file extension
        /// </summary>
        [Localizable(false)]
        public sealed class BitmapImageCheck : IDisposable {
            #region constructors

            public BitmapImageCheck() {
                string baseKeyPath;
                if (Environment.Is64BitOperatingSystem && !Environment.Is64BitProcess)
                    baseKeyPath = "Wow6432Node\\CLSID";
                else
                    baseKeyPath = "CLSID";
                _baseKey = Registry.ClassesRoot.OpenSubKey(baseKeyPath, false);
                RecalculateExtensions();
            }

            #endregion

            #region public methods

            /// <summary>
            ///     Check whether a file is likely to be supported by BitmapImage based upon its extension
            /// </summary>
            /// <param name="extension">File extension (with or without leading full stop), file name or file path</param>
            /// <returns>True if extension appears to contain a supported file extension, false if no suitable extension was found</returns>
            public bool IsExtensionSupported(string extension) {
                //prepare extension, should a full path be given
                if (extension.Contains(".")) extension = extension.Substring(extension.LastIndexOf('.') + 1);
                extension = extension.ToUpper();
                extension = extension.Insert(0, ".");
                return AllSupportedExtensions.Contains(extension);
            }

            #endregion

            #region class variables

            private readonly RegistryKey _baseKey;
            private const string WicDecoderCategory = "{7ED96837-96F0-4812-B211-F13C24117ED3}";

            #endregion

            #region properties

            /// <summary>
            ///     File extensions that are supported by decoders found elsewhere on the system
            /// </summary>
            public string[] CustomSupportedExtensions { get; private set; }

            /// <summary>
            ///     File extensions that are supported natively by .NET
            /// </summary>
            public string[] NativeSupportedExtensions { get; private set; }

            /// <summary>
            ///     File extensions that are supported both natively by NET, and by decoders found elsewhere on the system
            /// </summary>
            public string[] AllSupportedExtensions { get; private set; }

            #endregion

            #region private methods

            /// <summary>
            ///     Re-calculate which extensions are available on this system. It's unlikely this ever needs to be called outside of
            ///     the
            ///     constructor.
            /// </summary>
            private void RecalculateExtensions() {
                CustomSupportedExtensions = GetSupportedExtensions().ToArray();
                NativeSupportedExtensions = new[] {".BMP", ".GIF", ".ICO", ".JPEG", ".PNG", ".TIFF", ".DDS", ".JPG", ".JXR", ".HDP", ".WDP"};

                var cse = CustomSupportedExtensions;
                var nse = NativeSupportedExtensions;
                var ase = new string[cse.Length + nse.Length];
                Array.Copy(nse, ase, nse.Length);
                Array.Copy(cse, 0, ase, nse.Length, cse.Length);
                AllSupportedExtensions = ase;
            }

            /// <summary>
            ///     Represents information about a WIC decoder
            /// </summary>
            private struct DecoderInfo {
                public string FriendlyName;
                public string FileExtensions;
            }

            /// <summary>
            ///     Gets a list of additionally registered WIC decoders
            /// </summary>
            /// <returns></returns>
            private IEnumerable<DecoderInfo> GetAdditionalDecoders() {
                var result = new List<DecoderInfo>();

                foreach (var codecKey in GetCodecKeys()) {
                    var decoderInfo = new DecoderInfo();
                    decoderInfo.FriendlyName = Convert.ToString(codecKey.GetValue("FriendlyName", ""));
                    decoderInfo.FileExtensions = Convert.ToString(codecKey.GetValue("FileExtensions", ""));
                    result.Add(decoderInfo);
                }

                return result;
            }

            private List<string> GetSupportedExtensions() {
                var decoders = GetAdditionalDecoders();
                return decoders.SelectMany(decoder => decoder.FileExtensions.Split(',')).ToList();
            }

            private IEnumerable<RegistryKey> GetCodecKeys() {
                var result = new List<RegistryKey>();

                if (_baseKey == null) return result;
                var categoryKey = _baseKey.OpenSubKey(WicDecoderCategory + "\\instance", false);
                if (categoryKey == null) return result;
                // Read the guids of the registered decoders
                var codecGuids = categoryKey.GetSubKeyNames();
                result.AddRange(GetCodecGuids().Select(codecGuid => _baseKey.OpenSubKey(codecGuid)).Where(codecKey => codecKey != null));
                return result;
            }

            private IEnumerable<string> GetCodecGuids() {
                var categoryKey = _baseKey?.OpenSubKey(WicDecoderCategory + "\\instance", false);
                return categoryKey?.GetSubKeyNames();
            }

            #endregion

            #region overrides and whatnot

            public override string ToString() {
                var result = "";

                result += "\nNative support for the following extensions is available: ";
                result = NativeSupportedExtensions.Aggregate(result, (current, item) => current + item + ",");

                if (NativeSupportedExtensions.Any())
                    result = result.Remove(result.Length - 1);

                var decoders = GetAdditionalDecoders();
                var decoderInfos = decoders.ToList();

                if (!decoderInfos.Any()) {
                    result += "\n\nNo custom decoders found.";
                }
                else {
                    result += "\n\nThese custom decoders were also found:";
                    result = decoderInfos.Aggregate(result,
                        (current, decoder) => current + "\n" + decoder.FriendlyName + ", supporting extensions " + decoder.FileExtensions);
                }

                return result;
            }

            public void Dispose() {
                _baseKey.Dispose();
            }

            #endregion
        }
    }

    public static class PathUtils {
        /// <summary>
        ///     Creates a relative path from one file or folder to another.
        /// </summary>
        /// <param name="fromPath">Contains the directory that defines the start of the relative path.</param>
        /// <param name="toPath">Contains the path that defines the endpoint of the relative path.</param>
        /// <returns>The relative path from the start directory to the end path.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="fromPath" /> or <paramref name="toPath" /> is <c>null</c>.</exception>
        /// <exception cref="UriFormatException"></exception>
        /// <exception cref="InvalidOperationException"></exception>
        public static string GetRelativePath(string fromPath, string toPath) {
            if (string.IsNullOrEmpty(fromPath))
                throw new ArgumentNullException(nameof(fromPath));

            if (string.IsNullOrEmpty(toPath))
                throw new ArgumentNullException(nameof(toPath));

            var fromUri = new Uri(AppendDirectorySeparatorChar(fromPath));
            var toUri = new Uri(AppendDirectorySeparatorChar(toPath));

            if (fromUri.Scheme != toUri.Scheme)
                return toPath;

            var relativeUri = fromUri.MakeRelativeUri(toUri);
            var relativePath = Uri.UnescapeDataString(relativeUri.ToString());

            if (string.Equals(toUri.Scheme, Uri.UriSchemeFile, StringComparison.OrdinalIgnoreCase))
                relativePath = relativePath.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);

            return relativePath;
        }

        private static string AppendDirectorySeparatorChar(string path) {
            // Append a slash only if the path is a directory and does not have a slash.
            if (!Path.HasExtension(path) &&
                !path.EndsWith(Path.DirectorySeparatorChar.ToString()))
                return path + Path.DirectorySeparatorChar;

            return path;
        }
    }

    public static class UidProvider {
        public static int IdCount;

        public static int GetUid() {
            return IdCount++;
        }
    }

    public interface IResetable {
        AnotherCommandImplementation ResetCommand { get; }
    }

    public interface IHavePassword {
        string GetPassword();
        void ResetPassword();
    }

    public sealed class AccountException : Exception {
        public AccountException() { }
        public AccountException(string message) : base(message) { }
    }
}