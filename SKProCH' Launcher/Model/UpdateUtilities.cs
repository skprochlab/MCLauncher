﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Shared;

namespace Launcher.Model {
    internal static class UpdateUtilities {
        public static async Task<bool> IsLauncherUpdateAvailable() {
            return await IsLauncherUpdateAvailable(Assembly.GetExecutingAssembly().GetName().Version);
        }

        public static async Task<bool> IsLauncherUpdateAvailable(Version currentVersion) {
            try {
                var latestRelease = (await GetManifest())[0];
                var latestVersion = Version.Parse(Regex.IsMatch(latestRelease.TagName, @"\d+.\d+.\d+")
                    ? Regex.Match(latestRelease.TagName, @"\d+.\d+.\d+").Value
                    : Regex.Match(latestRelease.TagName, @"\d+.\d+").Value);
                return latestVersion.CompareTo(currentVersion) > 0;
            }
            catch (Exception) {
                return false;
            }
        }

        public static async Task<List<Release>> GetManifest() {
            var temp = await
                WebUtilities.GetAsync("https://gitlab.com/api/v4/projects/10334920/releases/");
            var releases = JsonConvert.DeserializeObject<List<Release>>(temp);
            return releases;
        }

        public static void ClearOldFiles() {
            try {
                if (!Directory.Exists("old")) return;
                var options =
                    Parallel.ForEach(Directory.GetFiles("old"), new ParallelOptions {
                        MaxDegreeOfParallelism = 666
                    }, s => {
                        try {
                            File.Delete(s);
                        }
                        catch (Exception) {
                            // ignored
                        }
                    });
                Directory.Delete("old");
            }
            catch (Exception) {
                // ignored
            }
        }
    }
}

namespace Launcher.Model {
    public class Release {
        [JsonProperty("tag_name", NullValueHandling = NullValueHandling.Ignore)]
        public string TagName { get; set; }

        [JsonProperty("description", NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("description_html", NullValueHandling = NullValueHandling.Ignore)]
        public string DescriptionHtml { get; set; }

        [JsonProperty("created_at", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? CreatedAt { get; set; }

        [JsonProperty("released_at", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? ReleasedAt { get; set; }

        [JsonProperty("author", NullValueHandling = NullValueHandling.Ignore)]
        public Author Author { get; set; }

        [JsonProperty("commit", NullValueHandling = NullValueHandling.Ignore)]
        public Commit Commit { get; set; }

        [JsonProperty("assets", NullValueHandling = NullValueHandling.Ignore)]
        public Assets Assets { get; set; }
    }

    public class Assets {
        [JsonProperty("count", NullValueHandling = NullValueHandling.Ignore)]
        public long? Count { get; set; }

        [JsonProperty("sources", NullValueHandling = NullValueHandling.Ignore)]
        public List<Source> Sources { get; set; }

        [JsonProperty("links", NullValueHandling = NullValueHandling.Ignore)]
        public List<Link> Links { get; set; }
    }

    public class Link {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("url", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Url { get; set; }

        [JsonProperty("external", NullValueHandling = NullValueHandling.Ignore)]
        public bool? External { get; set; }
    }

    public class Source {
        [JsonProperty("format", NullValueHandling = NullValueHandling.Ignore)]
        public string Format { get; set; }

        [JsonProperty("url", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Url { get; set; }
    }

    public class Author {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("username", NullValueHandling = NullValueHandling.Ignore)]
        public string Username { get; set; }

        [JsonProperty("state", NullValueHandling = NullValueHandling.Ignore)]
        public string State { get; set; }

        [JsonProperty("avatar_url", NullValueHandling = NullValueHandling.Ignore)]
        public Uri AvatarUrl { get; set; }

        [JsonProperty("web_url", NullValueHandling = NullValueHandling.Ignore)]
        public Uri WebUrl { get; set; }
    }

    public class Commit {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("short_id", NullValueHandling = NullValueHandling.Ignore)]
        public string ShortId { get; set; }

        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        [JsonProperty("created_at", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? CreatedAt { get; set; }

        [JsonProperty("parent_ids", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> ParentIds { get; set; }

        [JsonProperty("message", NullValueHandling = NullValueHandling.Ignore)]
        public string Message { get; set; }

        [JsonProperty("author_name", NullValueHandling = NullValueHandling.Ignore)]
        public string AuthorName { get; set; }

        [JsonProperty("author_email", NullValueHandling = NullValueHandling.Ignore)]
        public string AuthorEmail { get; set; }

        [JsonProperty("authored_date", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? AuthoredDate { get; set; }

        [JsonProperty("committer_name", NullValueHandling = NullValueHandling.Ignore)]
        public string CommitterName { get; set; }

        [JsonProperty("committer_email", NullValueHandling = NullValueHandling.Ignore)]
        public string CommitterEmail { get; set; }

        [JsonProperty("committed_date", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? CommittedDate { get; set; }
    }
}