﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Launcher.ViewModel;
using MaterialDesignThemes.Wpf;
using Shared;

namespace Launcher.Model {
    public static class WindowManager {
        public static void CloseWindow(Guid id) {
            GetWindow(id)?.Close();
        }

        public static Window GetWindow(Guid id) {
            return Application.Current.Windows.Cast<Window>()
                              .Select(window => new {window, w_id = window.DataContext as IRequireViewIdentification})
                              .Where(t => t.w_id != null && t.w_id.ViewID.Equals(id))
                              .Select(t => t.window)
                              .FirstOrDefault();
        }

        public static class PropertyTabsContainer {
            private static readonly ObservableConcurrentDictionary<int, View.PropertyTabsContainer> SettingsWindowRegistry =
                new ObservableConcurrentDictionary<int, View.PropertyTabsContainer>();

            public static View.PropertyTabsContainer GetById(int uid) {
                return SettingsWindowRegistry.TryGetValue(uid, out var tabsContainer) ? tabsContainer : null;
            }

            public static View.PropertyTabsContainer TryOpen(int uid = -1) {
                if (SettingsWindowRegistry.ContainsKey(uid)) {
                    Application.Current.Dispatcher.Invoke(() => { SettingsWindowRegistry[uid].Activate(); });
                    return SettingsWindowRegistry[uid];
                }

                View.PropertyTabsContainer window = null;
                Application.Current.Dispatcher.Invoke(() => {
                    window = new View.PropertyTabsContainer {DataContext = new PropertyTabsContainerVM(uid)};
                    window.Closed += (sender, args) => {
                        SettingsWindowRegistry.Remove(((PropertyTabsContainerVM) ((View.PropertyTabsContainer) sender).DataContext).Id);
                    };
                    window.Show();
                });
                SettingsWindowRegistry.Add(uid, window);
                return window;
            }

            public static void TravelTo(int uid, [Localizable(false)] string Default) {
                Application.Current.Dispatcher.BeginInvoke(new Action(() => ((PropertyTabsContainerVM) TryOpen(uid).DataContext).TravelTo(Default)));
            }

            [Localizable(false)]
            public static void TravelTo(int uid, [Localizable(false)] string @default, string message) {
                Application.Current.Dispatcher.BeginInvoke(new Action(() => ((PropertyTabsContainerVM) TryOpen(uid).DataContext).TravelTo(@default, message)));
            }

            public static async Task<bool?> ShowModalDialog(int uid, object content) {
                var window = TryOpen(uid);

                var dialogResult = await DialogHost.Show(content, @"SettingsDialogHost" + (uid == -1 ? "" : uid.ToString()));
                return (bool?) dialogResult;
            }
        }
    }

    public interface IRequireViewIdentification {
        // ReSharper disable once InconsistentNaming
        Guid ViewID { get; }
    }
}