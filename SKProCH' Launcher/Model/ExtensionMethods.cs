using System;
using System.Collections.Generic;
using System.Reactive.Linq;

namespace Launcher.Model {
    public static class ExtensionMethods {
        public static IObservable<IList<TSource>> BufferWhenAvailable<TSource>(this IObservable<TSource> source,
                                                                               TimeSpan threshold) {
            return source.Publish(sp =>
                sp.GroupByUntil(_ => true, _ => Observable.Timer(threshold))
                  .SelectMany(i => i.ToList()));
        }
    }
}