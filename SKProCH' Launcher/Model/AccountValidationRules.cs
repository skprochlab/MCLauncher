using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Data;

namespace Launcher.Model {
    public sealed class NicknameValidationRule : ValidationRule {
        public override ValidationResult Validate(object value, CultureInfo ci) {
            if (value == null || string.IsNullOrWhiteSpace(value.ToString()))
                return new ValidationResult(false, @"Field can't be empty");

            var r = new Regex("^[abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_]{3,16}$");
            if (value.ToString().Length < 3 || value.ToString().Length > 16)
                return new ValidationResult(false, @"Nickmame must be between 3 and 16 characters");
            return r.IsMatch(value.ToString()) ? new ValidationResult(true, null) : new ValidationResult(false, @"Unacceptable symbols");
        }
    }

    public sealed class UUIDValidationRule : ValidationRule {
        public override ValidationResult Validate(object value, CultureInfo ci) {
            if (value == null || string.IsNullOrWhiteSpace(value.ToString()))
                return new ValidationResult(true, null);

            var r = new Regex("^[a-f0-9]{8}(-?[a-f0-9]{4}){4}[a-f0-9]{8}$");
            return r.IsMatch(value.ToString()) ? new ValidationResult(true, null) : new ValidationResult(false, @"Invalid UUID");
        }
    }

    public sealed class UUIDDashesConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            return value.ToString().Replace(@"-", "");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            return value.ToString().Replace(@"-", "");
        }
    }
}