﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Launcher.Model;
using Launcher.Model.Config;
using Launcher.Model.Java;
using Launcher.ViewModel.PropertyTabs.Tabs;
using Newtonsoft.Json;
using Shared;
using SpecificMinecraftVersionManifest;

namespace Launcher {
    [Magic]
    public partial class Modpack : ModpackInfo {
        public delegate void OnMinecraftProcessChangedArgs(Modpack modpack, Process process, bool isLaunched);

        [JsonIgnore] private string _groupName;
        private bool _isFavourite;

        public string GroupName {
            get => _groupName;
            set {
                var oldValue = _groupName;
                _groupName = value;
                if (oldValue != null)
                    Config.LocalModpacks_OnChange(null, null);
            }
        }

        [JsonIgnore] public int Id { get; set; } = UidProvider.GetUid();
        [JsonIgnore] public double ProgressPercentage { get; set; } = -1;
        [JsonIgnore] public string ProgressName { get; set; }
        [JsonIgnore] public string ProgressDescription { get; set; }

        [JsonIgnore] public TabLogVM ThisModpackTabLog { get; set; } = new TabLogVM();
        [JsonIgnore] public bool IsLaunched { get; set; }
        [JsonIgnore] public Process MinecraftProcess { get; set; }
        [JsonIgnore] public bool IsLogRequested { get; set; }

        [JsonIgnore] public SemaphoreSlim Semaphore { get; } = new SemaphoreSlim(1);

        [JsonIgnore] public string PathToClient => Path.Combine(Directory.GetCurrentDirectory(), @"instances", ModpackName);

        public JavaControl JavaSettings { get; set; }

        public string CurrentVersion { get; set; }

        public bool IsFavourite {
            get => _isFavourite;
            set {
                _isFavourite = value;
                if (!IsSerealizing)
                    Config.OnFavouritesChanged();
            }
        }

        public string MinecraftVersion { get; set; } //Minecraft Version
        public IList<string> MinecraftLibs { get; set; } = new List<string>();
        public IList<Library> MinecraftNatives { get; set; } = new List<Library>();
        public string AssetsIndexPath { get; set; }
        public string ForgeVersion { get; set; } //Forge Version
        public IList<string> ForgeLibs { get; set; } = new List<string>();
        public string MainClass { get; set; }

        //if null — use default
        public int? AccountId { get; set; }

        [JsonIgnore] public AnotherCommandImplementation RunModpack => new AnotherCommandImplementation(o => { StartMinecraft(); });
        [JsonIgnore] public AnotherCommandImplementation RunModpackOffline => new AnotherCommandImplementation(o => { StartMinecraft(true); });

        [JsonIgnore]
        public AnotherCommandImplementation EditModpack => new AnotherCommandImplementation(o => { WindowManager.PropertyTabsContainer.TryOpen(Id); });

        [JsonIgnore]
        public AnotherCommandImplementation OpenModpackFolder => new AnotherCommandImplementation(o => { Process.Start("explorer.exe", PathToClient); });

        public event OnMinecraftProcessChangedArgs ProcessStateChanged;
        public event EventHandler LogRequest;
    }
}