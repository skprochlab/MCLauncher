﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Ionic.Zip;
using Launcher.Model;
using Launcher.Model.Additional;
using Launcher.Model.Config;
using Launcher.Model.Java;
using MinecraftAssetsManifest;
using Newtonsoft.Json;
using Rasyidf.Localization;
using Shared.Model;
using Shared.Model.ExtensionClasses;
using SpecificMinecraftVersionManifest;
using Utilities = Launcher.Model.Utilities;

namespace Launcher {
    public partial class Modpack : ISaveable {
        public void Save() {
            var path = Path.Combine(Directory.GetCurrentDirectory(), @"instances", ModpackName, @"modpack-info.json");
            Directory.CreateDirectory(Path.GetDirectoryName(path));
            File.WriteAllText(path, JsonConvert.SerializeObject(this, Formatting.Indented));
        }

        public void InstallMinecraft(string version = null, bool hardupdate = false) {
            Task.Run(async () => {
                await Semaphore.WaitAsync();
                ProgressPercentage = 0;
                MinecraftVersion = version ?? MinecraftVersion;
                ProgressName = string.Format(LocalizationService.GetString(@"Modpack", @"InstallingMinecraft", @"Minecraft {0} Installation"),
                    MinecraftVersion);

                GlobalDB.UpHistory(Id.ToString());
                MinecraftLibs.Clear();
                MinecraftNatives.Clear();
                AssetsIndexPath = null;

                var manifest = GameUtilities.GetMinecraftVersionManifest(MinecraftVersion);
                ProgressPercentage = 10;

                ProgressDescription = LocalizationService.GetString(@"Modpack", @"ProcessingAssets", @"Processing assets");
                Utilities.DownloadFile(manifest.AssetIndex.Url, $@"assets/indexes/{MinecraftVersion}.json");
                var assets = AssetsManifest.FromJson(File.ReadAllText($"assets/indexes/{MinecraftVersion}.json")).Objects;
                assets = assets.GroupBy(pair => pair.Value.Hash).Select(group => group.First()).ToDictionary(pair => pair.Key, pair => pair.Value);
                Parallel.ForEach(assets, new ParallelOptions {MaxDegreeOfParallelism = 666}, pair => {
                    GameUtilities.DownloadMinecraftAssets(pair, hardupdate);
                    ProgressPercentage += 40d / assets.Count;
                });

                ProgressDescription = LocalizationService.GetString(@"Modpack", @"ProcessingLibs", @"Processing libraries");
                Parallel.ForEach(manifest.Libraries, new ParallelOptions {MaxDegreeOfParallelism = 666}, lib => {
                    //If Artifact - simple download, not unzip to binaries
                    if (lib.Downloads.Artifact != null) {
                        var result = GameUtilities.DownloadMinecraftLibrary(lib, hardupdate);
                        MinecraftLibs.Add(PathUtils.GetRelativePath(Directory.GetCurrentDirectory(),
                            result.Path));
                    }

                    //If Classifiers - download and unzip on startup 
                    if (lib.Downloads.Classifiers != null) {
                        var result = GameUtilities.DownloadMinecraftNative(lib, hardupdate);
                        MinecraftNatives.Add((Library) result.InfoObject);
                    }

                    ProgressPercentage += 40d / manifest.Libraries.Count;
                });
                manifest.Downloads.Client.Path = Path.Combine(@"versions", version, $@"{version}.jar");
                GameUtilities.DownloadMinecraftJar(manifest.Downloads.Client);

                MainClass = manifest.MainClass;
                AssetsIndexPath = PathUtils.GetRelativePath(Directory.GetCurrentDirectory(),
                    Path.Combine(Directory.GetCurrentDirectory(), @"assets\\indexes", MinecraftVersion + @".json"));
                ProgressPercentage = -1;

                Save();
                Semaphore.Release();
            });
        }

        public void MapToResources() {
            if (Version.Parse(MinecraftVersion).CompareTo(Version.Parse("1.6")) >= 0) return;
            var assets = AssetsManifest.FromJson(File.ReadAllText($"assets/indexes/{MinecraftVersion}.json"));
            if (assets.MapToResources != true) return;
            Parallel.ForEach(assets.Objects.GroupBy(pair => pair.Key).Select(group => group.First()).ToDictionary(pair => pair.Key, pair => pair.Value),
                keyValuePair => {
                    var filepath = Path.Combine(PathToClient, ".minecraft", "resources", keyValuePair.Key.Replace(@"/", @"\"));
                    // ReSharper disable once AssignNullToNotNullAttribute
                    Directory.CreateDirectory(Path.GetDirectoryName(filepath));
                    File.Copy(Path.Combine("assets", "objects", keyValuePair.Value.Hash.Substring(0, 2), keyValuePair.Value.Hash), filepath, true);
                });
        }

        public void UnpackNatives() {
            foreach (var lib in MinecraftNatives) {
                var nativesPath = Path.Combine(PathToClient, "natives");
                var classifier = lib.GetClassifier();
                if (classifier == null)
                    continue;
                var path = Path.Combine(Directory.GetCurrentDirectory(), @"libraries/", classifier.Path.Replace(@".jar", ""), classifier.Sha1 + @".jar");
                using (var zf = ZipFile.Read(path)) {
                    if (lib.Extract == null)
                        lib.Extract = new Extract {Exclude = new List<string>()};
                    foreach (var zipEntry in zf)
                        if (!lib.Extract.Exclude.Any(s => zipEntry.FileName.Contains(s)))
                            try {
                                zipEntry.Extract(nativesPath);
                            }
                            catch (Exception) { }
                }
            }
        }

        public void ClearBinaries() {
            Utilities.RecursiveFoldersDelete(Path.Combine(PathToClient, "natives"));
        }

        private async Task<JavaControl> GetFinalJava() {
            var global = JavaUtils.GetGlobalJavaSetting();

            var path = JavaSettings?.JavaPath ?? global.JavaPath;
            if (path == "DownloadedJava")
                path = await JavaDownload.ResolveJavaPath(this);
            else if (path != "javaw")
                path = Path.Combine(Shared.Model.Utilities.GetDirectory(path), "javaw.exe");

            return new JavaControl {
                JavaPath = path,
                MinMem = JavaSettings?.MinMem ?? global.MinMem,
                MaxMem = JavaSettings?.MinMem ?? global.MinMem
            };
        }

        [Localizable(false)]
        public async Task<string> BuildCMD(string[] credentials, JavaControl javaControl) {
            var cmd = new StringBuilder();
            cmd.Append("-Xdiag ");
            cmd.Append($"-Xms{javaControl.MinMem}m ");
            cmd.Append($"-Xmx{javaControl.MaxMem}m ");

            cmd.Append("-XX:HeapDumpPath=MojangTricksIntelDriversForPerformance_javaw.exe_minecraft.exe.heapdump ");

            cmd.Append($"-Djava.library.path=\"{Path.Combine(PathToClient, "natives")}\" ");
            cmd.Append($"-Dorg.lwjgl.librarypath=\"{Path.Combine(PathToClient, "natives")}\" ");

            cmd.Append("-cp ");

            foreach (var lib in MinecraftLibs) cmd.Append($"\"{Path.GetFullPath(lib)}\";");

            cmd.Append($"\"{Path.GetFullPath(Path.Combine("versions", MinecraftVersion, $"{MinecraftVersion}.jar"))}\" ");
            cmd.Append($"{MainClass ?? "net.minecraft.client.main.Main"} ");


            cmd.Append("--width 1080 ");
            cmd.Append("--height 720 ");

            cmd.Append($"--username {credentials[0]} ");
            cmd.Append($"--version {MinecraftVersion} ");
            cmd.Append($"--gameDir \"{Path.Combine(PathToClient, ".minecraft")}\" ");
            cmd.Append($"--assetsDir \"{Path.Combine(Directory.GetCurrentDirectory(), "assets")}\" ");
            cmd.Append($"--assetIndex {MinecraftVersion} ");
            cmd.Append($"--uuid {credentials[1]}  ");
            cmd.Append($"--accessToken {credentials[2]} ");
            cmd.Append("--userType mojang ");
            cmd.Append("--userProperties {} ");

            return cmd.ToString();
        }

        [Localizable(false)]
        public void StartMinecraft(bool forceOffline = false) {
            Task.Run(async () => {
                IsLaunched = true;

                try {
                    if (MinecraftProcess != null && !MinecraftProcess.HasExited) return;
                }
                catch (InvalidOperationException) {
                    MinecraftProcess = null;
                    IsLaunched = false;
                    return;
                }

                GlobalDB.UpHistory(Id.ToString());
                OnLogRequest();
                if (Convert.ToBoolean(GlobalDB.GlobalSettings.FindById("ShowConsoleAtStartup") ?? "True"))
                    WindowManager.PropertyTabsContainer.TravelTo(Id, @"Log");
                ThisModpackTabLog.ClearLog();

                MinecraftProcess = FindLaunchedMinecraft();
                if (MinecraftProcess == null) {
                    ClearBinaries();
                    UnpackNatives();
                    MapToResources();
                    try {
                        Directory.CreateDirectory(Path.Combine(PathToClient, @".minecraft"));
                        // ReSharper disable once UseObjectOrCollectionInitializer
                        var startInfo = new ProcessStartInfo
                            {UseShellExecute = false, RedirectStandardError = true, RedirectStandardOutput = true, CreateNoWindow = true};

                        var javaControl = await GetFinalJava();
                        startInfo.FileName = javaControl.JavaPath;
                        ThisModpackTabLog.WriteToLog("Java path is:");
                        ThisModpackTabLog.WriteToLog(startInfo.FileName + Environment.NewLine);

                        startInfo.WorkingDirectory = Path.Combine(PathToClient, @".minecraft");
                        ThisModpackTabLog.WriteToLog("Minecraft folder is:");
                        ThisModpackTabLog.WriteToLog(startInfo.WorkingDirectory + Environment.NewLine);

                        var credentials = await AccountStorage.GetUserData(AccountId, forceOffline);
                        startInfo.Arguments = await BuildCMD(credentials, javaControl);
                        ThisModpackTabLog.WriteToLog($"Username: {credentials[0]}");
                        ThisModpackTabLog.WriteToLog($"UUID: {credentials[1]}" + Environment.NewLine);

                        MinecraftProcess = new Process {StartInfo = startInfo};

                        MinecraftProcess.OutputDataReceived += (sender, args) => { ThisModpackTabLog.WriteToLog(args.Data); };
                        MinecraftProcess.ErrorDataReceived += (sender, args) => { ThisModpackTabLog.WriteToLog(args.Data); };
                        MinecraftProcess.Start();
                        MinecraftProcess.EnableRaisingEvents = true;
                        MinecraftProcess.BeginErrorReadLine();
                        MinecraftProcess.BeginOutputReadLine();
                        OnProcessStateChanged(this, MinecraftProcess, true);
                    }
                    catch (AccountException) {
                        IsLaunched = false;
                        ThisModpackTabLog.WriteToLog(LocalizationService.GetString(@"Account", @"Error",
                            @"Check your Minecraft profile, there is something wrong with it."));
                    }
                    catch (Exception e) {
                        IsLaunched = false;
                        ThisModpackTabLog.WriteToLog(e.ToString());
                    }
                }
                else {
                    ThisModpackTabLog.AttachToFile(MinecraftProcess, Path.Combine(PathToClient, @".minecraft", @"logs", @"latest.log"));
                }

                if (MinecraftProcess != null)
                    MinecraftProcess.Exited += (sender, args) => {
                        OnProcessStateChanged(this, MinecraftProcess, false);
                        MinecraftProcess = null;
                        IsLaunched = false;

                        ThisModpackTabLog.WriteToLog(string.Format(LocalizationService.GetString(@"Log", @"ExitCode", @"Process exited with code {0}."),
                            ((Process) sender).ExitCode));

                        if (Convert.ToBoolean(GlobalDB.GlobalSettings.FindById("CloseConsole") ?? "False"))
                            Application.Current.Dispatcher?.Invoke(() => WindowManager.PropertyTabsContainer.GetById(Id)?.Close());

                        ClearBinaries();
                    };
            });
        }

        public void KillMinecraft() {
            if (MinecraftProcess == null || MinecraftProcess.HasExited) {
                IsLaunched = false;
                return;
            }

            MinecraftProcess?.Kill();
        }

        public Process FindLaunchedMinecraft() {
            foreach (var process in Process.GetProcessesByName("javaw"))
                try {
                    if (process.GetCommandLine().Contains($@"--gameDir ""{Path.Combine(PathToClient, ".minecraft")}"" ")) return process;
                }
                catch (Exception) {
                    // ignored
                }

            return null;
        }


        protected virtual void OnProcessStateChanged(Modpack sendermodpack, Process MinecraftProcess, bool islaunched) {
            ProcessStateChanged?.Invoke(sendermodpack, MinecraftProcess, islaunched);
        }

        protected virtual void OnLogRequest() {
            LogRequest?.Invoke(this, EventArgs.Empty);
            IsLogRequested = true;
        }
    }
}