﻿using Shared;
using Shared.Model;

namespace Launcher {
    [Magic]
    public class ModpackInfo : AutoSaveable {
        public string ModpackName { get; set; }
        public string ModpackIcon { get; set; }
        public string UpdatesUrl { get; set; } = null;
        public bool IsOnlyLocal { get; set; } = false;
    }
}