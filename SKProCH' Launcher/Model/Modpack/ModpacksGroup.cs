﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Launcher.Model.Config;
using Newtonsoft.Json;
using Rasyidf.Localization;
using Shared;
using MessageBox = Shared.View.MessageBox;

namespace Launcher {
    [Magic]
    public sealed class ModpacksGroup : PropertyChangedBase {
        public string GroupName { get; set; } = "";

        [JsonIgnore]
        public ICommand RemoveGroup =>
            new AnotherCommandImplementation(o => {
                var result = MessageBox.Show(LocalizationService.GetString("Modpack", "DeleteGroup", "Delete group?"),
                    string.Format(LocalizationService.GetString("Modpack", "DeleteGroupDesc", "Are you sure you want to delete the group {0}?"), GroupName),
                    MessageBoxButton.YesNo);
                if (result != MessageBoxResult.Yes) return;
                foreach (var includedModpack in IncludedModpacks) {
                    includedModpack.GroupName = "";
                    includedModpack.SafeSave();
                }

                Config.GroupsFromConfig.Remove(Config.GroupsFromConfig.First(x => x.GroupName == GroupName));
            });

        //Свойство для привязки
        [JsonIgnore] public bool IsDefault => string.IsNullOrWhiteSpace(GroupName);
        [JsonIgnore] public ObservableCollection<Modpack> IncludedModpacks { get; set; } = new ObservableCollection<Modpack>();
        public bool IsExpanded { get; set; } = true;
    }
}