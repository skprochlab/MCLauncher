﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;

namespace Launcher.Model.Additional {
    public static class AppMessageExchange {
        private static ServiceHost _serviceHost;

        public static void SetServer() {
            var address = new Uri("net.tcp://localhost:28123/SKPAppContract");
            var binding = new NetTcpBinding();
            var contract = typeof(SKPAppContract);
            _serviceHost = new ServiceHost(typeof(AppService));
            _serviceHost.AddServiceEndpoint(contract, binding, address);
            _serviceHost.Open();
            AppDomain.CurrentDomain.ProcessExit += (sender, args) => CloseServer();
        }

        public static void CloseServer() {
            try {
                _serviceHost?.Close();
            }
            catch (Exception) {
                // ignored
            }
        }

        public static SKPAppContract OpenChannel() {
            var address = new Uri("net.tcp://localhost:28123/SKPAppContract");
            var binding = new NetTcpBinding();
            var endpoint = new EndpointAddress(address);
            var channelFactory = new ChannelFactory<SKPAppContract>(binding, endpoint);
            var channel = channelFactory.CreateChannel();
            return channel;
        }

        public static void SendCommand(this SKPAppContract c, string command) {
            c.SendCommand(command);
        }

        [Localizable(false)]
        [ServiceContract]
        public interface SKPAppContract {
            [OperationContract]
            void SendCommand(string command);
        }

        public sealed class AppService : SKPAppContract {
            public void SendCommand(string command) {
                switch (command) {
                    case "TryOpenMV":
                        App.TryOpenVM();
                        break;
                    default:
                        Debug.WriteLine(command);
                        break;
                }
            }
        }
    }
}