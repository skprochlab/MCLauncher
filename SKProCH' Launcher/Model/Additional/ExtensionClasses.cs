﻿using System;
using SpecificMinecraftVersionManifest;

namespace Launcher.Model.Additional {
    public static class InstallExtensions {
        public static ServerClass GetClassifier(this Library lib) {
            var is64BitOs = Environment.Is64BitOperatingSystem;
            ServerClass classifier = null;
            if (is64BitOs ? lib.Downloads.Classifiers.NativesWindows64 != null : lib.Downloads.Classifiers.NativesWindows32 != null)
                classifier = is64BitOs ? lib.Downloads.Classifiers.NativesWindows64 : lib.Downloads.Classifiers.NativesWindows32;
            else if (lib.Downloads.Classifiers.NativesWindows != null) classifier = lib.Downloads.Classifiers.NativesWindows;
            return classifier;
        }
    }
}