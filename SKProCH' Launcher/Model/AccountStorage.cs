﻿using System;
using System.ComponentModel;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Launcher.Model.Config;
using Launcher.View.Additional;
using Launcher.ViewModel.Additional;
using LiteDB;
using MojangSharp.Endpoints;
using MojangSharp.Responses;
using Rasyidf.Localization;
using Shared;

namespace Launcher.Model {
    [Localizable(false)]
    [Magic]
    internal static class AccountStorage {
        static AccountStorage() {
            Accounts.EnsureIndex(abase => abase.Id);
        }

        public static LiteCollection<AccountBase> Accounts { get; set; } = GlobalDB.Database.GetCollection<AccountBase>("Accounts");

        public static int? DefaultAccount {
            get => int.TryParse(GlobalDB.GlobalSettings.FindById("DefaultAccount"), out var result) ? result : (int?) null;
            set => GlobalDB.GlobalSettings.Upsert("DefaultAccount", new Entity(value));
        }

        public static event EventHandler Changed;

        public static async Task<AuthenticateResponse> AddLicensed(string login, string pass, int? id) {
            var auth = await new Authenticate(new Credentials {
                Username = login,
                Password = pass
            }).PerformRequestAsync();
            if (auth.IsSuccess)
                AddLicensedToStorage(auth.AccessToken, auth.SelectedProfile.PlayerName, login, auth.AvailableProfiles[0].Value, id);
            return auth;
        }

        private static void AddLicensedToStorage(string accessToken, string nickname, string login, string uuid, int? id) {
            var accountToAdd = new AccountBase {
                IsLicensed = true, Nickname = nickname,
                AccessToken = accessToken, Login = login, Uuid = uuid
            };

            AddAccount(accountToAdd, id);
        }

        public static void AddUnlicensed(string nickname, string uuid, int? id = null) {
            var accountToAdd = new AccountBase {
                IsLicensed = false,
                Nickname = nickname, Uuid = string.IsNullOrWhiteSpace(uuid) ? GetUuid(nickname) : uuid
            };

            AddAccount(accountToAdd, id);
        }

        public static void AddAccount(AccountBase accountBaseToAdd, int? idToRemove) {
            TryRemoveAccount(idToRemove);
            Accounts.Insert(accountBaseToAdd);
            DefaultAccount = accountBaseToAdd.Id;
            OnChanged();
        }

        public static void TryRemoveAccount(int? id) {
            Accounts.Delete(Convert.ToString(id?.ToString() ?? string.Empty));
        }

        public static async Task<AccountBase> ValidateAndGetAccount(int? uid, int? modpackId = null) {
            if (Accounts.Count() == 0) {
                await Application.Current.Dispatcher.BeginInvoke(new Action(() => WindowManager.PropertyTabsContainer.TravelTo(-1, "Accounts",
                    LocalizationService.GetString("Account", "AddFirst",
                        "Add account first"))));
                throw new AccountException();
            }

            if (DefaultAccount == null) {
                await Application.Current.Dispatcher.BeginInvoke(new Action(() => WindowManager.PropertyTabsContainer.TravelTo(-1, "Accounts",
                    LocalizationService.GetString("Account", "ChooseDefault",
                        "Choose default account"))));
                throw new AccountException();
            }

            uid = uid == -1 ? null : uid;
            var account = Accounts.FindById(uid ?? DefaultAccount);

            if (account != null) return account;
            await Application.Current.Dispatcher.BeginInvoke(new Action(() => WindowManager.PropertyTabsContainer.TravelTo(modpackId ?? -1, "Accounts",
                LocalizationService.GetString("AccountBase", "AccountMissing",
                    "Account unavailable. Set default account"))));
            throw new AccountException();
        }

        public static async Task<string[]> GetUserData(int? uid, bool forceOffline = false) {
            var account = await ValidateAndGetAccount(uid);
            if (forceOffline || !account.IsLicensed)
                return new[] {
                    account.Nickname, string.IsNullOrWhiteSpace(account.Uuid) ? GetUuid(account.Nickname) : account.Uuid, "123456789123456789123456789"
                };
            return await Authenticate(uid);
        }

        public static async Task<string[]> Authenticate(int? uid) {
            var account = await ValidateAndGetAccount(uid);

            var validateResponse = await new Validate(account.AccessToken).PerformRequestAsync();
            if (validateResponse.IsSuccess)
                return new[] {
                    account.Nickname, account.Uuid, account.AccessToken
                };

            var refreshResponse = await new Refresh(account.AccessToken).PerformRequestAsync();
            if (refreshResponse.IsSuccess) {
                account.AccessToken = refreshResponse.AccessToken;
                Accounts.Update(account);
            }

            else {
                bool? result = null;

                Application.Current.Dispatcher.Invoke(new Action(async () => {
                    result = await WindowManager.PropertyTabsContainer.ShowModalDialog(-1, new AddAccountDialog {
                        DataContext =
                            new
                                AddAccountDialogVM((int) (uid ?? DefaultAccount),
                                    LocalizationService
                                       .GetString("AccountBase",
                                            "LoginFailed",
                                            "Unable to login to Mojang account. You must authorize again."))
                    });
                }));

                if (result != true)
                    throw new AccountException();
            }

            return new[] {
                account.Nickname, account.Uuid, account.AccessToken
            };
        }

        public static string GetUuid(string playerName, bool isOffline = true) {
            return NameUUIDFromBytes(Encoding.UTF8.GetBytes(isOffline ? $"OfflinePlayer:{playerName}" : playerName));
        }

        public static string NameUUIDFromBytes(byte[] input) {
            var md5 = MD5.Create();
            var hash = md5.ComputeHash(input);
            hash[6] &= 0x0f;
            hash[6] |= 0x30;
            hash[8] &= 0x3f;
            hash[8] |= 0x80;
            var hex = BitConverter.ToString(hash).Replace("-", string.Empty).ToLower();
            return hex.Insert(8, "-").Insert(13, "-").Insert(18, "-").Insert(23, "-");
        }

        public static void OnChanged() {
            Changed?.Invoke(null, EventArgs.Empty);
        }
    }

    [Magic]
    public sealed class AccountBase : PropertyChangedBase {
        // ReSharper disable once InconsistentNaming
        public int Id { get; set; }

        public string Login { get; set; }
        public string AccessToken { get; set; }

        public bool IsLicensed { get; set; }

        public string Nickname { get; set; }

        public string Uuid { get; set; }

        public string BodyRenderUrl => $@"https://crafatar.com/renders/body/{Uuid}?default=MHF_Steve&overlay=true";
    }
}