﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Shared;

namespace Launcher.Model.Config {
    [Magic]
    public sealed class Config : PropertyChangedBase {
        private static readonly object _lock = new object();
        public static ObservableCollection<ModpacksGroup> GroupsFromConfig;
        private static ObservableCollection<Modpack> _localModpacks;
        private static ObservableCollection<ModpacksGroup> _localGroups = new ObservableCollection<ModpacksGroup>();

        public static ObservableCollection<Modpack> LocalModpacks {
            get => _localModpacks;
            set {
                _localModpacks = value;
                NotifyStaticPropertyChanged("LocalModpacks");
            }
        }

        public static ObservableCollection<ModpacksGroup> LocalGroups {
            get => _localGroups;
            set {
                _localGroups = value;
                NotifyStaticPropertyChanged("LocalGroups");
            }
        }

        public static void Init() {
            Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), "instances"));

            GroupsFromConfig = new ObservableCollection<ModpacksGroup>();
            GroupsFromConfig = JsonConvert.DeserializeObject<ObservableCollection<ModpacksGroup>>((string) App.Reg.GetValue("Groups", "")) ??
                               new ObservableCollection<ModpacksGroup>();

            LocalModpacks = new ObservableCollection<Modpack>();
            foreach (var directory in Directory.GetDirectories(Path.Combine(Directory.GetCurrentDirectory(), "instances")))
                if (File.Exists(Path.Combine(directory, "modpack-info.json")))
                    LocalModpacks.Add(JsonConvert.DeserializeObject<Modpack>(File.ReadAllText(Path.Combine(directory, "modpack-info.json"))));

            LocalModpacks.CollectionChanged += LocalModpacks_OnChange;
            GroupsFromConfig.CollectionChanged += (sender, args) => App.Reg.SetValue("Groups", JsonConvert.SerializeObject(GroupsFromConfig));
            LocalModpacks_OnChange(null, null);
        }

        private static ModpacksGroup LoadByGroupName(string name) {
            if (GroupsFromConfig.All(x => x.GroupName != name))
                GroupsFromConfig.Add(new ModpacksGroup {GroupName = name, IncludedModpacks = new ObservableCollection<Modpack>(), IsExpanded = true});

            return GroupsFromConfig.First(x => x.GroupName == name);
        }

        public static void LocalModpacks_OnChange(object sender, EventArgs e) {
            lock (_lock) {
                LocalGroups.Clear();

                foreach (var g in LocalModpacks.GroupBy(x => x.GroupName)) {
                    var group = new ModpacksGroup {GroupName = g.Key, IsExpanded = LoadByGroupName(g.Key).IsExpanded};

                    foreach (var modpack in g) {
                        group.IncludedModpacks.Add(null);
                        group.IncludedModpacks[group.IncludedModpacks.Count - 1] = LocalModpacks.First(x => x.Id == modpack.Id);
                    }

                    LocalGroups.Add(group);
                }
            }
        }

        public static Modpack GetModpackByUid(int uid) {
            return LocalModpacks.FirstOrDefault(x => x.Id == uid);
        }


        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;

        private static void NotifyStaticPropertyChanged([Localizable(false)] string propertyName) {
            StaticPropertyChanged?.Invoke(null, new PropertyChangedEventArgs(propertyName));
        }

        public static event EventHandler FavouritesChanged;

        public static void OnFavouritesChanged() {
            FavouritesChanged?.Invoke(null, EventArgs.Empty);
        }

        public static List<Modpack> FindFavourites() {
            return LocalModpacks.Where(modpack => modpack.IsFavourite).ToList();
        }
    }
}