﻿using System.ComponentModel;
using Shared;

namespace Launcher.Model.Config {
    public class InheritedFromConfig : PropertyChangedBase {
        public InheritedFromConfig() {
            Config.StaticPropertyChanged += ConfigOnStaticPropertyChanged;
        }

        private void ConfigOnStaticPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e != null) RaisePropertyChanged(e.PropertyName);
        }
    }
}