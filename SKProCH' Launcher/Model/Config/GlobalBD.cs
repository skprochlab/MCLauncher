using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LiteDB;
using MojangSharp.Api;
using MojangSharp.Endpoints;

namespace Launcher.Model.Config {
    public static class GlobalDB {
        private static readonly Lazy<LiteDatabase> _database = new Lazy<LiteDatabase>(Init);

        public static LiteCollection<Entity> GlobalSettings = Database.GetCollection<Entity>(@"Global");
        public static LiteCollection<Entity> ModpacksHistory = Database.GetCollection<Entity>(@"History");

        public static LiteDatabase Database => _database.Value;

        private static LiteDatabase Init() {
            Directory.CreateDirectory("config");
            var tempdb = new LiteDatabase(@"config/GlobalConfig.db");
            //Updating database in future
            tempdb.UserVersion = 1;
            ApplyGlobalValues(tempdb);
            return tempdb;
        }

        private static void ApplyGlobalValues(LiteDatabase db) {
            var global = db.GetCollection<Entity>(@"Global");

            #region ClientToken

            var ClientToken = global.FindById(@"ClientToken")?.Value;
            if (ClientToken == null) {
                ClientToken = Guid.NewGuid().ToString();
                global.Upsert(@"ClientToken", new Entity(ClientToken));
            }

            Requester.ClientToken = ClientToken;

            #endregion
        }

        public static void UpdateAccounts() {
            var accounts = Database.GetCollection<AccountBase>(@"Accounts").FindAll().ToList();
            Parallel.ForEach(accounts, async accountBase => {
                if (!accountBase.IsLicensed) return;
                var oldNick = accountBase.Nickname;
                var profile = await new Profile(accountBase.Uuid).PerformRequestAsync();
                if (oldNick == profile?.Properties?.ProfileName || string.IsNullOrWhiteSpace(profile?.Properties?.ProfileName)) return;
                accountBase.Nickname = profile.Properties.ProfileName;
                Database.GetCollection<AccountBase>(@"Accounts").Upsert(accountBase);
            });
        }

        public static event EventHandler HistoryChanged;

        public static void OnHistoryChanged() {
            HistoryChanged?.Invoke(null, EventArgs.Empty);
        }

        public static void UpHistory(string id) {
            ModpacksHistory.DeleteMany(entity => entity.Value == id);
            while (ModpacksHistory.Count() > 4) ModpacksHistory.Delete(ModpacksHistory.Min());

            ModpacksHistory.Insert(new Entity(id));
            OnHistoryChanged();
        }
    }
}