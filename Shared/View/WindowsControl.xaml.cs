﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Interop;
using MaterialDesignThemes.Wpf;
using Shared.Model;
using MouseEventArgs = System.Windows.Input.MouseEventArgs;
using Point = System.Windows.Point;
using UserControl = System.Windows.Controls.UserControl;

namespace Shared.View {
    public sealed partial class WindowsControl : UserControl {
        public static readonly DependencyProperty IsCloseWindowProperty =
            DependencyProperty.Register("IsCloseWindow", typeof(bool), typeof(WindowsControl), new PropertyMetadata(true));

        public static readonly DependencyProperty CanManageWindowProperty =
            DependencyProperty.Register("CanManageWindow", typeof(ResizeMode), typeof(WindowsControl), new PropertyMetadata(ResizeMode.CanResizeWithGrip));

        public static readonly DependencyProperty IsMaximizedProperty = DependencyProperty.Register(
            "IsMaximized", typeof(bool), typeof(WindowsControl), new PropertyMetadata(default(bool)));

        private bool needMove;

        public WindowsControl() {
            InitializeComponent();
            DataContext = this;

            if (DesignerProperties.GetIsInDesignMode(this)) return;

            Dispatcher.BeginInvoke(new Action(() => { Window.GetWindow(this).StateChanged += WindowStateChanged; }));

            var minimizeicon = new PackIcon {Kind = PackIconKind.WindowMinimize};
            minimizeicon.SetResourceReference(ForegroundProperty, "MaterialDesignBody");
            MinimizeButton.Content = minimizeicon;

            var stateicon = new PackIcon {Kind = PackIconKind.WindowMaximize};
            stateicon.SetResourceReference(ForegroundProperty, "MaterialDesignBody");
            WindowStateButton.Content = stateicon;

            var closeicon = new PackIcon {Kind = PackIconKind.Close};
            closeicon.SetResourceReference(ForegroundProperty, "MaterialDesignBody");
            ExitButton.Content = closeicon;
        }

        public bool IsCloseWindow {
            get => (bool) GetValue(IsCloseWindowProperty);
            set => SetValue(IsCloseWindowProperty, value);
        }

        public bool IsManageButtonVisible {
            get => (ResizeMode) GetValue(CanManageWindowProperty) != ResizeMode.NoResize;
            set => SetValue(CanManageWindowProperty, value ? ResizeMode.CanResizeWithGrip : ResizeMode.NoResize);
        }

        public ResizeMode CanManageWindow {
            get => (ResizeMode) GetValue(CanManageWindowProperty);
            set => SetValue(CanManageWindowProperty, value);
        }

        public bool IsMaximized {
            get => (bool) GetValue(IsMaximizedProperty);
            set => SetValue(IsMaximizedProperty, value);
        }

        public static bool GetIsCloseWindow(DependencyObject obj) {
            return (bool) obj.GetValue(IsCloseWindowProperty);
        }

        public static void SetIsCloseWindow(DependencyObject obj, bool value) {
            obj.SetValue(IsCloseWindowProperty, value);
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e) {
            if (IsCloseWindow)
                Window.GetWindow(this).Close();
            else
                ExitUtilities.InitiateExit();
        }

        private void OnDragMouseMove(object sender, EventArgs e) {
            ((Window) sender).LocationChanged -= OnDragMouseMove;
            UnMaximize((Window) sender, false);
        }

        private void MinimizeWindow(object sender, RoutedEventArgs e) {
            Window.GetWindow(this).WindowState = WindowState.Minimized;
            IsMaximized = true;
        }

        private void ChangeWindowState(object sender, RoutedEventArgs e) {
            var form = Window.GetWindow(this);
            if (form.Tag == null)
                Maximize(form);
            else
                UnMaximize(form);
        }

        private void Maximize(Window form) {
            form.Tag = form.Width + ";" + form.Height + ";" + form.Left + ";" +
                       form.Top;
            var screen = WpfScreen.GetScreenFrom(form);
            form.Width = screen.WorkingArea.Width;
            form.Height = screen.WorkingArea.Height;
            form.Left = screen.WorkingArea.Left;
            form.Top = screen.WorkingArea.Top;
            form.WindowState = WindowState.Normal;
            (WindowStateButton.Content as PackIcon).Kind = PackIconKind.WindowRestore;
            form.ResizeMode = ResizeMode.NoResize;
            IsMaximized = true;
        }

        private void UnMaximize(Window form, bool move = true) {
            if (form.Tag == null) return;
            var sizes = new List<int>(form.Tag.ToString().Split(';').Select(int.Parse));
            form.Width = sizes[0];
            form.Height = sizes[1];
            if (move) {
                form.Left = sizes[2];
                form.Top = sizes[3];
            }

            form.Tag = null;
            form.WindowState = WindowState.Normal;
            (WindowStateButton.Content as PackIcon).Kind = PackIconKind.WindowMaximize;
            form.ResizeMode = ResizeMode.CanResizeWithGrip;
            IsMaximized = false;
        }

        private void WindowStateChanged(object sender, EventArgs e) {
            var form = Window.GetWindow(this);
            if (form.WindowState == WindowState.Maximized)
                ChangeWindowState(sender, null);
        }

        private void UIElement_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e) {
            needMove = false;
        }

        private void UIElement_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
            var form = Window.GetWindow(this);
            if (e.ClickCount == 2)
                ChangeWindowState(this, null);
            else
                needMove = true;
        }

        private void UIElement_OnMouseMove(object sender, MouseEventArgs e) {
            var form = Window.GetWindow(this);
            if (IsMaximized && needMove) {
                UnMaximize(form, false);

                var point = PointToScreen(e.MouseDevice.GetPosition(this));

                form.Left = point.X - form.Width * 0.5;
                form.Top = point.Y;
            }

            if (!needMove) return;
            needMove = false;
            try {
                form.DragMove();
            }
            catch (Exception) { }
        }
    }

    public sealed class WpfScreen {
        private readonly Screen screen;

        internal WpfScreen(Screen screen) {
            this.screen = screen;
        }

        public static WpfScreen Primary => new WpfScreen(Screen.PrimaryScreen);

        public Rect DeviceBounds => GetRect(screen.Bounds);

        public Rect WorkingArea => GetRect(screen.WorkingArea);

        public bool IsPrimary => screen.Primary;

        public string DeviceName => screen.DeviceName;

        public static IEnumerable<WpfScreen> AllScreens() {
            return Screen.AllScreens.Select(screen => new WpfScreen(screen));
        }

        public static WpfScreen GetScreenFrom(Window window) {
            var windowInteropHelper = new WindowInteropHelper(window);
            var screen = Screen.FromHandle(windowInteropHelper.Handle);
            var wpfScreen = new WpfScreen(screen);
            return wpfScreen;
        }

        public static WpfScreen GetScreenFrom(Point point) {
            var x = (int) Math.Round(point.X);
            var y = (int) Math.Round(point.Y);

            // are x,y device-independent-pixels ??
            var drawingPoint = new System.Drawing.Point(x, y);
            var screen = Screen.FromPoint(drawingPoint);
            var wpfScreen = new WpfScreen(screen);

            return wpfScreen;
        }

        private Rect GetRect(Rectangle value) {
            // should x, y, width, height be device-independent-pixels ??
            return new Rect {
                X = value.X,
                Y = value.Y,
                Width = value.Width,
                Height = value.Height
            };
        }
    }
}