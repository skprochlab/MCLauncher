﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Shared.View {
    public partial class MessageBox : Window {
        private MessageBoxResult _result = MessageBoxResult.None;

        public MessageBox() {
            InitializeComponent();
        }

        private void AddButtons(MessageBoxButton buttons) {
            switch (buttons) {
                case MessageBoxButton.OK:
                    AddButton("OK", MessageBoxResult.OK);
                    break;
                case MessageBoxButton.OKCancel:
                    AddButton("OK", MessageBoxResult.OK);
                    AddButton("Cancel", MessageBoxResult.Cancel, true);
                    break;
                case MessageBoxButton.YesNo:
                    AddButton("Yes", MessageBoxResult.Yes);
                    AddButton("No", MessageBoxResult.No);
                    break;
                case MessageBoxButton.YesNoCancel:
                    AddButton("Yes", MessageBoxResult.Yes);
                    AddButton("No", MessageBoxResult.No);
                    AddButton("Cancel", MessageBoxResult.Cancel, true);
                    break;
                default:
                    throw new ArgumentException(@"Unknown button value", nameof(buttons));
            }
        }

        private void AddButton(string text, MessageBoxResult result, bool isCancel = false) {
            var button = new Button {
                Content = text, IsCancel = isCancel, Style = (Style) FindResource(@"MaterialDesignFlatButton"),
                Margin = new Thickness(8, 0, 8, 0)
            };
            button.Click += (o, args) => {
                _result = result;
                DialogResult = true;
            };
            ButtonContainer.Children.Add(button);
        }

        public static MessageBoxResult Show(string message) {
            return Show("", message);
        }

        public static MessageBoxResult Show(string message, MessageBoxButton buttons) {
            return Show("", message, buttons);
        }

        public static MessageBoxResult Show(string caption, string message, MessageBoxButton buttons = MessageBoxButton.OK) {
            var dialog = new MessageBox {HeaderContainer = {Text = caption}, MessageContainer = {Text = message}};
            if (string.IsNullOrWhiteSpace(caption)) dialog.HeaderContainer.Visibility = Visibility.Collapsed;

            dialog.AddButtons(buttons);
            dialog.ShowDialog();
            return dialog._result;
        }

        private void UIElement_OnPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
            DragMove();
        }
    }
}