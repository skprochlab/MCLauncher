﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;

namespace Shared.View {
    /// <summary>
    ///     Логика взаимодействия для ErrorMessage.xaml
    /// </summary>
    public partial class ErrorMessage : Window {
        private bool isDeferredMovingStarted; //True - Mouse down -> Mouse up without moving -> Move; False - Mouse down -> Move

        private bool isMoving;                  //False - ignore mouse movements and don't scroll
        private readonly double slowdown = 200; //The number 200 is found from experiments, it should be corrected
        private Point? startPosition;

        public ErrorMessage() {
            InitializeComponent();
        }

        private void Hyperlink_OnRequestNavigate(object sender, RequestNavigateEventArgs e) {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }

        private void ErrorMessage_OnDeactivated(object sender, EventArgs e) {
            var window = (Window) sender;
            window.Topmost = true;
        }


        private void ScrollViewer_MouseDown(object sender, MouseButtonEventArgs e) {
            if (isMoving) //Moving with a released wheel and pressing a button
                CancelScrolling();
            else if (e.ChangedButton == MouseButton.Middle && e.ButtonState == MouseButtonState.Pressed)
                if (isMoving == false) //Pressing a wheel the first time
                {
                    isMoving = true;
                    startPosition = e.GetPosition(sender as IInputElement);
                    isDeferredMovingStarted = true; //the default value is true until the opposite value is set

                    //this.AddScrollSign(e.GetPosition(this.topLayer).X, e.GetPosition(this.topLayer).Y);
                }
        }

        //private void ScrollViewer_MouseUp(object sender, MouseButtonEventArgs e) {
        //    if (e.ChangedButton == MouseButton.Middle && e.ButtonState == MouseButtonState.Released && this.isDeferredMovingStarted != true)
        //        this.CancelScrolling();
        //}

        private void CancelScrolling() {
            isMoving = false;
            startPosition = null;
            isDeferredMovingStarted = false;
            //this.RemoveScrollSign();
        }

        private void ScrollViewer_MouseMove(object sender, MouseEventArgs e) {
            var sv = sender as ScrollViewer;

            if (isMoving && sv != null) {
                isDeferredMovingStarted = false; //standard scrolling (Mouse down -> Move)

                var currentPosition = e.GetPosition(sv);
                var offset = currentPosition - startPosition.Value;
                offset.Y /= slowdown;
                offset.X /= slowdown;

                //if(Math.Abs(offset.Y) > 25.0/slowdown)  //Some kind of a dead space, uncomment if it is neccessary
                sv.ScrollToVerticalOffset(sv.VerticalOffset + offset.Y);
                sv.ScrollToHorizontalOffset(sv.HorizontalOffset + offset.X);
            }
        }

        private void ScrollViewer_MouseUp(object sender, MouseEventArgs e) {
            if (isDeferredMovingStarted != true)
                CancelScrolling();
        }

        private void UIElement_OnMouseEnter(object sender, MouseEventArgs e) {
            if (isDeferredMovingStarted != true)
                CancelScrolling();
        }
    }
}