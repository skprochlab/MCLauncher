﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Windows;
using Sentry.Protocol;
using Shared.Model;
using Shared.View;

namespace Shared.ViewModel {
    [Magic]
    internal sealed class ErrorMessageVM : PropertyChangedBase {
        private string _description;

        public ErrorMessageVM(Exception exception, SentryId sentryId) : this(exception) {
            EventId = sentryId.ToString();
        }

        public ErrorMessageVM(Exception exception) {
            CurrentException = exception;
            ErrorText = CurrentException.ToString();
        }

        public Exception CurrentException { get; set; }
        public string ErrorText { get; set; }
        public string EventId { get; set; }


        public AnotherCommandImplementation Restart =>
            new AnotherCommandImplementation(o => {
                Process.Start(new ProcessStartInfo(Environment.GetCommandLineArgs()[0]) {
                    WorkingDirectory = Path.GetDirectoryName(Environment.GetCommandLineArgs()[0])
                });
                //Для того, что-бы не связывать проекты
                AutoSaveableUtils.SaveAll();
                Exceptions.OnRestarting();
                Environment.Exit(0);
            });

        public AnotherCommandImplementation Ignore => new AnotherCommandImplementation(o => { ((Window) o).Close(); });

        public AnotherCommandImplementation Throw => new AnotherCommandImplementation(o => throw new ReThrowedException(CurrentException));

        public string Name { get; set; }

        public string Email { get; set; }

        public string Description {
            get => _description;
            set {
                _description = value;
                CanSend = !string.IsNullOrWhiteSpace(value);
            }
        }

        public AnotherCommandImplementation Send => new AnotherCommandImplementation(o => {
            try {
                var request = WebRequest.Create("https://sentry.io/api/0/projects/skprochs-lab/launcher/user-feedback/");
                request.ContentType = "application/json";
                request.Headers["Authorization"] = "Bearer a2332848b3ab43e3936dbe00d02efa4c5319ec1647284e49b87e01d176c1eccb";
                request.Method = "POST";

                using (var streamWriter = new StreamWriter(request.GetRequestStream())) {
                    var json = $"{{\"comments\":\"{Description}\"," +
                               $"\"email\":\"{(string.IsNullOrWhiteSpace(Email) ? "anon@anon.anon" : Email)}\"," +
                               $"\"event_id\":\"{EventId}\"," +
                               $"\"name\":\"{(string.IsNullOrWhiteSpace(Name) ? "Anonymous" : Name)}\"}}";
                    streamWriter.Write(json);
                }

                var response = request.GetResponse() as HttpWebResponse;
                using (var responseStream = response.GetResponseStream()) {
                    var reader = new StreamReader(responseStream, Encoding.UTF8);
                    var text = reader.ReadToEnd();
                }
            }
            catch (Exception) {
                // ignored
            }
        });

        public bool CanSend { get; set; }
    }

    public static class LauncherErrorHandler {
        public static void ShowErrorWindow(Exception ex) {
            Application.Current.Dispatcher?.Invoke(() => { new ErrorMessage {DataContext = new ErrorMessageVM(ex)}.ShowDialog(); });
        }

        public static void ShowErrorWindow(Exception ex, SentryId sentryId) {
            Application.Current.Dispatcher?.Invoke(() => { new ErrorMessage {DataContext = new ErrorMessageVM(ex, sentryId)}.ShowDialog(); });
        }
    }
}