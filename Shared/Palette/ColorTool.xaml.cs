﻿using System.Windows.Controls;

namespace Shared.Palette {
    /// <summary>
    ///     Interaction logic for ColorTool.xaml
    /// </summary>
    public partial class ColorTool : UserControl {
        public ColorTool() {
            InitializeComponent();
            IsVisibleChanged += (sender, args) => {
                if ((bool) args.NewValue == false) ((ColorToolViewModel) DataContext).SaveThemeToReg();
            };
        }
    }
}