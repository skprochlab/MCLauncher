﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Shared.Palette {
    [Localizable(false)]
    public sealed class BrushToHexConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null) return null;

            string LowerHexString(int i) {
                return i.ToString("X").ToLower();
            }

            if (!(value is SolidColorBrush brush)) return "#ffffff";

            var hex = LowerHexString(brush.Color.R) +
                      LowerHexString(brush.Color.G) +
                      LowerHexString(brush.Color.B);

            return "#" + hex;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotSupportedException();
        }
    }
}