﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Shared.Palette {
    public sealed class ColorToBrushConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value is Color color)
                return new SolidColorBrush(color);

            return Binding.DoNothing;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value is SolidColorBrush brush)
                return brush.Color;

            return default(Color);
        }
    }

    [Localizable(false)]
    public sealed class ColorToHexConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value is Color c)
                return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");

            return "#000000";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            try {
                return (Color) ColorConverter.ConvertFromString(value.ToString());
            }
            catch (Exception) {
                return null;
            }
        }
    }
}