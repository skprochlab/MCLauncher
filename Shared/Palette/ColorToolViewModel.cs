﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters;
using System.Windows.Input;
using System.Windows.Media;
using MaterialDesignColors;
using MaterialDesignColors.ColorManipulation;
using MaterialDesignThemes.Wpf;
using Microsoft.Win32;
using Newtonsoft.Json;

namespace Shared.Palette {
    internal class ColorToolViewModel : INotifyPropertyChanged {
        private readonly RegPaletteHelper _paletteHelper = new RegPaletteHelper();

        private ColorScheme _activeScheme;
        private bool _isDark;

        private Color? _primaryColor;

        private Color? _primaryForegroundColor;

        private Color? _secondaryColor;

        private Color? _secondaryForegroundColor;

        private Color? _selectedColor;

        public ColorToolViewModel() {
            ToggleBaseCommand = new AnotherCommandImplementation(o => ApplyBase((bool) o));
            ChangeHueCommand = new AnotherCommandImplementation(ChangeHue);
            ChangeCustomHueCommand = new AnotherCommandImplementation(ChangeCustomColor);
            ApplyAccentCommand = new AnotherCommandImplementation(o => _paletteHelper.ChangeSecondaryColor(((Swatch) o).AccentExemplarHue.Color));
            ApplyPrimaryCommand = new AnotherCommandImplementation(o => _paletteHelper.ChangePrimaryColor(((Swatch) o).ExemplarHue.Color));
            ChangeToPrimaryCommand = new AnotherCommandImplementation(o => ChangeScheme(ColorScheme.Primary));
            ChangeToSecondaryCommand = new AnotherCommandImplementation(o => ChangeScheme(ColorScheme.Secondary));
            ChangeToPrimaryForegroundCommand = new AnotherCommandImplementation(o => ChangeScheme(ColorScheme.PrimaryForeground));
            ChangeToSecondaryForegroundCommand = new AnotherCommandImplementation(o => ChangeScheme(ColorScheme.SecondaryForeground));


            var theme = _paletteHelper.GetTheme();
            IsDark = theme.Paper == new MaterialDesignDarkTheme().MaterialDesignPaper;

            _primaryColor = theme.PrimaryMid.Color;
            _secondaryColor = theme.SecondaryMid.Color;

            SelectedColor = _primaryColor;
        }

        public ColorScheme ActiveScheme {
            get => _activeScheme;
            set {
                if (_activeScheme == value) return;
                _activeScheme = value;
                OnPropertyChanged();
            }
        }

        public Color? SelectedColor {
            get => _selectedColor;
            set {
                if (_selectedColor == value) return;
                _selectedColor = value;
                OnPropertyChanged();
                OnPropertyChanged(@"AProperty");
                OnPropertyChanged(@"RProperty");
                OnPropertyChanged(@"GProperty");
                OnPropertyChanged(@"BProperty");

                if (value is { } color) ChangeCustomColor(color);
            }
        }

        public IEnumerable<Swatch> OldSwatches { get; } = new SwatchesProvider().Swatches;


        public IEnumerable<ISwatch> Swatches { get; } = SwatchHelper.Swatches;

        public ICommand ChangeCustomHueCommand { get; }

        public ICommand ApplyAccentCommand { get; }
        public ICommand ApplyPrimaryCommand { get; }
        public ICommand ChangeHueCommand { get; }
        public ICommand ChangeToPrimaryCommand { get; }
        public ICommand ChangeToSecondaryCommand { get; }
        public ICommand ChangeToPrimaryForegroundCommand { get; }
        public ICommand ChangeToSecondaryForegroundCommand { get; }

        public ICommand ToggleBaseCommand { get; }

        public bool IsDark {
            get => _isDark;
            set {
                _isDark = value;
                OnPropertyChanged();
            }
        }

        public byte AProperty {
            get => _selectedColor.Value.A;
            set => SelectedColor = Color.FromArgb(value, RProperty, GProperty, BProperty);
        }

        public byte RProperty {
            get => _selectedColor.Value.R;
            set => SelectedColor = Color.FromArgb(AProperty, value, GProperty, BProperty);
        }

        public byte GProperty {
            get => _selectedColor.Value.G;
            set => SelectedColor = Color.FromArgb(AProperty, RProperty, value, BProperty);
        }

        public byte BProperty {
            get => _selectedColor.Value.B;
            set => SelectedColor = Color.FromArgb(AProperty, RProperty, GProperty, value);
        }

        public AnotherCommandImplementation ResetStyle =>
            new AnotherCommandImplementation(o => {
                ApplyBase(true);
                IsDark = true;
                SetPrimaryForegroundToSingleColor(Colors.White);
                SetSecondaryForegroundToSingleColor(Colors.Black);
                _paletteHelper.ChangePrimaryColor(OldSwatches.First(swatch => swatch.Name == @"teal").ExemplarHue.Color);
                _paletteHelper.ChangeSecondaryColor(OldSwatches.First(swatch => swatch.Name == @"amber").AccentExemplarHue.Color);
            });

        public event PropertyChangedEventHandler PropertyChanged;

        private void ApplyBase(bool isDark) {
            var theme = _paletteHelper.GetTheme();
            var baseTheme = isDark ? new MaterialDesignDarkTheme() : (IBaseTheme) new MaterialDesignLightTheme();
            theme.SetBaseTheme(baseTheme);
            _paletteHelper.SetTheme(theme);
        }

        private void ChangeCustomColor(object obj) {
            var color = (Color) obj;

            switch (ActiveScheme) {
                case ColorScheme.Primary:
                    _paletteHelper.ChangePrimaryColor(color);
                    _primaryColor = color;
                    break;
                case ColorScheme.Secondary:
                    _paletteHelper.ChangeSecondaryColor(color);
                    _secondaryColor = color;
                    break;
                case ColorScheme.PrimaryForeground:
                    SetPrimaryForegroundToSingleColor(color);
                    _primaryForegroundColor = color;
                    break;
                case ColorScheme.SecondaryForeground:
                    SetSecondaryForegroundToSingleColor(color);
                    _secondaryForegroundColor = color;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void ChangeScheme(ColorScheme scheme) {
            ActiveScheme = scheme;

            SelectedColor = ActiveScheme switch {
                ColorScheme.Primary             => _primaryColor,
                ColorScheme.Secondary           => _secondaryColor,
                ColorScheme.PrimaryForeground   => _primaryForegroundColor,
                ColorScheme.SecondaryForeground => _secondaryForegroundColor,
                _                               => SelectedColor
            };
        }

        private void ChangeHue(object obj) {
            var hue = (Color) obj;

            SelectedColor = hue;

            switch (ActiveScheme) {
                case ColorScheme.Primary:
                    _paletteHelper.ChangePrimaryColor(hue);
                    _primaryColor = hue;
                    break;
                case ColorScheme.Secondary:
                    _paletteHelper.ChangeSecondaryColor(hue);
                    _secondaryColor = hue;
                    break;
                case ColorScheme.PrimaryForeground:
                    SetPrimaryForegroundToSingleColor(hue);
                    _primaryForegroundColor = hue;
                    break;
                case ColorScheme.SecondaryForeground:
                    SetSecondaryForegroundToSingleColor(hue);
                    _secondaryForegroundColor = hue;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void SetPrimaryForegroundToSingleColor(Color color) {
            var theme = _paletteHelper.GetTheme();
            theme.PrimaryLight = new ColorPair(theme.PrimaryLight.Color, color);
            theme.PrimaryMid = new ColorPair(theme.PrimaryMid.Color, color);
            theme.PrimaryDark = new ColorPair(theme.PrimaryDark.Color, color);
            _paletteHelper.SetTheme(theme);
        }

        private void SetSecondaryForegroundToSingleColor(Color color) {
            var theme = _paletteHelper.GetTheme();
            theme.SecondaryLight = new ColorPair(theme.SecondaryLight.Color, color);
            theme.SecondaryMid = new ColorPair(theme.SecondaryMid.Color, color);
            theme.SecondaryDark = new ColorPair(theme.SecondaryDark.Color, color);
            _paletteHelper.SetTheme(theme);
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void SaveThemeToReg() {
            _paletteHelper.SaveToReg(_paletteHelper.GetTheme());
        }
    }

    internal enum ColorScheme {
        Primary,
        Secondary,
        PrimaryForeground,
        SecondaryForeground
    }

    public static class PaletteHelperExtensions {
        public static void ChangePrimaryColor(this PaletteHelper paletteHelper, Color color) {
            var theme = paletteHelper.GetTheme();
            theme.PrimaryLight = new ColorPair(color.Lighten(), theme.PrimaryLight.ForegroundColor);
            theme.PrimaryMid = new ColorPair(color, theme.PrimaryMid.ForegroundColor);
            theme.PrimaryDark = new ColorPair(color.Darken(), theme.PrimaryDark.ForegroundColor);
            paletteHelper.SetTheme(theme);
        }

        public static void ChangeSecondaryColor(this PaletteHelper paletteHelper, Color color) {
            var theme = paletteHelper.GetTheme();
            theme.SecondaryLight = new ColorPair(color.Lighten(), theme.SecondaryLight.ForegroundColor);
            theme.SecondaryMid = new ColorPair(color, theme.SecondaryMid.ForegroundColor);
            theme.SecondaryDark = new ColorPair(color.Darken(), theme.SecondaryDark.ForegroundColor);
            paletteHelper.SetTheme(theme);
        }
    }

    public sealed class RegPaletteHelper : PaletteHelper {
        public static RegistryKey Reg = Registry.CurrentUser.OpenSubKey("SOFTWARE", true)?.CreateSubKey("SKProCH's Launcher");

        public override void SetTheme(ITheme theme) {
            SaveToReg(theme);
            base.SetTheme(theme);
        }

        public void SaveToReg(ITheme theme) {
            Reg.SetValue("Palette", JsonConvert.SerializeObject(theme, Formatting.None, new JsonSerializerSettings {
                TypeNameHandling = TypeNameHandling.Objects,
                #pragma warning disable CS0618 // 'JsonSerializerSettings.TypeNameAssemblyFormat" является устаревшим: 'TypeNameAssemblyFormat is obsolete. Use TypeNameAssemblyFormatHandling instead.'
                TypeNameAssemblyFormat = FormatterAssemblyStyle.Simple
            }));
        }


        public static void LoadFromReg() {
            try {
                var currentTheme = JsonConvert.DeserializeObject<ITheme>((string) Reg.GetValue("Palette"), new JsonSerializerSettings {
                    TypeNameHandling = TypeNameHandling.Objects
                });
                if (currentTheme == null) return;
                var paletteHelper = new RegPaletteHelper();
                paletteHelper.SetTheme(currentTheme);
            }
            catch (Exception) {
                // ignored
            }
        }
    }
}