using System.Collections.Concurrent;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Timers;

namespace Shared.Model {
    public class AutoSaveable : PropertyChangedBase {
        private readonly string Guid = System.Guid.NewGuid().ToString();
        private readonly Timer lastChangeTimer = new Timer(60000);

        public AutoSaveable() {
            if (this is ISaveable saveable)
                lastChangeTimer.Elapsed += (sender, args) => {
                    lastChangeTimer.Stop();
                    saveable.Save();
                    AutoSaveableUtils.UnQueueToSave(Guid);
                };
        }

        public bool IsSerealizing { get; private set; }

        public void ForgetSave() {
            AutoSaveableUtils.UnQueueToSave(Guid);
        }

        public void SafeSave() {
            if (!(this is ISaveable saveable)) return;
            lastChangeTimer.Stop();
            saveable.Save();
            AutoSaveableUtils.UnQueueToSave(Guid);
        }

        public void QueueSave() {
            AutoSaveableUtils.QueueToSave(Guid, this as ISaveable);
            lastChangeTimer.Stop();
            lastChangeTimer.Start();
        }

        protected override void RaisePropertyChanged(string propName = null) {
            if (!IsSerealizing)
                QueueSave();
            base.RaisePropertyChanged(propName);
        }

        [OnDeserializing]
        internal void OnDeserializingMethod(StreamingContext context) {
            IsSerealizing = true;
        }

        [OnDeserialized]
        internal void OnDeserializedMethod(StreamingContext context) {
            IsSerealizing = false;
        }
    }

    public interface ISaveable {
        void Save();
    }

    public static class AutoSaveableUtils {
        public static readonly ConcurrentDictionary<string, ISaveable> GlobalSaveList = new ConcurrentDictionary<string, ISaveable>();

        public static void QueueToSave(string uuid, ISaveable saveable) {
            GlobalSaveList.TryAdd(uuid, saveable);
        }

        public static void UnQueueToSave(string uuid) {
            GlobalSaveList.TryRemove(uuid, out var unused);
        }

        public static void SaveAll() {
            var localSaveList = GlobalSaveList.ToList();
            GlobalSaveList.Clear();
            Parallel.ForEach(localSaveList, (o, s) => { o.Value.Save(); });
        }
    }
}