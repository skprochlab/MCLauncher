﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace Shared.Model.AttachedProperties {
    internal sealed class GridViewAutoColumnSize {
        public static readonly DependencyProperty AutoColumnSizeProperty = DependencyProperty.RegisterAttached(
            "AutoColumnSize", typeof(bool), typeof(GridViewAutoColumnSize),
            new UIPropertyMetadata(false, (o, args) => {
                if (
                    (bool)
                    o.GetValue(IsCorrectColumnsHandlerInstalledProperty)) return;

                TypeDescriptor
                   .GetProperties((ListView) o)["ItemsSource"]
                   .AddValueChanged((ListView) o,
                        (sender, eventArgs) =>
                            CorrectColumns((ListView) o));
                o.SetValue(IsCorrectColumnsHandlerInstalledProperty,
                    true);
            }));

        public static readonly DependencyProperty IsCorrectColumnsHandlerInstalledProperty = DependencyProperty.RegisterAttached(
            "IsCorrectColumnsHandlerInstalled", typeof(bool),
            typeof(GridViewAutoColumnSize), new PropertyMetadata(false));

        public static void CorrectColumns(ListView sender) {
            if (GetAutoColumnSize(sender) != true) return;
            if (sender == null ||
                ((GridView) sender.View).Columns.Count < 1) return;
            // Simulates column auto sizing
            foreach (var column in
                ((GridView) sender.View).Columns) {
                // Forcing change
                if (double.IsNaN(column.Width))
                    column.Width = 1;
                column.Width = double.NaN; //-V3008
            }
        }

        public static void SetAutoColumnSize(DependencyObject element, bool value) {
            element.SetValue(AutoColumnSizeProperty, value);
        }

        public static bool GetAutoColumnSize(DependencyObject element) {
            return (bool) element.GetValue(AutoColumnSizeProperty);
        }
    }
}