﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Shared.Model {
    public static class ScrollAnimationBehavior {
        #region Private ScrollViewer for ListBox

        private static ScrollViewer _listBoxScroller = new ScrollViewer();

        #endregion

        #region OnVerticalOffset Changed

        private static void OnVerticalOffsetChanged(DependencyObject target, DependencyPropertyChangedEventArgs e) {
            if (target is ScrollViewer scrollViewer)
                scrollViewer.ScrollToVerticalOffset((double) e.NewValue);
        }

        #endregion

        #region OnIsEnabledChanged Changed

        private static void OnIsEnabledChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e) {
            var target = sender;

            if (target != null && target is ScrollViewer viewer)
                viewer.Loaded += ScrollerLoaded;

            if (target == null || !(target is ListBox)) return;
            ((ListBox) target).Loaded += ListboxLoaded;
        }

        #endregion

        #region AnimateScroll Helper

        private static void AnimateScroll(ScrollViewer scrollViewer, double toValue) {
            var verticalAnimation = new DoubleAnimation
                {From = scrollViewer.VerticalOffset, To = toValue, Duration = new Duration(GetTimeDuration(scrollViewer))};
            var storyboard = new Storyboard();
            storyboard.Children.Add(verticalAnimation);
            Storyboard.SetTarget(verticalAnimation, scrollViewer);
            Storyboard.SetTargetProperty(verticalAnimation, new PropertyPath(VerticalOffsetProperty));
            storyboard.Begin();
        }

        #endregion

        #region NormalizeScrollPos Helper

        private static double NormalizeScrollPos(ScrollViewer scroll, double scrollChange, Orientation o) {
            var returnValue = scrollChange;

            if (scrollChange < 0)
                returnValue = 0;

            returnValue = o switch {
                Orientation.Vertical when scrollChange > scroll.ScrollableHeight  => scroll.ScrollableHeight,
                Orientation.Horizontal when scrollChange > scroll.ScrollableWidth => scroll.ScrollableWidth,
                _                                                                 => returnValue
            };

            return returnValue;
        }

        #endregion

        #region UpdateScrollPosition Helper

        private static void UpdateScrollPosition(object sender) {
            if (!(sender is ListBox listbox)) return;
            double scrollTo = 0;

            try {
                for (var i = 0; i < listbox.SelectedIndex; i++)
                    if (listbox.ItemContainerGenerator.ContainerFromItem(listbox.Items[i]) is ListBoxItem tempItem)
                        scrollTo += tempItem.ActualHeight;
            }
            finally {
                AnimateScroll(_listBoxScroller, scrollTo);
            }
        }

        #endregion

        #region SetEventHandlersForScrollViewer Helper

        private static void SetEventHandlersForScrollViewer(IInputElement scroller) {
            scroller.PreviewMouseWheel += ScrollViewerPreviewMouseWheel;
            scroller.PreviewKeyDown += ScrollViewerPreviewKeyDown;
        }

        #endregion

        #region scrollerLoaded Event Handler

        private static void ScrollerLoaded(object sender, RoutedEventArgs e) {
            var scroller = sender as ScrollViewer;
            SetEventHandlersForScrollViewer(scroller);
        }

        #endregion

        #region ScrollViewerPreviewMouseWheel Event Handler

        private static void ScrollViewerPreviewMouseWheel(object sender, MouseWheelEventArgs e) {
            double mouseWheelChange = e.Delta;
            var scroller = sender as ScrollViewer;
            var newVOffset = GetVerticalOffset(scroller) - mouseWheelChange / 3;

            if (newVOffset < 0)
                AnimateScroll(scroller, 0);
            else if (newVOffset > scroller.ScrollableHeight)
                AnimateScroll(scroller, scroller.ScrollableHeight);
            else
                AnimateScroll(scroller, newVOffset);

            e.Handled = true;
        }

        #endregion

        #region ScrollViewerPreviewKeyDown Handler

        private static void ScrollViewerPreviewKeyDown(object sender, KeyEventArgs e) {
            var scroller = sender as ScrollViewer;
            var keyPressed = e.Key;
            var newVerticalPos = GetVerticalOffset(scroller);

            // если добавить все возможные ветки, то там 200 строк кода лишних выйдет
            // ReSharper disable once ConvertSwitchStatementToSwitchExpression
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (keyPressed) {
                case Key.Down:
                    newVerticalPos = NormalizeScrollPos(scroller, newVerticalPos + GetPointsToScroll(scroller), Orientation.Vertical);
                    break;
                case Key.PageDown:
                    newVerticalPos = NormalizeScrollPos(scroller, newVerticalPos + scroller.ViewportHeight, Orientation.Vertical);
                    break;
                case Key.Up:
                    newVerticalPos = NormalizeScrollPos(scroller, newVerticalPos - GetPointsToScroll(scroller), Orientation.Vertical);
                    break;
                case Key.PageUp:
                    newVerticalPos = NormalizeScrollPos(scroller, newVerticalPos - scroller.ViewportHeight, Orientation.Vertical);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (newVerticalPos != GetVerticalOffset(scroller))
                AnimateScroll(scroller, newVerticalPos);

            e.Handled = true;
        }

        #endregion

        #region VerticalOffset Property

        public static DependencyProperty VerticalOffsetProperty =
            DependencyProperty.RegisterAttached("VerticalOffset",
                typeof(double),
                typeof(ScrollAnimationBehavior),
                new UIPropertyMetadata(0.0, OnVerticalOffsetChanged));

        public static void SetVerticalOffset(FrameworkElement target, double value) {
            target.SetValue(VerticalOffsetProperty, value);
        }

        public static double GetVerticalOffset(FrameworkElement target) {
            return (double) target.GetValue(VerticalOffsetProperty);
        }

        #endregion

        #region TimeDuration Property

        public static DependencyProperty TimeDurationProperty =
            DependencyProperty.RegisterAttached("TimeDuration",
                typeof(TimeSpan),
                typeof(ScrollAnimationBehavior),
                new PropertyMetadata(new TimeSpan(0, 0, 0, 0, 0)));

        public static void SetTimeDuration(FrameworkElement target, TimeSpan value) {
            target.SetValue(TimeDurationProperty, value);
        }

        public static TimeSpan GetTimeDuration(FrameworkElement target) {
            return (TimeSpan) target.GetValue(TimeDurationProperty);
        }

        #endregion

        #region PointsToScroll Property

        public static DependencyProperty PointsToScrollProperty =
            DependencyProperty.RegisterAttached("PointsToScroll",
                typeof(double),
                typeof(ScrollAnimationBehavior),
                new PropertyMetadata(0.0));

        public static void SetPointsToScroll(FrameworkElement target, double value) {
            target.SetValue(PointsToScrollProperty, value);
        }

        public static double GetPointsToScroll(FrameworkElement target) {
            return (double) target.GetValue(PointsToScrollProperty);
        }

        #endregion

        #region IsEnabled Property

        public static DependencyProperty IsEnabledProperty =
            DependencyProperty.RegisterAttached("IsEnabled",
                typeof(bool),
                typeof(ScrollAnimationBehavior),
                new UIPropertyMetadata(false, OnIsEnabledChanged));

        public static void SetIsEnabled(FrameworkElement target, bool value) {
            target.SetValue(IsEnabledProperty, value);
        }

        public static bool GetIsEnabled(FrameworkElement target) {
            return (bool) target.GetValue(IsEnabledProperty);
        }

        #endregion

        #region listboxLoaded Event Handler

        private static void ListboxLoaded(object sender, RoutedEventArgs e) {
            var listbox = sender as ListBox;
            _listBoxScroller = FindVisualChild<ScrollViewer>(listbox);
            SetEventHandlersForScrollViewer(_listBoxScroller);
            SetTimeDuration(_listBoxScroller, new TimeSpan(0, 0, 0, 0, 200));
            SetPointsToScroll(_listBoxScroller, 16.0);
            listbox.SelectionChanged += ListBoxSelectionChanged;
            listbox.Loaded += ListBoxLoaded;
            listbox.LayoutUpdated += ListBoxLayoutUpdated;
        }

        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj)
            where T : DependencyObject {
            if (depObj == null) yield break;

            for (var i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++) {
                var child = VisualTreeHelper.GetChild(depObj, i);

                if (child is T dependencyObject)
                    yield return dependencyObject;

                foreach (var childOfChild in FindVisualChildren<T>(child))
                    yield return childOfChild;
            }
        }

        public static TDependencyObject FindVisualChild<TDependencyObject>(DependencyObject obj)
            where TDependencyObject : DependencyObject {
            return FindVisualChildren<TDependencyObject>(obj).FirstOrDefault();
        }

        #endregion

        #region ListBox Event Handlers

        private static void ListBoxLayoutUpdated(object sender, EventArgs e) {
            UpdateScrollPosition(sender);
        }

        private static void ListBoxLoaded(object sender, RoutedEventArgs e) {
            UpdateScrollPosition(sender);
        }

        private static void ListBoxSelectionChanged(object sender, SelectionChangedEventArgs e) {
            UpdateScrollPosition(sender);
        }

        #endregion
    }
}