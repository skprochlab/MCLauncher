using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Shared.Model.AttachedProperties {
    public static class ValidationBehavior {
        private static DependencyPropertyDescriptor GetHasErrorDescriptor(DependencyObject d) {
            return (DependencyPropertyDescriptor) d.GetValue(HasErrorDescriptorProperty);
        }

        private static void SetHasErrorDescriptor(DependencyObject d, DependencyPropertyDescriptor value) {
            d.SetValue(HasErrorDescriptorProperty, value);
        }

        #region Attached Properties

        public static readonly DependencyProperty HasErrorProperty = DependencyProperty.RegisterAttached(
            "HasError",
            typeof(bool),
            typeof(ValidationBehavior),
            new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                null, CoerceHasError));

        private static readonly DependencyProperty HasErrorDescriptorProperty = DependencyProperty.RegisterAttached(
            "HasErrorDescriptor",
            typeof(DependencyPropertyDescriptor),
            typeof(ValidationBehavior));

        #endregion

        #region Attached Property Getters and setters

        public static bool GetHasError(DependencyObject d) {
            return (bool) d.GetValue(HasErrorProperty);
        }

        public static void SetHasError(DependencyObject d, bool value) {
            d.SetValue(HasErrorProperty, value);
        }

        #endregion

        #region CallBacks

        private static object CoerceHasError(DependencyObject d, object baseValue) {
            var result = (bool) baseValue;

            if (BindingOperations.IsDataBound(d, HasErrorProperty)) {
                if (GetHasErrorDescriptor(d) != null) return result;
                var desc = DependencyPropertyDescriptor.FromProperty(Validation.HasErrorProperty, d.GetType());
                desc.AddValueChanged(d, OnHasErrorChanged);
                SetHasErrorDescriptor(d, desc);
                result = Validation.GetHasError(d);
            }
            else {
                if (GetHasErrorDescriptor(d) == null) return result;
                var desc = GetHasErrorDescriptor(d);
                desc.RemoveValueChanged(d, OnHasErrorChanged);
                SetHasErrorDescriptor(d, null);
            }

            return result;
        }

        private static void OnHasErrorChanged(object sender, EventArgs e) {
            (sender as DependencyObject)?.SetValue(HasErrorProperty, ((DependencyObject) sender).GetValue(Validation.HasErrorProperty));
        }

        #endregion
    }
}