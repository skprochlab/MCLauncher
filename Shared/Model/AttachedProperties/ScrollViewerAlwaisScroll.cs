﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Shared.Model
{
    [Localizable(false)]
    public class ScrollViewerAlwaisScroll
    {
        public static readonly DependencyProperty CanScrollToEndProperty =
            DependencyProperty.RegisterAttached("CanScrollToEnd", typeof(bool), typeof(ScrollViewerAlwaisScroll),
                                                new FrameworkPropertyMetadata(AlwaysScrollToEndChanged)
                                                {BindsTwoWayByDefault = true, DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged, DefaultValue = false});

        public static bool GetCanScrollToEnd(DependencyObject scroll) {
            if (scroll == null) {
                throw new ArgumentNullException("scroll");
            }

            return (bool) scroll.GetValue(CanScrollToEndProperty);
        }

        public static void SetCanScrollToEnd(DependencyObject scroll, bool alwaysScrollToEnd) {
            if (scroll == null) {
                throw new ArgumentNullException("scroll");
            }

            scroll.SetValue(CanScrollToEndProperty, alwaysScrollToEnd);
        }

        public static readonly DependencyProperty ScrollingToEndNowProperty =
            DependencyProperty.RegisterAttached("ScrollingToEndNow", typeof(bool), typeof(ScrollViewerAlwaisScroll),
                                                new FrameworkPropertyMetadata(ScrollingToEndNowChanged)
                                                {BindsTwoWayByDefault = true, DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged, DefaultValue = false});

        public static bool GetScrollingToEndNow(DependencyObject scroll) {
            if (scroll == null) {
                throw new ArgumentNullException("scroll");
            }

            return (bool) scroll.GetValue(ScrollingToEndNowProperty);
        }

        public static void SetScrollingToEndNow(ScrollViewer scroll, bool alwaysScrollToEnd) {
            if (scroll == null) {
                throw new ArgumentNullException("scroll");
            }

            scroll.SetValue(ScrollingToEndNowProperty, alwaysScrollToEnd);
        }

        private static void ScrollingToEndNowChanged(object sender, DependencyPropertyChangedEventArgs e) {
            if ((bool) e.NewValue == false) return;
            ScrollViewer scroll = (sender as ScrollViewer) ?? (sender as DependencyObject).GetChildOfType<ScrollViewer>();
            if (scroll == null)
                throw new InvalidOperationException("The attached AlwaysScrollToEnd property can only be applied to ScrollViewer instances.");
            scroll.ScrollToEnd();
        }

        private static void AlwaysScrollToEndChanged(object sender, DependencyPropertyChangedEventArgs e) {
            ScrollViewer scroll = (sender as ScrollViewer) ?? (sender as DependencyObject).GetChildOfType<ScrollViewer>();
            if (scroll == null)
                throw new InvalidOperationException("The attached AlwaysScrollToEnd property can only be applied to ScrollViewer instances.");
            bool alwaysScrollToEnd = (e.NewValue != null) && (bool) e.NewValue;
            if (alwaysScrollToEnd) {
                scroll.ScrollToEnd();
                scroll.ScrollChanged += ScrollChanged;
                SetScrollingToEndNow(scroll, true);
            }
            else {
                scroll.ScrollChanged -= ScrollChanged;
                SetScrollingToEndNow(scroll, false);
            }
        }

        private static void ScrollChanged(object sender, ScrollChangedEventArgs e) {
            ScrollViewer scroll = (sender as ScrollViewer) ?? (sender as DependencyObject).GetChildOfType<ScrollViewer>();
            if (scroll == null)
                throw new InvalidOperationException("The attached AlwaysScrollToEnd property can only be applied to ScrollViewer instances.");
            // User scroll event : set or unset autoscroll mode
            if (e.ExtentHeightChange == 0) {
                SetScrollingToEndNow(scroll, Math.Abs(scroll.VerticalOffset - scroll.ScrollableHeight) < 2);
            }

            // Content scroll event : autoscroll eventually
            if (e.ExtentHeightChange == 0) return;
            if ((bool) scroll.GetValue(ScrollingToEndNowProperty))
                scroll.ScrollToVerticalOffset(scroll.ExtentHeight);
        }
    }
}