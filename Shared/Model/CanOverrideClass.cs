﻿namespace Shared.Model {
    [Magic]
    public sealed class CanOverrideClass<T> : PropertyChangedBase
        where T : class {
        public CanOverrideClass() { }

        public CanOverrideClass(T content) {
            Content = content;
        }

        public CanOverrideClass(T content, bool isEnabled) {
            IsEnabled = isEnabled;
            Content = content;
        }

        public CanOverrideClass(T content, bool isEnabled, bool isDefault) {
            IsEnabled = isEnabled;
            IsDefault = isDefault;
            Content = content;
        }

        public bool IsEnabled { get; set; } = true;
        public bool IsDefault { get; set; }
        public T Content { get; set; }
    }
}