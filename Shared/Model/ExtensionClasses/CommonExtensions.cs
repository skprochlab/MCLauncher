﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;

namespace Shared.Model {
    public static class CommonExtensions {
        public static T GetChildOfType<T>(this DependencyObject depObj)
            where T : DependencyObject {
            if (depObj == null) return null;

            for (var i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++) {
                var child = VisualTreeHelper.GetChild(depObj, i);

                var result = child as T ?? GetChildOfType<T>(child);
                if (result != null) return result;
            }

            return null;
        }

        public static HashSet<T> ToHashSet<T>(this IEnumerable<T> source,
                                              IEqualityComparer<T> comparer = null) {
            return new HashSet<T>(source, comparer);
        }
    }
}