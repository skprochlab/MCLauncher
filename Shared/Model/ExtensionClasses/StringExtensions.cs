﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Shared.Model {
    [Localizable(false)]
    public static class StringExtensions {
        #region SplitLines

        public static string[] SplitByLines(this string source, StringSplitOptions splitOption = StringSplitOptions.RemoveEmptyEntries) {
            var result = Regex.Split(source, "\r\n|\r|\n");
            return splitOption == StringSplitOptions.None ? result : result.Where(s => s != string.Empty).ToArray();
        }

        #endregion

        #region SafeSubstring

        public static string SafeSubstring(this string text, int start, int length) {
            return text.Length <= start         ? ""
                : text.Length - start <= length ? text.Substring(start)
                                                  : text.Substring(start, length);
        }

        #endregion

        #region Escaping

        private static readonly char[] ToEscape =
            "\0\x1\x2\x3\x4\x5\x6\a\b\t\n\v\f\r\xe\xf\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f\"\\".ToCharArray();

        private static readonly string[] Literals =
            @"\0,\x0001,\x0002,\x0003,\x0004,\x0005,\x0006,\a,\b,\t,\n,\v,\f,\r,\x000e,\x000f,\x0010,\x0011,\x0012,\x0013,\x0014,\x0015,\x0016,\x0017,\x0018,\x0019,\x001a,\x001b,\x001c,\x001d,\x001e,\x001f"
               .Split(',');

        public static string Escape(this string input) {
            var i = input.IndexOfAny(ToEscape);
            if (i < 0) return input;

            var sb = new StringBuilder(input.Length + 5);
            var j = 0;

            do {
                sb.Append(input, j, i - j);
                var c = input[i];
                if (c < 0x20) sb.Append(Literals[c]);
                else sb.Append(@"\").Append(c);
            } while ((i = input.IndexOfAny(ToEscape, j = ++i)) > 0);

            return sb.Append(input, j, input.Length - j).ToString();
        }

        #endregion

        #region Trim

        public static string Trim(this string target, string trimString) {
            return string.IsNullOrEmpty(trimString) ? target : target.TrimEnd(trimString).TrimStart(trimString);
        }

        public static string TrimStart(this string target, string trimString) {
            if (string.IsNullOrEmpty(trimString)) return target;

            var result = target;

            while (result.StartsWith(trimString))
                result = result.Substring(trimString.Length);

            return result;
        }

        public static string TrimEnd(this string target, string trimString) {
            if (string.IsNullOrEmpty(trimString)) return target;

            var result = target;

            while (result.EndsWith(trimString))
                result = result.Substring(0, result.Length - trimString.Length);

            return result;
        }

        #endregion
    }
}