﻿using System.Collections.Generic;
using System.Linq;

namespace Shared.Model {
    public static class EnumerableExtension {
        public static IEnumerable<T> ConcatItems<T>(this IEnumerable<T> source, params T[] items) {
            return source.Concat(items);
        }
    }
}