﻿using System;

namespace Shared.Model
{
    public static partial class ExceptionExtension
    {
        public static void Throw(this Exception ex) {
            if (ex != null)
                throw ex;
        }

        public static void ThrowUnsafe(this Exception ex) { throw ex; }
    }
}