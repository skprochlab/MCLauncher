﻿using System;
using System.IO;
using System.Linq;

namespace Shared.Model {
    internal static class Utilities {
        #region FindInPATH

        public static string FindOnPath(string exeName) {
            return (Environment.GetEnvironmentVariable("PATH") ?? "")
                  .Split(';')
                  .Select(test => test.Trim())
                  .Where(path => !string.IsNullOrEmpty(path) && File.Exists(Path.Combine(path, exeName)))
                  .Select(Path.GetFullPath)
                  .FirstOrDefault();
        }

        #endregion

        public static string GetDirectory(string path) {
            return string.IsNullOrWhiteSpace(Path.GetExtension(path)) ? path : Path.GetDirectoryName(path);
        }

        public static void MoveDirectory(string source, string target) {
            var sourcePath = source.TrimEnd('\\', ' ');
            var targetPath = target.TrimEnd('\\', ' ');
            var files = Directory.EnumerateFiles(sourcePath, @"*", SearchOption.AllDirectories)
                                 .GroupBy(Path.GetDirectoryName);
            foreach (var folder in files) {
                var targetFolder = folder.Key.Replace(sourcePath, targetPath);
                Directory.CreateDirectory(targetFolder);
                foreach (var file in folder) {
                    var targetFile = Path.Combine(targetFolder, Path.GetFileName(file));
                    try {
                        if (File.Exists(targetFile)) File.Delete(targetFile);
                        File.Move(file, targetFile);
                    }
                    catch (Exception) {
                        // ignored
                    }
                }
            }

            Directory.Delete(source, true);
        }

        #region FileArch

        public enum MachineType {
            Native = 0,
            I586 = 0x014c,
            Itanium = 0x0200,
            X64 = 0x8664
        }

        public static MachineType GetMachineType(string fileName) {
            // dos header is 64 bytes
            // PE header address is 4 bytes
            const int pePtrOffset = 60;
            const int machineOffset = 4;
            var data = new byte[4096];

            using (Stream stm = new FileStream(fileName, FileMode.Open, FileAccess.Read)) {
                stm.Read(data, 0, 4096);
            }

            var peHdrAddress = BitConverter.ToInt32(data, pePtrOffset);
            int machineUint = BitConverter.ToUInt16(data, peHdrAddress + machineOffset);
            return (MachineType) machineUint;
        }

        #endregion
    }
}