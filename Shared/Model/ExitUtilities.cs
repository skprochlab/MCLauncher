﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace Shared.Model {
    public static class ExitUtilities {
        public static event EventHandler<string> Restart;
        public static event EventHandler Dispose;
        public static event EventHandler Save;

        public static void SmartRestart(string dir = null) {
            dir ??= AppDomain.CurrentDomain.BaseDirectory;
            PrepareExit();
            try {
                Restart?.Invoke(null, dir);
            }
            finally {
                Shutdown();
            }
        }

        public static void InitiateExit() {
            PrepareExit();
            Shutdown();
        }

        public static void PrepareExit() {
            SaveAll();
            DisposeForExit();
        }

        public static void Shutdown() {
            Application.Current.Shutdown();
            new Thread(async o => {
                await Task.Delay(5000);
                Environment.Exit(-1);
            }) {Priority = ThreadPriority.Highest}.Start();
        }

        public static void DisposeForExit() {
            Dispose?.Invoke(null, null);
        }

        public static void SaveAll() {
            AutoSaveableUtils.SaveAll();
            Save?.Invoke(null, null);
        }
    }
}