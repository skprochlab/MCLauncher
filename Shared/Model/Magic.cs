﻿using System;

namespace Shared {
    internal sealed class MagicAttribute : Attribute { }

    internal sealed class NoMagicAttribute : Attribute { }
}