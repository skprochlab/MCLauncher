using System.Globalization;
using System.Windows.Controls;

namespace Shared.Model.ValidationRules {
    public class NonEmptyRule : ValidationRule {
        public override ValidationResult Validate(object value, CultureInfo ci) {
            if (value == null || string.IsNullOrWhiteSpace(value.ToString()))
                return new ValidationResult(false, @"Field can't be empty");
            return new ValidationResult(true, null);
        }
    }
}