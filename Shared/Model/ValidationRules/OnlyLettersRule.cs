using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Controls;

namespace Shared.Model.ValidationRules {
    public sealed class OnlyLettersRule : ValidationRule {
        public override ValidationResult Validate(object value, CultureInfo ci) {
            if (value == null || string.IsNullOrWhiteSpace(value.ToString()))
                return new ValidationResult(false, @"Field can't be empty");

            var r = new Regex("^[a-zA-Z]*$");
            return r.IsMatch(value.ToString()) ? new ValidationResult(true, null) : new ValidationResult(false, @"Unacceptable symbols");
        }
    }
}