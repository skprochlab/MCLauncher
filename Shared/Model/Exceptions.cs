﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Sentry;
using Shared.ViewModel;

namespace Shared.Model {
    internal static class Exceptions {
        public static event EventHandler Restarting;

        public static void SetGlobalHandlers(Dispatcher dispatcher) {
            AppDomain.CurrentDomain.UnhandledException += async delegate(object sender, UnhandledExceptionEventArgs args) {
                #if !DEBUG
                if (args.ExceptionObject is ReThrowedException) return;
                LauncherErrorHandler.ShowErrorWindow(args.ExceptionObject as Exception, SentrySdk.CaptureException(args.ExceptionObject as Exception));
                Thread.CurrentThread.IsBackground = true;
                Thread.CurrentThread.Name = "Dead thread";
                await Task.Delay(-1);
                #endif
            };
            TaskScheduler.UnobservedTaskException += (sender, args) => {
                #if !DEBUG
                args.Exception.Flatten();
                LauncherErrorHandler.ShowErrorWindow(args.Exception, SentrySdk.CaptureException(args.Exception));
                args.SetObserved();
                #endif
            };
            dispatcher.UnhandledException += (sender, args) => {
                #if !DEBUG
                if (args.Exception is ReThrowedException) return;
                if (args.Handled) return;
                LauncherErrorHandler.ShowErrorWindow(args.Exception, SentrySdk.CaptureException(args.Exception));
                args.Handled = true;
                #endif
            };
            Application.Current.DispatcherUnhandledException += (sender, args) => {
                #if !DEBUG
                if (args.Exception is ReThrowedException) return;
                if (args.Handled) return;
                LauncherErrorHandler.ShowErrorWindow(args.Exception, SentrySdk.CaptureException(args.Exception));
                args.Handled = true;
                #endif
            };
        }

        public static void OnRestarting() {
            Restarting?.Invoke(null, EventArgs.Empty);
        }
    }

    public class ReThrowedException : Exception {
        public ReThrowedException(Exception innerException)
            : base("This exception has rethrown from exception handler user control", innerException) { }
    }
}