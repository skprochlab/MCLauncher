using System;
using System.Collections.Generic;
using System.Windows;

namespace SKProCHInstaller.Model {
    public static class WPFUtils {
        public static IEnumerable<T> FindLogicalChildren<T>(DependencyObject depObj) where T : DependencyObject {
            if (depObj == null) yield break;

            foreach (var rawChild in LogicalTreeHelper.GetChildren(depObj)) {
                if (!(rawChild is DependencyObject)) continue;
                var child = (DependencyObject) rawChild;
                if (child is T dependencyObject) yield return dependencyObject;

                foreach (var childOfChild in FindLogicalChildren<T>(child))
                    yield return childOfChild;
            }
        }

        public static void ForceValidateBindings<T>(object parentObject, DependencyProperty property) where T : FrameworkElement {
            foreach (var textBox in FindLogicalChildren<T>(parentObject as DependencyObject))
                try {
                    textBox.GetBindingExpression(property).UpdateSource();
                }
                catch (Exception) {
                    // ignored
                }
        }
    }
}