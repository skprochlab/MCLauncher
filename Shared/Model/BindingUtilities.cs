using System.Windows;

namespace SKProCHInstaller.Model {
    public class Wrapper : DependencyObject {
        public static readonly DependencyProperty FormatProperty =
            DependencyProperty.Register("Format", typeof(object),
                typeof(Wrapper), new FrameworkPropertyMetadata(null));

        public object Format {
            get => GetValue(FormatProperty);
            set => SetValue(FormatProperty, value);
        }
    }

    public class BindingProxy : Freezable {
        public static readonly DependencyProperty DataProperty =
            DependencyProperty.Register("Data", typeof(object), typeof(BindingProxy), new PropertyMetadata(null));

        public object Data {
            get => GetValue(DataProperty);
            set => SetValue(DataProperty, value);
        }

        protected override Freezable CreateInstanceCore() {
            return new BindingProxy();
        }
    }
}